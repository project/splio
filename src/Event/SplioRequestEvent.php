<?php

namespace Drupal\splio\Event;

use Drupal\Core\Entity\EntityInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Defines the events related to the sync activity of the Splio module.
 *
 * This class dispatches events related to the requests made to the Splio API.
 * These events are meant to be dispatched right before a request to Splio is
 * sent.
 */
class SplioRequestEvent extends Event {

  const SPLIO_CREATE = 'splio_event.create';
  const SPLIO_READ = 'splio_event.read';
  const SPLIO_UPDATE = 'splio_event.update';
  const SPLIO_DELETE = 'splio_event.delete';

  /**
   * Splio entity.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  private EntityInterface $drupalEntity;

  /**
   * Splio entity formatted inside an array ready to be sent to Splio.
   *
   * @var array
   */
  private array $splioEntity;

  /**
   * Determines if the original entity has been changed.
   *
   * @var bool
   */
  private bool $changed;

  /**
   * SplioEvent constructor.
   *
   * @param array $splioEntity
   *   Receives an entity in the form of an array, formatted with the structure
   *   that the Splio API expects to receive for Splio entities.
   */
  public function __construct(
    EntityInterface $drupalEntity,
    array $splioEntity
  ) {
    $this->drupalEntity = $drupalEntity;
    $this->splioEntity = $splioEntity;
    $this->changed = FALSE;
  }

  /**
   * Alters the object that will be sent to the Splio API.
   *
   * @param array $entity
   *   Receives a Splio entity formatted inside an array.
   */
  public function alterSplioEntity(array $entity) {
    if ($this->changed === FALSE) {
      $this->changed = !($this->splioEntity === $entity);
    }

    $this->splioEntity = $entity;
  }

  /**
   * Returns the entity whose data will be sent to Splio.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The Drupal entity.
   */
  public function getDrupalEntity() {
    return $this->drupalEntity;
  }

  /**
   * Returns the object that will be sent to the Splio API.
   *
   * @return array
   *   Receives a Splio entity formatted inside an array.
   */
  public function getSplioEntity() {
    return $this->splioEntity;
  }

  /**
   * Determines whether the entity to be sent to the Splio API has been altered.
   *
   * @return bool
   *   Returns true if the item has change, false in any other case.
   */
  public function hasChangedEntity() {
    return $this->changed;
  }

}
