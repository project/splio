<?php

namespace Drupal\splio\Event;

use Drupal\Core\Entity\EntityInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Defines the events related to the sync activity of the Splio module.
 *
 * This class dispatches events related to the responses received by Splio.
 * These events are meant to be dispatched right after a response from Splio is
 * received.
 */
class SplioResponseEvent extends Event {

  public const SPLIO_EVENT = 'splio_event.response';

  public const LOG_LEVEL_ERROR = 'error';

  public const LOG_LEVEL_WARNING = 'warning';

  public const LOG_LEVEL_NOTICE = 'notice';

  /**
   * Splio's received response. It may be a response or an exception.
   *
   * @var mixed
   */
  private $response;

  /**
   * Splio's sent entity structure.
   *
   * @var array|null
   */
  private array $entityStructure;

  /**
   * Entity used to generate the Splio entity structure.
   *
   * @var \Drupal\Core\Entity\EntityInterface|null
   */
  private EntityInterface $entity;

  /**
   * In case an exception occurred while trying to sync with Splio, and this
   * param is set to TRUE, the exception will not be thrown. No traces in the
   * log will appear either. By default, this param is set to FALSE.
   *
   * @var bool
   */
  private bool $isSilentException;

  /**
   * The entity type.
   *
   * @var string
   */
  private string $entityType;

  /**
   * The action.
   *
   * @var string
   */
  private string $action;

  /**
   * The log level (error, warning, notice).
   *
   * @var string
   */
  private string $logLevel = self::LOG_LEVEL_ERROR;

  /**
   * SplioResponseEvent constructor.
   *
   * @param mixed $response
   *   Receives the response received from Splio.
   * @param array|null $entityStructure
   *   Receives the entity structure sent to Splio.
   * @param \Drupal\Core\Entity\EntityInterface|null $entity
   *   Receives the entity used to generate the Splio entity structure.
   */
  public function __construct(
    $response,
    array $entityStructure,
    EntityInterface $entity,
    string $entityType,
    string $action
  ) {

    $this->response = $response;
    $this->entityStructure = $entityStructure;
    $this->entity = $entity;
    $this->isSilentException = FALSE;
    $this->entityType = $entityType;
    $this->action = $action;
  }

  /**
   * Returns the response received from the Splio API.
   *
   * @return mixed
   *   Returns the result of the HTTP request made to Splio's API.
   */
  public function getSplioResponse() {

    return $this->response;
  }

  /**
   * Returns the Splio entity structure sent to Splio's API.
   *
   * @return array
   *   Returns a Splio entity formatted inside an array.
   */
  public function getSplioEntityStructure(): ?array {

    return $this->entityStructure;
  }

  /**
   * Returns the Drupal entity used to generate the structure sent to Splio API.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The Drupal entity used to generate the structure sent to Splio API.
   */
  public function getSplioEntity(): ?EntityInterface {

    return $this->entity;
  }

  /**
   * Returns the entity type.
   *
   * @return string
   *   The entity type.
   */
  public function getEntityType(): string {

    return $this->entityType;
  }

  /**
   * Returns the action.
   *
   * @return string
   *   The action.
   */
  public function getAction(): string {

    return $this->action;
  }

  /**
   * Returns the property isSilentException.
   *
   * @return bool
   *   Returns a bool defining if an exception should be thrown after the event
   *   dispatch.
   */
  public function isSilentException(): bool {

    return $this->isSilentException;
  }

  /**
   * Sets the property isSilentException.
   *
   * @param bool $isSilentException
   *   Defines if an exception should be thrown after the event dispatch.
   */
  public function setIsSilentException(bool $isSilentException): void {

    $this->isSilentException = $isSilentException;
  }

  /**
   * Returns the log level.
   *
   * @return string
   *   The log level.
   */
  public function getLogLevel(): string {

    return $this->logLevel;
  }

  /**
   * Sets the log level.
   *
   * @param string $logLevel
   *   The log level.
   */
  public function setLogLevel(string $logLevel): void {

    $this->logLevel = $logLevel;
  }

}
