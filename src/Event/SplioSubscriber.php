<?php

namespace Drupal\splio\Event;

use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Queue\SuspendQueueException;
use Drupal\splio\Entity\SplioEntity;
use Drupal\splio\Exception\EntitiesNotConfiguredYetException;
use Drupal\splio\Exception\EntityIsEmptyException;
use Drupal\splio\Exception\EntityTypeNotFoundInSplioException;
use Drupal\splio\Exception\OriginalEntityNotFoundException;
use Drupal\splio\Exception\UserModifyUniqueKeyFieldValueException;
use Drupal\splio\Services\SplioConnector;
use Drupal\splio\Services\SplioContactsListsManagerInterface;
use Drupal\splio\Services\SplioEntityHandlerInterface;
use Drupal\splio\Services\SplioQueueHandlerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class SplioSubscriber implements EventSubscriberInterface {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  private EntityTypeManager $entityTypeManager;

  /**
   * Splio connector.
   *
   * @var \Drupal\splio\Services\SplioConnector
   */
  private SplioConnector $splioConnector;

  /**
   * Splio queue handler.
   *
   * @var \Drupal\splio\Services\SplioQueueHandlerInterface
   */
  private SplioQueueHandlerInterface $splioQueueHandler;

  /**
   * Splio entity handler.
   *
   * @var \Drupal\splio\Services\SplioEntityHandlerInterface
   */
  private SplioEntityHandlerInterface $splioEntityHandler;

  /**
   * Splio contacts lists manager.
   *
   * @var \Drupal\splio\Services\SplioContactsListsManagerInterface
   */
  private SplioContactsListsManagerInterface $contactsListsManager;

  /**
   * Logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  private LoggerInterface $logger;

  public function __construct(
    EntityTypeManager $entityTypeManager,
    SplioConnector $splioConnector,
    SplioQueueHandlerInterface $splioQueueHandler,
    SplioEntityHandlerInterface $splioEntityHandler,
    SplioContactsListsManagerInterface $contactsListsManager,
    LoggerInterface $logger
  ) {

    $this->entityTypeManager = $entityTypeManager;
    $this->splioConnector = $splioConnector;
    $this->splioQueueHandler = $splioQueueHandler;
    $this->splioEntityHandler = $splioEntityHandler;
    $this->contactsListsManager = $contactsListsManager;
    $this->logger = $logger;
  }

  /**
   * EntityConfigForm container creator.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Global container for EntityConfigForm.
   *
   * @return \Drupal\splio\Event\SplioSubscriber
   *   Returns the config form with the injected services.
   */
  public static function create(
    ContainerInterface $container
  ): SplioSubscriber {

    return new static(
      $container->get('entity_type.manager'),
      $container->get('splio.splio_connector'),
      $container->get('splio.queue_handler'),
      $container->get('splio.entity_handler'),
      $container->get('splio.contacts_lists_manager'),
      $container->get('splio.services.yml')
    );
  }

  /**
   * {@inheritDoc}
   */
  public static function getSubscribedEvents(): array {

    $events[SplioRequestEvent::SPLIO_CREATE][] = ['splioRequestCreate'];
    $events[SplioRequestEvent::SPLIO_UPDATE][] = ['splioRequestUpdate'];
    $events[SplioResponseEvent::SPLIO_EVENT][] = ['splioResponseReceived'];

    return $events;
  }

  /**
   * Reacts whenever a "SplioRequestEvent::SPLIO_CREATE" is triggered.
   *
   * Checks the received entity type and performs the proper actions for
   * each case before sending a request to Splio.
   *
   * @param \Drupal\splio\Event\SplioRequestEvent $event
   *   The Splio request event.
   *
   * @noinspection PhpUnused
   */
  public function splioRequestCreate(SplioRequestEvent $event) {

    $splioEntity = $event->getSplioEntity();
    $action = $splioEntity['splioEntityType'] . 'RequestCreate';

    if (method_exists($this, $action)) {
      $this->$action($event);
    }
  }

  /**
   * Reacts whenever a 'SplioRequestEvent::SPLIO_UPDATE' is triggered.
   *
   * Checks the received entity type and performs the proper actions for
   * each case before sending a request to Splio.
   *
   * @param \Drupal\splio\Event\SplioRequestEvent $event
   *   The Splio request event.
   *
   * @noinspection PhpUnused
   */
  public function splioRequestUpdate(SplioRequestEvent $event) {

    $splioEntity = $event->getSplioEntity();
    $action = $splioEntity['splioEntityType'] . 'RequestUpdate';

    if (method_exists($this, $action)) {
      $this->$action($event);
    }
  }

  /**
   * Reacts whenever a "SplioResponseEvent" is triggered.
   *
   * Checks the received entity type and performs the proper actions for
   * each case before after receiving a response from Splio.
   *
   * @param \Drupal\splio\Event\SplioResponseEvent $event
   *   The Splio request event.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException|\Throwable
   *
   * @noinspection PhpUnused
   */
  public function splioResponseReceived(SplioResponseEvent $event): void {

    $this->manageSubscriptionLists($event);
  }

  /**
   * Performs the required actions for contacts before a request is sent.
   *
   * @param \Drupal\splio\Event\SplioRequestEvent $event
   *   The Splio request event.
   *
   * @throws \Throwable
   *
   * @noinspection PhpUnused
   */
  public function contactsRequestCreate(SplioRequestEvent $event): void {

    $this->parseContactsFields($event);
  }

  /**
   * Performs the required actions for contacts before a request is sent.
   *
   * @param \Drupal\splio\Event\SplioRequestEvent $event
   *   The Splio request event.
   *
   * @throws \Throwable
   *
   * @noinspection PhpUnused
   */
  public function contactsRequestUpdate(SplioRequestEvent $event): void {

    $this->parseContactsFields($event);

    $keyField = $this->splioEntityHandler->getEntityTypeKeyField(SplioEntity::TYPE_CONTACTS);
    $result = $this->contactsKeyValueHasChanged($keyField, $event);

    if (TRUE === $result) {
      // We need to delete current contact in Splio,
      // create the same one, but with different key field,
      // and associate the orders from old contact to new one.
      $this->enqueueDeleteUserItem($event);

      $this->enqueueCreateUserItem($event);

      $this->enqueueReassignOrdersToNewUser($event);

      $drupalEntity = $event->getDrupalEntity();

      throw new UserModifyUniqueKeyFieldValueException(
        $drupalEntity->original->get($keyField)->value,
        $drupalEntity->get($keyField)->value
      );
    }
  }

  /**
   * Reacts whenever a 'SplioRequestEvent' is triggered for contacts.
   *
   * Parse Splio entity fields, that might be needed for its proper sync
   * with Splio.
   *  - contacts: swap "external_id" with actual key field.
   *
   * @param \Drupal\splio\Event\SplioRequestEvent $event
   *   The Splio request event.
   */
  private function parseContactsFields(SplioRequestEvent $event): void {

    $keyField = $this->splioEntityHandler->getEntityTypeKeyField(
      SplioEntity::TYPE_CONTACTS
    );
    $splioEntity = $event->getSplioEntity();
    $splioEntity[$keyField] = $splioEntity['external_id'];

    unset($splioEntity['external_id']);

    $event->alterSplioEntity($splioEntity);
  }

  /**
   * Checks user subscription lists.
   *
   * Splio handles user subscription automatically, when at least one list is
   * sent to its service. But if we send no lists, it is necessary to call the
   * subscription service.
   *
   * @param \Drupal\splio\Event\SplioResponseEvent $event
   *   Dispatched event.
   *
   * @throws \Throwable
   */
  private function manageSubscriptionLists(SplioResponseEvent $event): void {

    if (SplioEntity::TYPE_CONTACTS !== $event->getEntityType()) {
      return;
    }

    $entityStructure = $event->getSplioEntityStructure();
    $selectedLists = $this->contactsListsManager
      ->getIdListFromEntityStructure($entityStructure);
    $allLists = $this->contactsListsManager
      ->getIdsListFromStorage();
    $unsubscribeFrom = array_diff($allLists, $selectedLists);
    $keyField = $this->splioEntityHandler
      ->getEntityTypeKeyField(SplioEntity::TYPE_CONTACTS);

    if (\count($unsubscribeFrom) > 0) {
      $this->splioConnector
        ->unsubscribeFromLists($entityStructure[$keyField], $unsubscribeFrom);
    }
  }

  /**
   * Checks if contacts key field has been modified.
   *
   * Entities key field cannot be modified in Splio system, thus it is
   * necessary to check if it has been modified.
   *
   * @param string $keyField
   *   Entity key field.
   * @param \Drupal\splio\Event\SplioRequestEvent $event
   *   Event triggered.
   *
   * @return bool
   *   TRUE if key was modified; FALSE otherwise.
   */
  private function contactsKeyValueHasChanged(
    string $keyField,
    SplioRequestEvent $event
  ): bool {

    $drupalEntity = $event->getDrupalEntity();
    $splioEntity = $event->getSplioEntity();

    return ('update' === $splioEntity['action'])
      && !empty($drupalEntity->get($keyField)->value)
      && !empty($drupalEntity->original)
      && !empty($drupalEntity->original->get($keyField)->value)
      && ($drupalEntity->original->get($keyField)->value !== $drupalEntity->get($keyField)->value);
  }

  /**
   * Enqueues an item marked as to be deleted.
   *
   * @param \Drupal\splio\Event\SplioRequestEvent $event
   *   Event triggered.
   *
   * @throws \Drupal\splio\Exception\EntitiesNotConfiguredYetException
   * @throws \Drupal\splio\Exception\EntityIsEmptyException
   * @throws \Drupal\splio\Exception\EntityTypeNotFoundInSplioException
   * @throws \Drupal\splio\Exception\OriginalEntityNotFoundException
   */
  private function enqueueDeleteUserItem(SplioRequestEvent $event): void {

    $contact = $event->getDrupalEntity();

    if (empty($contact->original)) {
      throw new OriginalEntityNotFoundException();
    }

    try {
      $this->splioQueueHandler->addEntityToQueue($contact->original, 'delete');
    }
    catch (
      EntityTypeNotFoundInSplioException
      | EntitiesNotConfiguredYetException
      | EntityIsEmptyException
      $exception
    ) {
      $this->logger->error($exception->getMessage());

      throw new $exception();
    }
  }

  /**
   * Enqueues an item marked as to be created.
   *
   * @param \Drupal\splio\Event\SplioRequestEvent $event
   *   Event triggered.
   *
   * @throws \Drupal\splio\Exception\EntitiesNotConfiguredYetException
   * @throws \Drupal\splio\Exception\EntityTypeNotFoundInSplioException
   * @throws \Drupal\splio\Exception\EntityIsEmptyException
   */
  private function enqueueCreateUserItem(SplioRequestEvent $event): void {

    $contact = clone $event->getDrupalEntity();

    unset($contact->original);

    try {
      $this->splioQueueHandler->addEntityToQueue($contact, 'create');
    }
    catch (
      EntityTypeNotFoundInSplioException
      | EntitiesNotConfiguredYetException
      | EntityIsEmptyException
      $exception
    ) {
      $this->logger->error($exception->getMessage());

      throw new $exception();
    }
  }

  /**
   * Reassigns orders from old user to new one.
   *
   * If a new contact has been created, and "original" property has a value,
   * it means that the user changed their key field value,
   * so we previously deleted them, created a new one, and all their orders,
   * in Splio, has to be transferred to the new contact.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\splio\Exception\EntitiesNotConfiguredYetException
   * @throws \Drupal\splio\Exception\EntityIsEmptyException
   * @throws \Drupal\splio\Exception\EntityTypeNotFoundInSplioException
   */
  private function enqueueReassignOrdersToNewUser(
    SplioRequestEvent $event
  ): void {

    $keyField = $this->splioEntityHandler->getEntityTypeKeyField(
      SplioEntity::TYPE_CONTACTS
    );

    try {
      $entities = $this->splioEntityHandler->getLocalSplioEntitiesConfig();
    }
    catch (EntitiesNotConfiguredYetException $exception) {
      return;
    }

    $splioEntity = $event->getSplioEntity();
    $keyFieldValue = $this->getSplioEntityKeyFieldValue(
      $splioEntity,
      $keyField
    );
    $receiptContactFieldRelation = $this->getReceiptContactFieldRelation();
    $orderIds = $this->entityTypeManager
      ->getStorage($entities[SplioEntity::TYPE_RECEIPTS]['local_entity'])
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition($receiptContactFieldRelation, $keyFieldValue)
      ->execute();

    if (0 === \count($orderIds)) {
      return;
    }

    // Force enqueuing this orders, even though there are no changes.
    foreach ($orderIds as $orderId) {
      $order = $this->entityTypeManager
        ->getStorage($entities[SplioEntity::TYPE_RECEIPTS]['local_entity'])
        ->load($orderId);

      try {
        $this->splioQueueHandler->addEntityToQueue($order, 'update');
      }
      catch (
        EntityTypeNotFoundInSplioException
        | EntitiesNotConfiguredYetException
        | EntityIsEmptyException
        $exception
      ) {
        $this->logger->error($exception->getMessage());

        throw new $exception();
      }
    }
  }

  /**
   * Gets the key field value from a given entity.
   *
   * @param array $splioEntity
   *   Splio entity that we want to know its key field value.
   * @param string $keyField
   *   Key field.
   *
   * @return string
   *   Value of entity key field.
   */
  private function getSplioEntityKeyFieldValue(
    array $splioEntity,
    string $keyField
  ): string {

    return $splioEntity[$keyField] ?? $splioEntity['custom_fields'][$keyField]['value'];
  }

  /**
   * Get the Drupal field value that relations receipts and contacts.
   *
   * @return string
   *   The field with form xxx.yyy:zzz.aaa
   *
   * @todo Improve how we get the value, since it can be got directly from
   * Drupal system.
   */
  private function getReceiptContactFieldRelation(): string {
    $receiptFields = $this->splioEntityHandler
      ->getLocalSplioEntitiesFields(['splio_entity' => SplioEntity::TYPE_RECEIPTS]);
    /** @var \Drupal\splio\Entity\SplioFieldInterface $receiptContactField */
    $receiptContactField = $receiptFields['receipts_contact_id'];
    $receiptContactFieldRelation = $receiptContactField->getDrupalField();
    $parts = explode(".", $receiptContactFieldRelation);
    $parts[1] = "entity:" . $parts[1];

    return str_replace(['{{', '}}'], '', implode('.', $parts));
  }

}
