<?php

namespace Drupal\splio\Plugin\QueueWorker;

use Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\DelayedRequeueException;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\splio\Exception\EntitiesNotConfiguredYetException;
use Drupal\splio\Exception\QueueItemHasNotValidDataException;
use Drupal\splio\Exception\SuspendSplioQueueException;
use Drupal\splio\Services\ExceptionMessageParserInterface;
use Drupal\splio\Services\SplioConnector;
use Drupal\Core\Queue\SuspendQueueException;
use Drupal\splio\Event\SplioQueueEvent;
use Drupal\splio\Services\SplioEntityHandlerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Manages the Splio entity queue and starts the sync process on CRON run.
 *
 * @QueueWorker(
 *   id = "cron_splio_sync",
 *   title = @Translation("Cron Splio sync process"),
 *   cron = {"time" = 20}
 * )
 */
class SplioQueueController extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * Entity manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  private EntityTypeManager $entityManager;

  /**
   * Splio connector service.
   *
   * @var \Drupal\splio\Services\SplioConnector
   */
  private SplioConnector $splioConnector;

  /**
   * Event dispatcher.
   *
   * @var \Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher
   */
  private ContainerAwareEventDispatcher $eventDispatcher;

  /**
   * Logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  private LoggerInterface $logger;

  /**
   * Splio entity handler.
   *
   * @var \Drupal\splio\Services\SplioEntityHandlerInterface
   */
  private SplioEntityHandlerInterface $splioEntityHandler;

  /**
   * Exception message parser service.
   *
   * @var \Drupal\splio\Services\ExceptionMessageParserInterface
   */
  private ExceptionMessageParserInterface $exceptionMessageParser;

  /**
   * SplioQueueController constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entityManager
   *   The entityManager service.
   * @param \Drupal\splio\Services\SplioConnector $splioConnector
   *   The splioConnector service.
   * @param \Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher $eventDispatcher
   *   The event dispatched for Splio requests.
   * @param \Psr\Log\LoggerInterface $logger
   *   The loggerInterface service.
   * @param \Drupal\splio\Services\SplioEntityHandlerInterface $splioEntityHandler
   *   The Splio entity handler.
   * @param \Drupal\splio\Services\ExceptionMessageParserInterface $exceptionMessageParser
   *   The exception message parser.
   */
  public function __construct(
    EntityTypeManager $entityManager,
    SplioConnector $splioConnector,
    ContainerAwareEventDispatcher $eventDispatcher,
    LoggerInterface $logger,
    SplioEntityHandlerInterface $splioEntityHandler,
    ExceptionMessageParserInterface $exceptionMessageParser
  ) {
    $this->entityManager = $entityManager;
    $this->splioConnector = $splioConnector;
    $this->eventDispatcher = $eventDispatcher;
    $this->logger = $logger;
    $this->splioEntityHandler = $splioEntityHandler;
    $this->exceptionMessageParser = $exceptionMessageParser;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {

    return new static(
      $container->get('entity_type.manager'),
      $container->get('splio.splio_connector'),
      $container->get('event_dispatcher'),
      $container->get('logger.channel.splio'),
      $container->get('splio.entity_handler'),
      $container->get('splio.exception_parser')
    );
  }

  /**
   * Works on a single queue item.
   *
   * @param mixed $data
   *   The data that was passed to
   *   \Drupal\Core\Queue\QueueInterface::createItem() when the item was queued.
   *
   * @throws \Exception
   *   A QueueWorker plugin may throw an exception to indicate there was a
   *   problem. The cron process will log the exception, and leave the item in
   *   the queue to be processed again later.
   * @throws \Drupal\Core\Queue\SuspendQueueException
   *   More specifically, a SuspendQueueException should be thrown when a
   *   QueueWorker plugin is aware that the problem will affect all subsequent
   *   workers of its queue. For example, a callback that makes HTTP requests
   *   may find that the remote server is not responding. The cron process will
   *   behave as with a normal Exception, and in addition will not attempt to
   *   process further items from the current item's queue during the current
   *   cron run.
   * @throws \Drupal\Core\Queue\DelayedRequeueException
   *   When there is an error in Splio CRUD, the exception might point out to
   *   requeue the queue item, in order to process it later. In this case, and
   *   since the queue is handled by Drupal\ultimate_cron\QueueWorker, we must
   *   throw Drupal\Core\Queue\DelayedRequeueException.
   *
   * @see \Drupal\Core\Cron::processQueues()
   */
  public function processItem($data) {

    $this->checkData($data);

    try {
      $entities = $this->splioEntityHandler->getLocalSplioEntitiesConfig();
    }
    catch (EntitiesNotConfiguredYetException $exception) {
      throw new SuspendQueueException($exception->getMessage());
    }

    if (!array_key_exists($data['splioEntityType'], $entities)) {
      // At this point, we do not need to throw any exception.
      return;
    }

    $entityDrupalType = $entities[$data['splioEntityType']]['local_entity'];

    try {
      $entity = $this->entityManager
        ->getStorage($entityDrupalType)
        ->load($data['id']);

      if (
        isset($data['lang'])
        && !empty($entity)
        && $entity->hasTranslation($data['lang'])
      ) {
        $entity = $entity->getTranslation($data['lang']);
      }

      // In delete actions, the $entity might not exist, in these cases the
      // provided data['original'] entity will be used as the current entity.
      if (empty($entity) && !(empty($data['original']))) {
        $entity = $data['original'];
      }
    }
    catch (\Exception $exception) {
      $this->logger->error(
        "A problem occurred when trying to process the item: %message",
        [
          '%message' => $this->exceptionMessageParser->getMessage($exception),
        ]
      );

      return;
    }

    // If there is an original entity, add it to the current entity.
    if (!empty($data['original'])) {
      $entity->original = $data['original'];
    }

    // Manage the event to be dispatched.
    $queueEvent = new SplioQueueEvent($data);

    $this->eventDispatcher->dispatch(
      $queueEvent,
      SplioQueueEvent::SPLIO_DEQUEUE
    );

    // In case someone captured the event and made changes in the item,
    // update the item before inserting it into the queue.
    $data = $queueEvent->getSplioQueueItem();

    // Set the CRUD action to be performed by the SplioConnector service.
    $action = $data['action'] . 'Entities';

    if (method_exists($this->splioConnector, $action)) {
      if (empty($entity)) {
        $this->logger->warning(
        'Entity is empty with this data: ' . json_encode($data)
        );
        return;
      }

      $result = $this->splioConnector->$action([$entity]);

      if (end($result) instanceof \Exception) {
        $exception = end($result);
        $statusCode = $exception->getCode();

        if ($exception instanceof DelayedRequeueException) {
          throw $exception;
        }

        if (
          $exception instanceof EntitiesNotConfiguredYetException
          || $exception instanceof SuspendSplioQueueException
        ) {
          throw new SuspendQueueException($exception->getMessage());
        }

        if ($statusCode >= 500 && $statusCode <= 599) {
          throw new SuspendQueueException(
            'Splio server is not responding. Aborting sync until next cron job.'
          );
        }

        // Mind that the exception below will cause the queue to stop running
        // in case it was executed via drush queue-run. The queue is meant to
        // be handled by cron.
        $message = [
          'Message: ' . $this->exceptionMessageParser->getMessage($exception),
          'Code: ' . $statusCode,
          'Data: ' . json_encode($data),
        ];

        throw new \Exception(implode('. ', $message));
      }
    }
  }

  /**
   * Checks that queue item data is an array.
   *
   * @param mixed $data
   *   Queue item data.
   *
   * @throws \Drupal\splio\Exception\QueueItemHasNotValidDataException
   */
  private function checkData($data) {

    if (
      !is_array($data)
      || empty($data['splioEntityType'])
      || empty($data['id'])
      || empty($data['action'])
    ) {
      throw new QueueItemHasNotValidDataException();
    }
  }

}
