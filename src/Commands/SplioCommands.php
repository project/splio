<?php

namespace Drupal\splio\Commands;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\splio\Exception\EntitiesNotConfiguredYetException;
use Drupal\splio\Services\SplioEntityHandlerInterface;
use Drupal\splio\Services\SplioQueueHandlerInterface;
use Drush\Commands\DrushCommands;

/**
 * A Drush command file.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 */
class SplioCommands extends DrushCommands {

  use StringTranslationTrait;

  /**
   * Queue handler.
   *
   * @var \Drupal\splio\Services\SplioQueueHandlerInterface
   */
  private SplioQueueHandlerInterface $queueHandler;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private EntityTypeManagerInterface $entityTypeManager;

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  private ConfigFactory $configFactory;

  /**
   * Splio entity handler.
   *
   * @var \Drupal\splio\Services\SplioEntityHandlerInterface
   */
  private SplioEntityHandlerInterface $splioEntityHandler;

  /**
   * SplioConnector constructor.
   *
   * @param \Drupal\splio\Services\SplioQueueHandlerInterface $queueHandler
   *   Drupal queue item handler.
   * @param \Drupal\Core\Config\ConfigFactory $config
   *   Drupal config factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entityTypeManager.
   * @param \Drupal\splio\Services\SplioEntityHandlerInterface $splioEntityHandler
   *   The entity handler.
   */
  public function __construct(
    SplioQueueHandlerInterface $queueHandler,
    ConfigFactory $config,
    EntityTypeManagerInterface $entityTypeManager,
    SplioEntityHandlerInterface $splioEntityHandler
  ) {

    parent::__construct();
    $this->queueHandler = $queueHandler;
    $this->configFactory = $config;
    $this->entityTypeManager = $entityTypeManager;
    $this->splioEntityHandler = $splioEntityHandler;
  }

  /**
   * Adds the specified entity to the Splio queue.
   *
   * @param array $options
   *   An associative array of options whose values come from cli, aliases,
   *   config, etc.
   *
   * @option entity_type
   *   The type of the entity.
   * @option entity_id
   *   The id of the entity.
   * @option action
   *   The desired action. Accepted values: create, update, delete.
   * @usage drush splio-entity-enqueue --entity_type=bb_user --entity_id=123456
   *   --action=create Adds to the queue: 'create' the 'user' with id '123456'.
   * @usage drush spl-queue --entity_type=node --entity_id=654321
   *   --action=delete Adds to the queue: 'delete' the 'node' with id '654321'.
   *
   * @command splio:entity-enqueue
   * @aliases spl-queue,splio-entity-enqueue
   */
  public function entityEnqueue(array $options = [
    'entity_type' => NULL,
    'entity_id' => NULL,
    'action' => NULL,
  ]) {

    $error = FALSE;
    $entity = NULL;

    if (
      empty($options['entity_type'])
      || !$this->validateOptionEntityType($options['entity_type'])
    ) {
      $error = TRUE;
      $this
        ->logger
        ->error(
          $this->t(
            '--entity_type is not valid. Check your Splio module configuration and try again.'
          )
        );
    }

    try {
      $entity = $this->entityTypeManager
        ->getStorage($options['entity_type'])
        ->load($options['entity_id']);
    }
    catch (\Exception $e) {
      $error = TRUE;
      $this
        ->logger
        ->error(
          $this->t("The requested entity could not be loaded. Aborting...")
        );
    }

    if (!isset($entity)) {
      $error = TRUE;
      $this
        ->logger
        ->error(
          $this->t("--entity_id is not valid. Ensure the entity exists.")
        );
    }

    if (
      empty($options['action'])
      || !$this->validateOptionAction($options['action'])
    ) {
      $error = TRUE;
      $this
        ->logger
        ->error(
          $this->t('--action option is not valid. The only allowed values are "create", "update", "delete".')
        );
    }

    if (TRUE !== $error) {
      try {
        $this->queueHandler->addEntityToQueue($entity, $options['action']);
      }
      catch (\Exception $exception) {
        $this->logger->error(
          $this->t("Entity could not be added to Splio queue.")
        );
      }

      $this->logger->info($this->t("Entity added to Splio queue."));
    }
  }

  /**
   * Validate that the type entity_type is valid.
   *
   * @param string $entityType
   *   Entity type value from --entity_type command option.
   *
   * @return bool
   *   TRUE if it is valid; FALSE otherwise.
   */
  private function validateOptionEntityType(string $entityType): bool {

    try {
      $splioEntities = $this->splioEntityHandler->getLocalSplioEntitiesConfig();
    }
    catch (EntitiesNotConfiguredYetException $exception) {
      return FALSE;
    }

    foreach ($splioEntities as $definition) {
      $splioEntity = $definition['local_entity'];
      $splioBundle = $definition['local_entity_bundle'];

      if (
        $splioEntity != ''
        && (
          $splioEntity == $entityType
          || $splioBundle == $entityType
        )
      ) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * Validate the action option.
   *
   * @param string $action
   *   Action value from --action command option.
   *
   * @return bool
   *   TRUE if valid; FALSE otherwise.
   */
  private function validateOptionAction(string $action): bool {

    $validActions = [
      'create',
      'update',
      'delete',
    ];

    return in_array($action, $validActions);
  }

}
