<?php

namespace Drupal\splio\Entity;

use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\splio\Exception\EntitiesNotConfiguredYetException;
use Drupal\splio\Exception\EntityTypeNotFoundInSplioException;
use Drupal\splio\Services\SplioConnectorInterface;
use Drupal\splio\Services\SplioEntityHandlerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class for Splio entity.
 *
 * @package Drupal\splio\Entity
 */
class SplioEntity {

  // Used to provide dependency injection methods for serialization.
  use DependencySerializationTrait;

  /**
   * The Splio entity types.
   */
  const TYPE_CONTACTS = 'contacts';
  const TYPE_CONTACTS_LISTS = 'contacts_lists';
  const TYPE_PRODUCTS = 'products';
  const TYPE_RECEIPTS = 'receipts';
  const TYPE_ORDER_LINES = 'order_lines';
  const TYPE_STORES = 'stores';

  /**
   * Connector to Splio services.
   *
   * @var \Drupal\splio\Services\SplioConnectorInterface
   */
  private SplioConnectorInterface $splioConnector;

  /**
   * Splio entity handler.
   *
   * @var \Drupal\splio\Services\SplioEntityHandlerInterface
   */
  private SplioEntityHandlerInterface $splioEntityHandler;

  /**
   * Logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  private LoggerInterface $logger;

  /**
   * Stores the Splio entity type.
   *
   * @var string
   */
  private string $splioEntityType;

  /**
   * Stores the Drupal entity type.
   *
   * @var string|null
   */
  private ?string $localEntityType;

  /**
   * Stores the Drupal bundle type.
   *
   * @var string|null
   */
  private ?string $localBundleType;

  /**
   * Stores the name of the Splio entity type.
   *
   * @var string|null
   */
  private ?string $label;

  /**
   * Stores the configured Splio entity types.
   *
   * @var array
   */
  private array $splioEntityTypes;

  /**
   * Stores the entity fields that belong to a specific Splio entity.
   *
   * @var array
   */
  private array $splioEntityFields = [];

  public function __construct(
    SplioConnectorInterface $splioConnector,
    SplioEntityHandlerInterface $splioEntityHandler,
    LoggerInterface $logger,
    string $splioEntityType
  ) {
    $this->splioConnector = $splioConnector;
    $this->splioEntityHandler = $splioEntityHandler;
    $this->logger = $logger;

    try {
      $this->setupEntity($splioEntityType);
    }
    catch (
      EntityTypeNotFoundInSplioException
      | EntitiesNotConfiguredYetException
      $exception
    ) {
      // Do nothing.
    }
  }

  /**
   * Static create.
   */
  public static function create(ContainerInterface $container): SplioEntity {
    return new static(
      $container->get('splio.splio_connector'),
      $container->get('splio.entity_handler'),
      $container->get('logger.channel.splio'),
      $container->get('path.current')
    );
  }

  /**
   * Sets up the proper Splio entity type.
   *
   * @param string $splioEntityType
   *   Splio entity type.
   *
   * @throws \Drupal\splio\Exception\EntityTypeNotFoundInSplioException
   * @throws \Drupal\splio\Exception\EntitiesNotConfiguredYetException
   */
  private function setupEntity(string $splioEntityType): void {

    $this->splioEntityTypes = $this
      ->splioEntityHandler
      ->getLocalSplioEntitiesConfig();
    $this->splioEntityType = $splioEntityType;
    $this->localEntityType = NULL;
    $this->localBundleType = NULL;
    $this->label = NULL;

    if (empty($this->splioEntityTypes[$splioEntityType])) {
      throw new EntityTypeNotFoundInSplioException($this->splioEntityType);
    }

    $entityData = $this->splioEntityTypes[$this->splioEntityType];

    if (!empty($entityData['local_entity'])) {
      $this->localEntityType = $entityData['local_entity'];
    }

    if (!empty($entityData['local_entity_bundle'])) {
      $this->localBundleType = $entityData['local_entity_bundle'];
    }

    if (!empty($entityData['label'])) {
      $this->label = $entityData['label'];
    }
  }

  /**
   * Returns the splio type of the entity.
   *
   * @return string
   *   String containing the splio entity type.
   */
  public function getSplioEntityType(): string {
    return $this->splioEntityType;
  }

  /**
   * Returns the local type of the entity.
   *
   * @return string|null
   *   String containing the local entity type.
   */
  public function getLocalEntityType(): ?string {
    return $this->localEntityType;
  }

  /**
   * Returns the label of the entity.
   *
   * @return string|null
   *   String containing the entity name.
   */
  public function getLabel(): ?string {
    return $this->label;
  }

  /**
   * Returns all the default fields for the given SplioEntity.
   *
   * @return array
   *   Array containing the default splio fields of the entity.
   */
  public function getEntityFields(): array {
    return $this->splioEntityFields;
  }

  /**
   * Returns the local bundle type of the entity.
   *
   * @return string|null
   *   String containing the local entity type.
   */
  public function getLocalBundleType(): ?string {
    return $this->localBundleType;
  }

}
