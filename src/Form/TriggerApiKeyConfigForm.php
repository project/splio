<?php

namespace Drupal\splio\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\splio\Services\SplioConnector;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class to configure Splio Trigger API connection.
 */
class TriggerApiKeyConfigForm extends ConfigFormBase {

  const SETTINGS = 'splio.splio_trigger.settings';

  const MAX_TIMEOUT = 1;

  /**
   * Config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private ConfigFactoryInterface $config_factory;

  /**
   * Splio connector service.
   *
   * @var \Drupal\splio\Services\SplioConnector
   */
  private SplioConnector $splioConnector;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    SplioConnector $splioConnector
  ) {

    parent::__construct($config_factory);

    $this->config_factory = $config_factory;
    $this->splioConnector = $splioConnector;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {

    return [
      self::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {

    return new static(
      $container->get('config.factory'),
      $container->get('splio.splio_connector')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {

    return 'splio_trigger_api_key_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config(self::SETTINGS);

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['key_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Trigger API Key settings'),
    ];

    $form['actions']['key_settings']['splio_config'] = [
      '#type' => 'key_select',
      '#title' => $this->t('Select your Splio Trigger API Key:'),
      '#key_filters' => ['type_group' => 'authentication'],
      '#default_value' => $config->get('splio_config'),
      '#required' => TRUE,
    ];

    $form['actions']['server_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Server settings'),
    ];

    $form['actions']['server_settings']['splio_server'] = [
      '#type' => 'radios',
      '#title' => $this->t('Select the Splio API server:'),
      '#default_value' => $config->get('splio_server'),
      '#options' => [
        's3s.fr/api/trigger/nph-10.pl' => $this->t('v10 (https://s3s.fr/api/trigger/nph-10.pl)'),
      ],
      '#required' => TRUE,
    ];

    $form['actions']['timeout'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Timeouts'),
    ];

    $form['actions']['timeout']['connection_timeout'] = [
      '#type' => 'select',
      '#title' => $this->t('Select the Splio Trigger API server connection timeout (seconds)'),
      '#default_value' => $config->get('connection_timeout') ?? self::MAX_TIMEOUT,
      '#options' => $this->generateTimeout(),
      '#required' => TRUE,
    ];

    $form['actions']['timeout']['response_timeout'] = [
      '#type' => 'select',
      '#title' => $this->t('Select the Splio Trigger API server response timeout (seconds)'),
      '#default_value' => $config->get('response_timeout') ?? self::MAX_TIMEOUT,
      '#options' => $this->generateTimeout(),
      '#required' => TRUE,
    ];

    $form['actions']['test_connection']['#ajax'] = [
      'callback' => [$this, 'testTriggerApiConnection'],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Retrieve the configuration from file and set new values.
    $this->configFactory->getEditable(self::SETTINGS)
      ->set('splio_config', $form_state->getValue('splio_config'))
      ->set('splio_server', $form_state->getValue('splio_server'))
      ->set('connection_timeout', $form_state->getValue('connection_timeout'))
      ->set('response_timeout', $form_state->getValue('response_timeout'))
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Tests the connection with the Trigger API for the selected configuration.
   *
   * @param array $form
   *   The current form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse|mixed|\Psr\Http\Message\ResponseInterface
   *   Reloads the page printing a message with the test connection result.
   */
  public function testTriggerApiConnection(
    array $form,
    FormStateInterface $form_state
  ) {

    $config = [
      'key' => $form_state->getValue('splio_config'),
      'server' => $form_state->getValue('splio_server'),
      'connection_timeout' => $form_state->getValue('connection_timeout'),
      'timeout' => $form_state->getValue('response_timeout'),
    ];

    $response = $this->splioConnector->ping($config);
    $msg = ($response)
      ? [
        $this->messenger()->addStatus(
          $this->t('Successfully connected to Splio Trigger!')
        ),
        "status",
      ]
      : [
        $this->messenger()->addError(
            $this->t(
              'Could not connect to Splio Trigger. Check your Splio Trigger configuration and try again. Visit the <a href="@help">help page</a> for further information.',
              ['@help' => '/admin/help/splio']
            )
        ),
        "error",
      ];

    $this->messenger()->addMessage('Trigger API Key: ' . $config['key'], end($msg));
    $this->messenger()->addMessage('Server: ' . $config['server'], end($msg));

    $response = new AjaxResponse();
    $currentURL = Url::fromRoute('<current>');
    $response->addCommand(new RedirectCommand($currentURL->toString()));

    return $response;
  }

  /**
   * Push to an array from 0 to 10, to set a range of positive timeouts.
   *
   * @return array
   *   Values for possible timeouts, from 0 to 10.
   */
  private function generateTimeout(): array {

    $timeout = [];

    foreach (range(0, 10) as $seconds) {
      $timeout[$seconds] = $seconds;
    }

    return $timeout;
  }

}
