<?php

namespace Drupal\splio\Form;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfo;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Entity\EntityTypeRepository;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Menu\LocalTaskManager;
use Drupal\splio\Entity\SplioEntity;
use Drupal\splio\Exception\EntitiesNotConfiguredYetException;
use Drupal\splio\Services\SplioEntityHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class to handle Splio entity types and store them in config.
 */
class EntitiesConfigForm extends ConfigFormBase {

  /**
   * The Splio settings.
   *
   * @var string
   */
  const SETTINGS = 'splio.entity.config';

  const DEFAULT_KEY_FIELD = 'extid';

  /**
   * {@inheritdoc}
   */
  protected $configFactory;

  /**
   * Entity type repository.
   *
   * @var \Drupal\Core\Entity\EntityTypeRepository
   */
  protected EntityTypeRepository $entityTypeRepository;

  /**
   * Entity type bundle info.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfo
   */
  protected EntityTypeBundleInfo $entityTypeBundleInfo;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected EntityTypeManager $entityTypeManager;

  /**
   * Local task manager.
   *
   * @var \Drupal\Core\Menu\LocalTaskManager
   */
  protected LocalTaskManager $localTaskManager;

  /**
   * Cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected CacheBackendInterface $cache;

  /**
   * Splio entity handler.
   *
   * @var \Drupal\splio\Services\SplioEntityHandlerInterface
   */
  protected SplioEntityHandlerInterface $splioEntityHandler;

  /**
   * The current Splio entity.
   *
   * @var string
   */
  protected string $currentSplioEntity;

  /**
   * Stores the entity type labels.
   *
   * @var array
   */
  protected array $contentEntities;

  /**
   * Fixed array with Splio entity types names.
   *
   * @var array
   */
  protected array $splioEntityLabel;

  /**
   * EntityConfigForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory to manage local configuration.
   * @param \Drupal\Core\Entity\EntityTypeRepository $entityTypeRepository
   *   Entity type repository to get access to the entity repository.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfo $entityTypeBundleInfo
   *   Entity type bundle info to get access to the bundles of each entity.
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   Entity type manager to get access to the entity manager.
   * @param \Drupal\Core\Menu\LocalTaskManager $localTaskManager
   *   Local task manager to clear the tasks cache.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   Drupal cache to manage the cached data.
   * @param \Drupal\splio\Services\SplioEntityHandlerInterface $splioEntityHandler
   *   Splio entity handler.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    EntityTypeRepository $entityTypeRepository,
    EntityTypeBundleInfo $entityTypeBundleInfo,
    EntityTypeManager $entityTypeManager,
    LocalTaskManager $localTaskManager,
    CacheBackendInterface $cache,
    SplioEntityHandlerInterface $splioEntityHandler
  ) {

    parent::__construct($config_factory);

    $this->entityTypeRepository = $entityTypeRepository;
    $this->entityTypeBundleInfo = $entityTypeBundleInfo;
    $this->entityTypeManager = $entityTypeManager;
    $this->localTaskManager = $localTaskManager;
    $this->cache = $cache;
    $this->splioEntityHandler = $splioEntityHandler;
    $this->splioEntityLabel = [
      SplioEntity::TYPE_CONTACTS => $this->t('Contacts'),
      SplioEntity::TYPE_PRODUCTS => $this->t('Products'),
      SplioEntity::TYPE_RECEIPTS => $this->t('Receipts'),
      SplioEntity::TYPE_ORDER_LINES => $this->t('Order lines'),
      SplioEntity::TYPE_STORES => $this->t('Stores'),
      SplioEntity::TYPE_CONTACTS_LISTS => $this->t('Contacts lists'),
    ];
  }

  /**
   * EntityConfigForm container creator.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Global container for EntityConfigForm.
   *
   * @return \Drupal\Core\Form\ConfigFormBase|\Drupal\splio\Form\EntitiesConfigForm
   *   Returns the config form with the injected services.
   */
  public static function create(ContainerInterface $container) {

    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.menu.local_task'),
      $container->get('cache.default'),
      $container->get('splio.entity_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {

    return [self::SETTINGS];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {

    return 'splio_entity_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(
    array $form,
    FormStateInterface $form_state
  ): array {

    $localEntityTypes = [];
    $form['entity_config'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Entity configuration'),
    ];
    $form['entity_config']['entity_config_table'] = [
      '#type' => 'table',
      '#caption' => $this->t("Configure the mapping between Splio and your site's entities."),
      '#header' => [
        'splio_entity' => $this->t('Splio entity'),
        'local_entity' => $this->t('Local entity'),
        'local_entity_bundle' => $this->t('Local entity bundle'),
      ],
      '#attributes' => [
        'id' => 'local_entity',
      ],
    ];
    $this->contentEntities = $this
      ->entityTypeRepository
      ->getEntityTypeLabels(TRUE)['Content'];

    try {
      $localEntityTypes = $this
        ->splioEntityHandler
        ->getLocalSplioEntitiesConfig();
    }
    catch (EntitiesNotConfiguredYetException $exception) {
      $this->messenger()->addWarning($exception->getMessage());
    }

    foreach ($this->splioEntityLabel as $key => $name) {
      // Contacts lists are managed within contacts.
      if (SplioEntity::TYPE_CONTACTS_LISTS === $key) {
        continue;
      }

      $this->currentSplioEntity = $key;
      $form['entity_config']['entity_config_table'][$key]['splio_entity'] = [
        '#type' => 'label',
        '#title' => $name ?? '',
        '#default_value' => $name ?? '',
      ];
      $form['entity_config']['entity_config_table'][$key]['local_entity'] = [
        '#type' => 'select',
        '#options' => $this->contentEntities,
        '#empty_value' => '',
        '#default_value' => $this->getLocalEntityDefaultValue(
          $key,
          $localEntityTypes
        ),
        '#ajax' => [
          'callback' => [$this, 'updateBundleSelect'],
          'event' => 'change',
          'wrapper' => 'local_entity',
          'progress' => [
            'type' => NULL,
            'message' => NULL,
          ],
        ],
      ];
      $userInput = $form_state->getUserInput();
      $inputLocalEntityIsEmpty = empty(
        $userInput['entity_config_table'][$key]['local_entity']
      );
      $entityUserInput = $this->getLocalEntityFromUserInput(
        $inputLocalEntityIsEmpty,
        $key,
        $localEntityTypes,
        $form_state
      );
      $defaultValue = $this->getLocalEntityBundleDefaultValue(
        $inputLocalEntityIsEmpty,
        $key,
        $localEntityTypes
      );
      $bundles = $this->getEntityBundles($entityUserInput);
      $form['entity_config']['entity_config_table'][$key]['local_entity_bundle'] = [
        '#type' => 'select',
        '#options' => $bundles,
        '#empty_value' => '',
        '#default_value' => $defaultValue,
        '#validated' => TRUE,
        '#disabled' => empty($bundles),
      ];

      if (empty($bundles)) {
        $form['entity_config']['entity_config_table'][$key]['local_entity_bundle']['#attributes'] = ['style' => 'color:grey;'];
      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * Gets local entity type from form user input.
   *
   * If empty, tries to get it from local storage.
   *
   * @param bool $inputLocalEntityIsEmpty
   *   Tells if input is empty.
   * @param string $splioEntityType
   *   Current entity type checked.
   * @param array|null $localEntityTypes
   *   Entity types from storage.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return string|null
   *   Local entity value.
   */
  protected function getLocalEntityFromUserInput(
    bool $inputLocalEntityIsEmpty,
    string $splioEntityType,
    ?array $localEntityTypes,
    FormStateInterface $form_state
  ): ?string {
    if (!$inputLocalEntityIsEmpty) {
      return $form_state->getUserInput()['entity_config_table'][$splioEntityType]['local_entity'];
    }

    if (!empty($localEntityTypes[$splioEntityType]['local_entity'])) {
      return $localEntityTypes[$splioEntityType]['local_entity'];
    }

    return NULL;
  }

  /**
   * Gets local entity default value from form user input.
   *
   * If empty, tries to get it from local storage.
   *
   * @param string $splioEntityType
   *   Current entity type checked.
   * @param array|null $localEntityTypes
   *   Entity types from storage.
   *
   * @return string
   *   Default value.
   */
  protected function getLocalEntityDefaultValue(
    string $splioEntityType,
    ?array $localEntityTypes
  ): string {

    if (
      !empty($localEntityTypes[$splioEntityType])
      && !empty($localEntityTypes[$splioEntityType]['local_entity'])
    ) {
      return $localEntityTypes[$splioEntityType]['local_entity'];
    }

    return '';
  }

  /**
   * Gets local entity bundle default value from form user input.
   *
   * If empty, tries to get it from local storage.
   *
   * @param bool $inputLocalEntityIsEmpty
   *   Tells if input is empty.
   * @param string $splioEntityType
   *   Current entity type checked.
   * @param array|null $localEntityTypes
   *   Entity types from storage.
   *
   * @return string|null
   *   Local entity bundle default value.
   */
  protected function getLocalEntityBundleDefaultValue(
    bool $inputLocalEntityIsEmpty,
    string $splioEntityType,
    ?array $localEntityTypes
  ): string {
    if ($inputLocalEntityIsEmpty || empty($data['local_entity_bundle'])) {
      return '';
    }

    return $localEntityTypes[$splioEntityType]['local_entity_bundle'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $splioEntitiesArray = [];
    $entityConfigTable = $form_state->getUserInput()['entity_config_table'];

    foreach ($this->splioEntityLabel as $key => $name) {
      $localEntity = '';
      $localEntityBundle = '';

      if (!empty($entityConfigTable[$key]['local_entity'])) {
        $localEntity = $entityConfigTable[$key]['local_entity'];
      }

      if (!empty($splioEntities[$key]['local_entity_bundle'])) {
        $localEntityBundle = $splioEntities[$key]['local_entity_bundle'];
      }

      $splioEntitiesArray[$key]['label'] = (string) $this->splioEntityLabel[$key];
      $splioEntitiesArray[$key]['local_entity'] = $localEntity;
      $splioEntitiesArray[$key]['local_entity_bundle'] = $localEntityBundle;
      $splioEntitiesArray[$key]['splio_entity_key_field'] = self::DEFAULT_KEY_FIELD;

      if (SplioEntity::TYPE_CONTACTS === $key && !empty($entityConfigTable[SplioEntity::TYPE_CONTACTS]['local_entity'])) {
        $splioEntitiesArray[SplioEntity::TYPE_CONTACTS_LISTS]['label'] = $this->splioEntityLabel[SplioEntity::TYPE_CONTACTS_LISTS];
        $splioEntitiesArray[SplioEntity::TYPE_CONTACTS_LISTS]['local_entity'] = $splioEntitiesArray[SplioEntity::TYPE_CONTACTS]['local_entity'];
        $splioEntitiesArray[SplioEntity::TYPE_CONTACTS_LISTS]['local_entity_bundle'] = $splioEntitiesArray[SplioEntity::TYPE_CONTACTS]['local_entity_bundle'];
        $splioEntitiesArray[SplioEntity::TYPE_CONTACTS_LISTS]['splio_entity_key_field'] = NULL;
      }

      // Remove the SplioFields in case a SplioEntity is set to none.
      if (empty($entityConfigTable[$key]['local_entity'])) {
        $entityFields = $this->splioEntityHandler
          ->getLocalSplioEntitiesFields(
            ['splio_entity' => $key]
          );

        $this->entityTypeManager
          ->getStorage('splio_field')
          ->delete($entityFields);

        $splioEntitiesArray[$key]['splio_entity_key_field'] = '';

        $this->cache->delete('entity_fields_' . $key);
      }
    }

    $this->configFactory
      ->getEditable(self::SETTINGS)
      ->set('splio_entities', $splioEntitiesArray)
      ->save();

    $this->localTaskManager->clearCachedDefinitions();

    parent::submitForm($form, $form_state);
  }

  /**
   * Updates the select bundle items based on the entity selected by the user.
   *
   * @param array $form
   *   The current form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form-state.
   *
   * @return array
   *   Returns the updated form.
   */
  public function updateBundleSelect(
    array $form,
    FormStateInterface $form_state
  ): array {

    $splioEntities = [];

    try {
      $splioEntities = $this->splioEntityHandler->getLocalSplioEntitiesConfig();
    }
    catch (EntitiesNotConfiguredYetException $exception) {
      // Do nothing.
    }

    $config = $this->config(self::SETTINGS)->get('splio_entities');

    foreach ($splioEntities as $splioEntity) {
      $entityUserInput = $form_state
        ->getUserInput()['entity_config_table'][$splioEntity]['local_entity'];

      if ($splioEntity === $this->currentSplioEntity) {
        $form['entity_config']['entity_config_table'][$splioEntity]['local_entity_bundle'] = [
          '#type' => 'select',
          '#options' => '',
          '#empty_value' => '',
          '#default_value' => '',
          '#disabled' => TRUE,
        ];
        $form['entity_config']['entity_config_table'][$splioEntity]['local_entity_bundle']['#attributes'] = ['style' => 'color:grey;'];
      }

      // Some UI changes so the form is more intuitive.
      if (
        (empty($entityUserInput) && !empty($config[$splioEntity]['local_entity']))
        || ($entityUserInput != $config[$splioEntity]['local_entity'] && !empty($config[$splioEntity]['local_entity']))
      ) {
        $form['entity_config']['entity_config_table'][$splioEntity]['splio_entity']['#attributes'] = ['style' => 'color:red;'];
        $form['entity_config']['entity_config_table'][$splioEntity]['local_entity']['#description'] = '<span>' . $this
          ->t("Saved config for %splioEntity will be lost!", ['%splioEntity' => $this->splioEntityLabel[$splioEntity]]) . '</span>';
        $form['entity_config']['entity_config_table'][$splioEntity]['local_entity']['#attributes'] = ['style' => 'color:red;'];
        $form['entity_config']['entity_config_table'][$splioEntity]['local_entity_bundle']['#disabled'] = TRUE;
      }
    }

    return $form['entity_config']['entity_config_table'];
  }

  /**
   * Returns the bundles for the selected Entity.
   *
   * In case no bundles are returned, returns an empty array.
   *
   * @param mixed $entity_type_id
   *   Entity id.
   *
   * @return array
   *   Array containing the bundles.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getEntityBundles($entity_type_id): array {
    $bundles = [];

    if (empty($entity_type_id) || is_array($entity_type_id)) {
      return $bundles;
    }

    $allBundles = $this->entityTypeBundleInfo->getBundleInfo($entity_type_id);

    foreach ($allBundles as $key => $bundle) {
      $label = end($bundle);

      if (!is_object($label)) {
        $bundles[$this->entityTypeManager
          ->getDefinition($entity_type_id)
          ->getBundleLabel()][$key] = $label;
      }
      else {
        return [];
      }
    }

    return $bundles;
  }

}
