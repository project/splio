<?php

namespace Drupal\splio\Form;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\field\Entity\FieldConfig;
use Drupal\splio\Entity\SplioEntity;
use Drupal\splio\Exception\EntitiesNotConfiguredYetException;
use Drupal\splio\Services\SplioConnector;
use Drupal\splio\Services\SplioConnectorInterface;
use Drupal\splio\Services\SplioEntityHandlerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Form handler for the SplioField to edit fields.
 */
class SplioFieldForm extends EntityForm {

  const SPLIO_ID_FIELD = 'id';

  const PREFIX_CONTACT_LIST = 'contacts_lists_';

  /**
   * Entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  private EntityFieldManager $entityFieldManager;

  /**
   * Current path stack.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  private CurrentPathStack $currentPathStack;

  /**
   * Splio connector.
   *
   * @var \Drupal\splio\Services\SplioConnectorInterface
   */
  private SplioConnectorInterface $splioConnector;

  /**
   * Splio entity handler.
   *
   * @var \Drupal\splio\Services\SplioEntityHandlerInterface
   */
  private SplioEntityHandlerInterface $splioEntityHandler;

  /**
   * Logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  private LoggerInterface $logger;

  /**
   * The Splio entity configured by the form.
   *
   * @var \Drupal\splio\Entity\SplioEntity
   */
  private SplioEntity $splioEntity;

  /**
   * The Splio entity being managed currently by the form.
   *
   * @var string
   */
  private string $currentEntity;

  /**
   * The available fields for the current Splio entity.
   *
   * @var array
   */
  private array $splioEntityFields = [];

  /**
   * Array of local entity fields.
   *
   * @var array
   */
  private array $localConfigEntitiesFields = [];

  /**
   * Stores the entity being managed currently by the form.
   *
   * @var array
   */
  private array $fieldOptions = [];

  /**
   * Splio lists to which the contacts can be subscribed.
   *
   * @var \Drupal\splio\Entity\SplioEntity
   */
  private $contactsLists = [];

  /**
   * Local config entities for contacts lists.
   *
   * @var array
   */
  private $localConfigEntitiesContactsLists = [];

  /**
   * Splio lists -remotely fetched- to which the contacts can be subscribed.
   *
   * @var array
   */
  private $remoteContactsLists = [];

  /**
   * Constructs an SplioEntityConfigForm object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entityTypeManager.
   * @param \Drupal\Core\Entity\EntityFieldManager $entityFieldManager
   *   The entityFieldManager.
   * @param \Drupal\Core\Path\CurrentPathStack $currentPathStack
   *   The currentPathStack.
   * @param \Drupal\splio\Services\SplioConnector $splioConnector
   *   The splioConnector service.
   * @param \Drupal\splio\Services\SplioEntityHandlerInterface $splioEntityHandler
   *   Splio entity handler.
   * @param \Psr\Log\LoggerInterface $logger
   *   Logger.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    EntityFieldManager $entityFieldManager,
    CurrentPathStack $currentPathStack,
    SplioConnector $splioConnector,
    SplioEntityHandlerInterface $splioEntityHandler,
    LoggerInterface $logger
  ) {

    $this->entityTypeManager = $entityTypeManager;
    $this->entityFieldManager = $entityFieldManager;
    $this->currentPathStack = $currentPathStack;
    $this->splioConnector = $splioConnector;
    $this->splioEntityHandler = $splioEntityHandler;
    $this->logger = $logger;

    $this->checkEntityConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container
  ): SplioFieldForm {

    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('path.current'),
      $container->get('splio.splio_connector'),
      $container->get('splio.entity_handler'),
      $container->get('logger.channel.splio'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {

    $form = parent::form($form, $form_state);
    $userInput = $form_state->getUserInput();

    $this->setupForm();

    // The form container.
    $form['entity_fields'] =
      [
        '#type' => 'fieldset',
        '#title' => $this->currentEntity . " " . $this->t("fields configuration"),
      ];

    // The general fields table.
    $form['entity_fields']['entity_fields_table'] =
      [
        '#type' => 'table',
        '#caption' => $this->t(
          "Configure the mapping between the Splio's %entity fields and your site's fields.\nPlease, remember to save this form, and export the configuration, whenever you modify any Splio's %entity field.",
          ['%entity' => $this->currentEntity]
        ),
        '#attributes' => [
          'id' => 'entity_fields',
        ],
      ];

    $form['entity_fields']['entity_fields_table']['#header'][] = $this->t('Splio field');
    $form['entity_fields']['entity_fields_table']['#header'][] = $this->t('Splio type');
    $form['entity_fields']['entity_fields_table']['#header'][] = $this->t('Local field');
    $form['entity_fields']['entity_fields_table']['#header'][] = $this->t('Local type');

    if (SplioEntity::TYPE_ORDER_LINES !== $this->currentEntity) {
      $form['entity_fields']['entity_fields_table']['#header'][] = $this->t('Is key');
      $form['entity_fields']['radios'] = [
        '#type' => 'tableselect',
        '#multiple' => FALSE,
        '#header' => [],
      ];
    }

    $form['entity_fields']['entity_fields_table']['#header'][] = '';

    // Create a row for each Splio field.
    foreach ($this->splioEntityFields as $splioField) {
      if (self::SPLIO_ID_FIELD === $splioField['field_key']) {
        continue;
      }

      // Default fields are presented slightly different from custom ones.
      if ($splioField['is_system']) {
        // Splio field label.
        $form['entity_fields']['entity_fields_table'][$splioField['field_key']]['splio_field'] = [
          '#type' => 'label',
          '#title' => $splioField['label'],
        ];
      }
      else {
        // Splio field text field.
        $form['entity_fields']['entity_fields_table'][$splioField['field_key']]['splio_field'] = [
          '#type' => 'processed_text',
          '#text' => $splioField['label'],
        ];
      }

      // Default / custom field select.
      $form['entity_fields']['entity_fields_table'][$splioField['field_key']]['is_system'] = [
        '#type' => 'processed_text',
        '#text' => $splioField['is_system'] ? $this->t('Default field') : $this->t('Custom field'),
      ];

      // Drupal field correspondence.
      $form['entity_fields']['entity_fields_table'][$splioField['field_key']]['drupal_field'] = [
        '#type' => 'select',
        '#options' => $this->fieldOptions,
        '#empty_value' => '',
        '#default_value' => $this->getFormFieldDefaultValue($splioField),
      ];

      // Type field select.
      $form['entity_fields']['entity_fields_table'][$splioField['field_key']]['type_field'] = [
        '#type' => 'processed_text',
        '#text' => $splioField['data_type'] ?? 'text',
      ];

      // Order lines have no key field, so its column should not be presented.
      if (SplioEntity::TYPE_ORDER_LINES !== $this->currentEntity) {
        // Key field radio.
        $form['entity_fields']['entity_fields_table'][$splioField['field_key']]['is_key_field'] = [
          '#type' => 'radio',
          '#attributes' => [
            'name' => 'radios',
            'value' => $splioField['field_key'],
          ],
        ];

        // If the form is submitted.
        if (\count($userInput) > 0) {
          if (
            // If the key field is selected by user.
            'Save' === $userInput['op']
            && !empty($userInput['radios'])
            && $userInput['radios'] === $splioField['field_key']
          ) {
            $form['entity_fields']['entity_fields_table'][$splioField['field_key']]['is_key_field']['#default_value'] = 1;
            $form['entity_fields']['entity_fields_table'][$splioField['field_key']]['is_key_field']['#attributes']['checked'] = 'checked';
          }
        }
        // Accessing the form.
        elseif ($this->getFormFieldIsKeyField($splioField)) {
          $form['entity_fields']['entity_fields_table'][$splioField['field_key']]['is_key_field']['#default_value'] = 1;
          $form['entity_fields']['entity_fields_table'][$splioField['field_key']]['is_key_field']['#attributes']['checked'] = 'checked';
        }
      }

      // Type hidden field.
      $form['entity_fields']['entity_fields_table'][$splioField['field_key']]['type_hidden'] =
        [
          '#type' => 'hidden',
          '#value' => $splioField['data_type'] ?? 'text',
        ];
    }

    $form['entity_fields']['actions'] = [
      '#type' => 'actions',
    ];

    // Contacts have an extra form that appears below the regular fields form.
    if (SplioEntity::TYPE_CONTACTS === $this->currentEntity) {
      $form += $this->generateContactsListForm($form);
    }

    // Add some prefix and sufix to make all the forms work together.
    $form['#prefix'] = "<div id='splio-field-form-wrapper'>";
    $form['#suffix'] = "</div>";

    $form_state->setValue('splioEntity', $this->splioEntity);

    return $form;
  }

  /**
   * Sets up the variables that will be used in the form.
   *
   * TODO: At the moment, only one-level entityReferences are allowed to be set
   *   in each field. In the future could be interesting to allow an undefined
   *   number of recursive entityReferences for every field.
   */
  private function setupForm() {

    $this->currentEntity = $this->splioEntity->getSplioEntityType();
    $this->splioEntityFields = $this->getSplioEntityFields();
    $localEntity = $this->splioEntity->getLocalEntityType();
    $localBundleEntity = $this->splioEntity->getLocalBundleType();
    $fieldOptions = [];
    $baseFieldDefinitions = $this->entityFieldManager
      ->getFieldDefinitions($localEntity, $localBundleEntity);
    $this->localConfigEntitiesFields = $this->splioEntityHandler
      ->getLocalSplioEntitiesFields(
        ['splio_entity' => $this->splioEntity->getSplioEntityType()]
      );

    foreach ($baseFieldDefinitions as $fieldName => $fieldDef) {
      $fieldOptions['Fields'][$fieldDef->getName()] = $fieldDef->getName();

      // In case the current field is an entity reference, load all the
      // subfields that belong to the entity reference.
      if ("entity_reference" === $fieldDef->getType()) {
        // Get the target type of the field.
        $entityReferenceType = $fieldDef
          ->getFieldStorageDefinition()
          ->getSetting('target_type');

        // If the current field is a FieldConfig, load its bundle.
        $entityReferenceBundle = ($fieldDef instanceof FieldConfig)
          ? $fieldDef->get('bundle') : NULL;

        try {
          $this->entityFieldManager->getBaseFieldDefinitions($entityReferenceType);
        }
        // If a logic exception is thrown use the target entity type id,
        // instead of the target_type setting.
        catch (\LogicException $logicException) {
          $entityReferenceType = $fieldDef
            ->getFieldStorageDefinition()
            ->getTargetEntityTypeId();
        }
        finally {
          // Finally, after properly defining the entity reference type, if the
          // field has no referenceBundle, load its base fields.
          if (empty($entityReferenceBundle)) {
            $entityReferenceFields = $this
              ->entityFieldManager
              ->getBaseFieldDefinitions($entityReferenceType);
          }
          // In any other case, the field has a referenceBundle so load its
          // field definitions for the entity and its bundle.
          else {
            $entityReferenceFields = $this
              ->entityFieldManager
              ->getFieldDefinitions($entityReferenceType, $entityReferenceBundle);
          }
        }

        // Since the current entity is an entity reference, add each field of
        // the referenced entity to the array.
        foreach ($entityReferenceFields as $entityReferenceDef) {
          $key = 'Entity: ' . $entityReferenceType . " ({$fieldName})";
          $reference = "{{{$fieldName}.{$entityReferenceType}.{$entityReferenceDef->getName()}}}";
          $fieldOptions[$key][$reference] = $entityReferenceType . ': ' . $entityReferenceDef->getName();
        }
      }
    }

    $this->fieldOptions = $fieldOptions;
  }

  /**
   * Gets the default value for current Splio field in the config form.
   *
   * @param array $splioField
   *   Splio field to get its default value.
   *
   * @return string
   *   String if it exists; empty string otherwise.
   */
  protected function getFormFieldDefaultValue(array $splioField): string {

    $defaultValue = '';

    /** @var \Drupal\splio\Entity\SplioFieldInterface $configEntityField */
    foreach ($this->localConfigEntitiesFields as $configEntityField) {
      if ($splioField['field_key'] === $configEntityField->getSplioField()) {
        $defaultValue = $configEntityField->getDrupalField();

        break;
      }
    }

    return $defaultValue ?? '';
  }

  /**
   * Gets current Splio field in the config form is key field.
   *
   * @param array $splioField
   *   Splio field to find out whether it is a key field.
   *
   * @return bool
   *   TRUE if it is a key field; FALSE otherwise.
   */
  protected function getFormFieldIsKeyField(array $splioField): bool {

    /** @var \Drupal\splio\Entity\SplioFieldInterface $configEntityField */
    foreach ($this->localConfigEntitiesFields as $configEntityField) {
      if ($splioField['field_key'] === $configEntityField->getSplioField()) {
        return $configEntityField->isKeyField();
      }
    }

    return FALSE;
  }

  /**
   * Gets the default value of a contact list.
   *
   * @param string $name
   *   Field name.
   *
   * @return string
   *   Default field value.
   */
  protected function getContactListDefaultValue(string $name): string {

    if (empty($this->localConfigEntitiesContactsLists[$name])) {
      return '';
    }

    return $this->localConfigEntitiesContactsLists[$name]->getDrupalField() ?? '';
  }

  /**
   * {@inheritDoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    if (SplioEntity::TYPE_ORDER_LINES === $this->splioEntity->getSplioEntityType()) {
      return;
    }

    parent::validateForm($form, $form_state);

    $keyFieldIsSelected = FALSE;
    $userInput = $form_state->getUserInput();

    foreach ($userInput['entity_fields_table'] as $key => $inputField) {
      if ($form_state->getValue('radios') === $key) {
        $keyFieldIsSelected = TRUE;

        if (empty($inputField['drupal_field'])) {
          $form_state->setErrorByName($key, $this->t('This selected field cannot be the field key. You must select a local field in the first place.'));

          break;
        }
      }
    }

    if (FALSE === $keyFieldIsSelected) {
      $form_state->setErrorByName($userInput['form_id'], $this->t('You need to set a field as a key field.'));
    }
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function save(array $form, FormStateInterface $form_state) {

    $this->currentEntity = $this->splioEntity->getSplioEntityType();
    $configEntities = [];

    try {
      $this->deleteCurrentConfigEntitiesFields();

      foreach ($this->splioEntityFields as $splioField) {
        if (self::SPLIO_ID_FIELD === $splioField['field_key']) {
          continue;
        }

        $configEntity = $this->createConfigEntity($splioField, $form_state);

        if (!empty($configEntity)) {
          $configEntities[$splioField['field_key']] = $configEntity;
        }
      }

      if (\count($configEntities) > 0) {
        $this->saveConfigEntities($configEntities);
      }

      $this->messenger()->addMessage($this->t('Fields saved successfully.'));
    }
    catch (\Exception $exception) {
      // Restore old config entities.
      $this->saveConfigEntities($this->localConfigEntitiesFields);

      $message = 'The fields could not be saved: ' . $exception->getMessage();

      $this->messenger()->addMessage(
        $this->t($message),
        MessengerInterface::TYPE_ERROR
      );
    }

    // Save the contact's list form along with the regular form.
    if (SplioEntity::TYPE_CONTACTS === $this->currentEntity) {
      $this->saveContactsLists($form_state);
    }

    $form_state->setRedirect('entity.splio.' . $this->currentEntity);
  }

  /**
   * Gets Splio current entity fields from Splio service.
   *
   * @return array
   *   The current entity fields.
   */
  protected function getSplioEntityFields(): array {

    $splioEntityFields = $this->splioConnector->getEntityTypeFieldsFromSplio(
      $this->currentEntity
    );

    if (empty($splioEntityFields['elements'])) {
      $this->logger->warning(
        'No fields for ' . $this->currentEntity . ' from Splio'
      );

      return [];
    }

    usort(
      $splioEntityFields['elements'],
      function ($a, $b) {
        // Default values come first.
        if ($a['is_system'] > $b['is_system']) {
          return -1;
        }

        if ($a['is_system'] === $b['is_system']) {
          if ($a['label'] < $b['label']) {
            return -1;
          }
        }

        return 1;
      }
    );

    return $splioEntityFields['elements'];
  }

  /**
   * Creates a splio entity, that is a config entity.
   *
   * @param array $splioField
   *   Splio field data.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Drupal form state.
   *
   * @return \Drupal\Core\Config\Entity\ConfigEntityInterface|null
   *   Config entity if saving was correct; NULL otherwise.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function createConfigEntity(
    array $splioField,
    FormStateInterface $form_state
  ): ?ConfigEntityInterface {

    $entityFieldsTable = $form_state->getUserInput()['entity_fields_table'];
    $entityField = $entityFieldsTable[$splioField['field_key']];

    if (empty($entityField['drupal_field'])) {
      return NULL;
    }

    $prefix = $this->currentEntity . '_';
    $id = $prefix . $splioField['field_key'];
    $configEntity = $this->entityTypeManager
      ->getStorage('splio_field')
      ->create();

    $configEntity->setId($id);
    $configEntity->setIsNew(TRUE);
    $configEntity->setIsKeyField(
      $splioField['field_key'] === $form_state->getValue('radios')
    );
    $configEntity->setSplioField($splioField['field_key']);
    $configEntity->setDrupalField($entityField['drupal_field']);
    $configEntity->setTypeField($entityField['type_hidden'] ?? 'string');
    $configEntity->setSplioEntity($this->currentEntity);
    $configEntity->setIsDefaultField($splioField['is_system']);
    $configEntity->setListIndex($splioField['id'] ?: NULL);

    return $configEntity;
  }

  /**
   * Deletes all Splio config entities.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function deleteCurrentConfigEntitiesFields() {

    /** @var \Drupal\Core\Config\Entity\ConfigEntityInterface $configSplioField */
    foreach ($this->localConfigEntitiesFields as $configSplioField) {
      $configSplioField->delete();
    }
  }

  /**
   * Deletes all Splio config entities (for contact lists).
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function deleteCurrentConfigEntitiesContactsLists() {

    /** @var \Drupal\Core\Config\Entity\ConfigEntityInterface $configSplioField */
    foreach ($this->localConfigEntitiesContactsLists as $configSplioField) {
      $configSplioField->delete();
    }
  }

  /**
   * Saves config entities.
   *
   * @param \Drupal\Core\Config\Entity\ConfigEntityInterface[] $configEntities
   *   Config entities to save.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function saveConfigEntities(array $configEntities) {

    foreach ($configEntities as $configEntity) {
      $configEntity->save();
    }
  }

  /**
   * Manages the 'contacts_lists' form under the 'contacts' entity form.
   *
   * Requests the contacts lists to Splio and creates a form, so the user can
   * configure each list.
   *
   * @param array $form
   *   The current form.
   *
   * @return array
   *   The $form updated including the 'contacts_list' form.
   */
  private function generateContactsListForm(
    array $form
  ): array {

    $form[SplioEntity::TYPE_CONTACTS_LISTS] = [
      '#type' => 'fieldset',
      '#title' => $this->currentEntity . " " . $this->t("Lists configuration"),
    ];
    $form[SplioEntity::TYPE_CONTACTS_LISTS]['lists_table'] = [
      '#type' => 'table',
      '#caption' => $this->t(
        "A contact <b>will be included</b> in a Splio list <b>only</b> in case the <i>Local List Field</i> contains the <b>exact name</b> of one (or multiple) of the lists below <b>or</b> a <b>TRUE</b> value (integer 1 or boolean TRUE) are stored as a value for the selected field. Mind that the configured local entity for <i>contacts</i> must define a field/s that contains the available lists in Splio.",
        ['%entity' => $this->currentEntity]
      ),
      '#attributes' => [
        'id' => 'list_fields',
      ],
      '#header' => [
        $this->t('Contacts list'),
        $this->t('Local list field'),
      ],
    ];
    $this->remoteContactsLists = $this->splioConnector->getContactsLists();
    $this->localConfigEntitiesContactsLists = $this->splioEntityHandler
      ->getLocalSplioEntitiesFields(
        ['splio_entity' => $this->contactsLists->getSplioEntityType()]
      );

    foreach ($this->remoteContactsLists as $list) {
      $name = self::PREFIX_CONTACT_LIST . $list;
      $form[SplioEntity::TYPE_CONTACTS_LISTS]['lists_table'][$list]['splio_list'] = [
        '#type' => 'label',
        '#title' => $list,
        '#description' => '',
      ];
      $form[SplioEntity::TYPE_CONTACTS_LISTS]['lists_table'][$list]['drupal_list'] = [
        '#type' => 'select',
        '#options' => $this->fieldOptions,
        '#empty_value' => '',
        '#default_value' => $this->getContactListDefaultValue($name),
      ];
    }

    return $form;
  }

  /**
   * Manages the contacts_lists entity type.
   *
   * Iterates over the existing splio contact lists and saves the configuration
   * set by the user in the contacts lists form.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form-state.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function saveContactsLists(FormStateInterface $form_state): void {

    $listsFields = [];
    $listsTable = $form_state->getUserInput()['lists_table'];

    try {
      $this->deleteCurrentConfigEntitiesContactsLists();

      foreach ($listsTable as $name => $list) {
        $key = self::PREFIX_CONTACT_LIST . $name;
        $configEntityContactList = $this->entityTypeManager
          ->getStorage('splio_field')
          ->create();

        $configEntityContactList->setIsNew(TRUE);
        $configEntityContactList->setId($key);
        $configEntityContactList->setListIndex(
          $this->getContactListIdFromSplio($name)
        );
        $configEntityContactList->setSplioEntity(SplioEntity::TYPE_CONTACTS_LISTS);
        $configEntityContactList->setSplioField($name);
        $configEntityContactList->setDrupalField($list['drupal_list']);
        $configEntityContactList->setTypeField('string');
        $configEntityContactList->setIsKeyField(FALSE);

        $listsFields[] = $configEntityContactList;
      }

      if (\count($listsFields) > 0) {
        $this->saveConfigEntities($listsFields);
        $this->messenger()->addMessage($this->t('Lists saved successfully.'));
      }
    }
    catch (\Exception $exception) {
      // Restore old config entities.
      $this->saveConfigEntities($this->localConfigEntitiesContactsLists);

      $this->messenger()->addMessage(
        $this->t(
          'Something went wrong, the lists could not be saved. Try again.'
        ),
        MessengerInterface::TYPE_ERROR
      );
    }
  }

  /**
   * Gets Splio list index from name.
   *
   * @param string $name
   *   List name.
   *
   * @return int|null
   *   Int if the name exists in the list retrieved from Splio; NULL otherwise.
   */
  protected function getContactListIdFromSplio(string $name): ?int {

    foreach ($this->remoteContactsLists as $index => $contactsList) {
      if ($name === $contactsList) {
        return $index;
      }
    }

    return NULL;
  }

  /**
   * Checks whether the requested entity is configured correctly.
   *
   * In case the requested entity is configured correctly a SplioEntity instance
   * will be created. Otherwise, the user will be redirected to
   * EntitiesConfigForm in order to set up the entity configuration.
   */
  private function checkEntityConfiguration() {

    $currentPath = explode('/', $this->currentPathStack->getPath());
    $currentEntity = end($currentPath);
    $response = new RedirectResponse('/admin/config/splio/entity');

    try {
      $this->splioEntityHandler->getLocalSplioEntitiesConfig();
    }
    catch (EntitiesNotConfiguredYetException $exception) {
      $this->messenger()->addMessage($exception->getMessage());

      $response->send();
    }

    $this->splioEntity = new SplioEntity(
      $this->splioConnector,
      $this->splioEntityHandler,
      $this->logger,
      $currentEntity
    );

    if (SplioEntity::TYPE_CONTACTS === $currentEntity) {
      $this->contactsLists = new SplioEntity(
        $this->splioConnector,
        $this->splioEntityHandler,
        $this->logger,
        SplioEntity::TYPE_CONTACTS_LISTS
      );
    }
  }

}
