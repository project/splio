<?php

namespace Drupal\splio\Utility;

/**
 * Provides helpers to perform diffs on multi dimensional arrays.
 *
 * @ingroup utility
 */
class DiffArray {

  /**
   * Recursively computes the difference of arrays with additional index check.
   *
   * This is a version of array_diff_assoc() that supports multidimensional
   * arrays.
   *
   * This is a version of Drupal\Component\Utility\DiffArray that doesn't do absolute
   * comparisons. We need "2" == 2 and not "2" === 2.
   *
   * @param array $array1
   *   The array to compare from.
   * @param array $array2
   *   The array to compare to.
   *
   * @return array
   *   Returns an array containing all the values from array1 that are not
   *   present in array2.
   */
  public static function diffAssocRecursive(array $array1, array $array2) {
    $difference = [];

    // Fix difference in format.
    foreach ($array2 as $key => $value) {
      if (is_array($value) && empty($array1[$key]) && !empty($value)) {
        if (is_array($value[array_key_first($value)])) {
          $array1[$key][array_key_first($value)] = [];
        }
        else {
          $array1[$key][array_key_first($value)] = NULL;
        }
      }
    }

    foreach ($array1 as $key => $value) {
      if (is_array($value)) {
        if (!array_key_exists($key, $array2)) {
          if (is_array($value)) {
            $array2[$key] = [];
          }
          else {
            $array2[$key] = NULL;
          }
        }
        if (!is_array($array2[$key])) {
          $difference[$key] = [
            'before' => $value,
            'after' => $array2[$key] ?? NULL,
          ];
        }
        else {
          if ((is_array($value) && !empty($value)) || (!is_array($value) && $value != '')) {
            $new_diff = static::diffAssocRecursive($value, $array2[$key]);
            if (!empty($new_diff)) {
              $difference[$key] = $new_diff;
            }
          }
          else {
            if (is_array($value) && is_array($array2[$key]) && !empty($array2[$key]) && !empty($value)) {
              // Do a reverse recursive.
              $new_diff = static::diffAssocRecursive($array2[$key], $value);
              $sub_key = array_keys($new_diff[0])[0];
              // Switch around before/after.
              $difference[$key][0][$sub_key] = [
                'before' => $new_diff[0][$sub_key]['after'],
                'after' => $new_diff[0][$sub_key]['before'],
              ];
            }
          }
        }
      }
      // Don't use !== here. "120.7" !== "120.700" but "120.7" == "120.700".
      elseif (
        (
          !array_key_exists($key, $array2) &&
          $value !== NULL
        ) ||
        (string) $array2[$key] != (string) $value
      ) {
        $difference[$key] = [
          'before' => $value,
          'after' => $array2[$key] ?? NULL,
        ];
      }
    }

    return $difference;
  }

}
