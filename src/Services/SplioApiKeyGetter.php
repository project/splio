<?php

namespace Drupal\splio\Services;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\key\KeyRepositoryInterface;

class SplioApiKeyGetter implements SplioApiKeyGetterInterface {

  /**
   * Config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private ConfigFactoryInterface $config;

  /**
   * Key repository service.
   *
   * @var \Drupal\key\KeyRepositoryInterface
   */
  private KeyRepositoryInterface $keyManager;

  public function __construct(
    ConfigFactoryInterface $config,
    KeyRepositoryInterface $keyManager
  ) {
    $this->config = $config;
    $this->keyManager = $keyManager;
  }

  /**
   * {@inheritDoc}
   */
  public function getApiKey(string $settings): ?string {
    $immutableConfig = $this->config->get($settings);
    $savedKey = $immutableConfig->get('splio_config') ?? '';
    $key = empty($this->keyManager->getKey($savedKey))
      ? ''
      : $this->keyManager->getKey($savedKey)->getKeyValues();

    return $key['apiKey'] ?? NULL;
  }

}
