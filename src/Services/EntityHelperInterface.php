<?php

namespace Drupal\splio\Services;

use Drupal\Core\Entity\EntityInterface;

interface EntityHelperInterface {

  /**
   * Returns original entity before saving.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity to handle.
   *
   * @return mixed|null
   *   Returns EntityInterface or NULL.
   */
  public function getOriginal(EntityInterface $entity);

}
