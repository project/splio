<?php

namespace Drupal\splio\Services;

use Drupal\Core\State\State;
use Drupal\Core\State\StateInterface;
use Drupal\splio\Exception\CannotAuthenticateException;
use Drupal\splio\Exception\TokenNotReceivedException;
use Drupal\splio\Form\ApiKeyConfigForm;
use GuzzleHttp\ClientInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;

/**
 * Authenticator to Splio.
 */
class SplioAuthenticator implements SplioAuthenticatorInterface {

  const TOKEN_FIELD_NAME = 'splio.token';

  /**
   * HTTP client service.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  private ClientInterface $client;

  /**
   * Logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  private LoggerInterface $logger;

  /**
   * Splio URI generator service.
   *
   * @var \Drupal\splio\Services\SplioUriGeneratorInterface
   */
  private SplioUriGeneratorInterface $uriGenerator;

  /**
   * Drupal state service.
   *
   * @var \Drupal\Core\State\State
   */
  private State $state;

  /**
   * API key getter service.
   *
   * @var \Drupal\splio\Services\SplioApiKeyGetterInterface
   */
  private SplioApiKeyGetterInterface $apiKeyGetter;

  /**
   * Exception message parser.
   *
   * @var \Drupal\splio\Services\ExceptionMessageParserInterface
   */
  private ExceptionMessageParserInterface $exceptionMessageParser;

  /**
   * Token from Splio.
   *
   * @var string|null
   */
  private ?string $token;

  public function __construct(
    ClientInterface $client,
    LoggerInterface $logger,
    SplioUriGeneratorInterface $uriGenerator,
    StateInterface $state,
    SplioApiKeyGetterInterface $apiKeyGetter,
    ExceptionMessageParserInterface $exceptionMessageParser
  ) {

    $this->client = $client;
    $this->logger = $logger;
    $this->uriGenerator = $uriGenerator;
    $this->state = $state;
    $this->apiKeyGetter = $apiKeyGetter;
    $this->exceptionMessageParser = $exceptionMessageParser;
  }

  /**
   * {@inheritDoc}
   */
  public function getToken(): ?string {

    return $this->token ?? NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function setToken(?string $token): void {

    if (isset($token)) {
      $this->state->set(self::TOKEN_FIELD_NAME, $token);
      $this->token = $token;
    }
    else {
      $this->state->delete(self::TOKEN_FIELD_NAME);
      $this->token = NULL;
    }
  }

  /**
   * {@inheritDoc}
   */
  public function tokenIsExpired(): bool {

    $decodedToken = $this->decodeJwt($this->token);
    $expiration = new \DateTimeImmutable(
      date('Y-m-d H:i:s', $decodedToken['exp'])
    );
    $now = new \DateTimeImmutable('now', $expiration->getTimezone());

    return ($expiration->getTimeStamp() - $now->getTimeStamp()) < 0;
  }

  /**
   * {@inheritDoc}
   */
  public function authenticate(): ?string {

    $token = $this->getToken();

    if (!empty($token) && !$this->tokenIsExpired()) {
      return $token;
    }

    $this->setTokenFromState();

    $token = $this->getToken();

    if (!empty($token) && !$this->tokenIsExpired()) {
      return $token;
    }

    // New token from Splio.
    $response = $this->sendRequestToSplio();
    $token = $this->getTokenFromResponse($response);

    $this->setToken($token);

    return $this->token;
  }

  /**
   * {@inheritDoc}
   */
  public function decodeJwt(string $token): array {

    return json_decode(
      base64_decode(
        str_replace('_', '/', str_replace('-', '+', explode('.', $token)[1]))
      ),
      TRUE
    );
  }

  /**
   * Sets local token property with Drupal state value.
   */
  private function setTokenFromState(): void {

    $this->token = $this->state->get(self::TOKEN_FIELD_NAME);
  }

  /**
   * Sends authentication request to Splio.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   Splio response.
   *
   * @throws \Drupal\splio\Exception\CannotAuthenticateException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  private function sendRequestToSplio(): ResponseInterface {

    $uri = $this->uriGenerator->getUri(
      SplioUriGenerator::SPLIO_PATH['authenticate']
    );
    $options = [
      'body' => json_encode(
        [
          'api_key' => $this->apiKeyGetter->getApiKey(
            ApiKeyConfigForm::SETTINGS
          ),
        ]
      ),
    ];

    try {
      return $this->client->post($uri, $options);
    }
    catch (\Exception $exception) {
      $message = json_encode(
        [
          'message' => 'Cannot authenticate to Splio: ' . $this->exceptionMessageParser->getMessage($exception),
          'uri' => $uri,
          'method' => 'POST',
          'options' => $options,
        ]
      );

      $this->logger->error($message);

      throw new CannotAuthenticateException($message);
    }
  }

  /**
   * Gets token from Splio response or throws exception if error.
   *
   * @param \Psr\Http\Message\ResponseInterface $response
   *   Response from Splio.
   *
   * @return string
   *   Token.
   *
   * @throws \Drupal\splio\Exception\TokenNotReceivedException
   */
  private function getTokenFromResponse(ResponseInterface $response): string {

    $response = $response->getBody()->getContents();
    $response = json_decode($response, TRUE);

    if (!empty($response['token'])) {
      return $response['token'];
    }

    $message = json_encode(
      [
        'message' => 'Splio did not send an authentication token. Message: ' . json_encode($response),
      ]
    );

    $this->logger->error($message);

    throw new TokenNotReceivedException($message);
  }

}
