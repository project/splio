<?php

namespace Drupal\splio\Services;

use Drupal\Core\Entity\EntityInterface;
use Drupal\splio\Exception\EntitiesNotConfiguredYetException;
use Drupal\splio\Exception\EntityIsEmptyException;
use Drupal\splio\Exception\EntityTypeNotFoundInSplioException;

interface SplioQueueHandlerInterface {

  /**
   * Adds the received splio entity to a queue to be processed later.
   *
   * Receives a splio entity and the CRUD action to perform with it.
   * The entity will be added to a queue where it will be processed on the
   * next cron run.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity which will be added to the queue.
   * @param string $action
   *   CRUD action that will be performed when the received entity is processed.
   *   Accepts the following parameters: create, update, delete and dequeue.
   *
   * @throws EntitiesNotConfiguredYetException
   * @throws EntityIsEmptyException
   * @throws EntityTypeNotFoundInSplioException
   */
  public function addEntityToQueue(EntityInterface $entity, $action): void;

  /**
   * Deletes an item from queue.
   *
   * @param array $item
   *   Item to delete.
   */
  public function deleteItem(array $item): void;

}
