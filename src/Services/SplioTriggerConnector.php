<?php

namespace Drupal\splio\Services;

use Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\key\KeyRepository;
use Drupal\splio\Form\TriggerApiKeyConfigForm;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\RequestOptions;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class to connect to Splio Trigger API.
 */
class SplioTriggerConnector extends SplioConnector {

  const UNIVERSE = 'bodeboca';

  /**
   * Getter for API key.
   *
   * @var \Drupal\splio\Services\SplioApiKeyGetterInterface
   */
  private SplioApiKeyGetterInterface $apiKeyGetter;

  public function __construct(
    KeyRepository $keyManager,
    ConfigFactory $config,
    EntityTypeManagerInterface $entityTypeManager,
    QueueFactory $queueFactory,
    ContainerAwareEventDispatcher $eventDispatcher,
    LoggerInterface $logger,
    ClientInterface $client,
    SplioAuthenticatorInterface $authenticator,
    SplioUriGeneratorInterface $uriGenerator,
    SplioEntityHandlerInterface $splioEntityHandler,
    SplioEntityStructureGeneratorInterface $splioEntityGenerator,
    SplioApiKeyGetterInterface $apiKeyGetter,
    ExceptionMessageParserInterface $exceptionMessageParser
  ) {
    parent::__construct(
      $keyManager,
      $config,
      $entityTypeManager,
      $queueFactory,
      $eventDispatcher,
      $logger,
      $client,
      $authenticator,
      $uriGenerator,
      $splioEntityHandler,
      $splioEntityGenerator,
      $exceptionMessageParser
    );

    $this->apiKeyGetter = $apiKeyGetter;
  }

  /**
   * Sends a petition to Splio to send a message to recipients.
   *
   * @param array $data
   *   The trigger data to be fired in Splio.
   *   [
   *      message => 'xxxxx',
   *      opcode => [
   *        type => 'xxxx',
   *        success_message => 'xxx',
   *        failure_message => 'xxx',
   *      ],
   *   ].
   *
   * @return array
   *   [
   *     code => HTTP response status code,
   *     error => has value if an error occurred
   *   ].
   *
   * @throws \Drupal\splio\Exception\TokenNotReceivedException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function triggerMessage(array $data): array {
    $config = $this->getConfigConnection();
    $uri = $this->uriGenerator->getUri('', $config);
    $options['formParams'] = [
      'universe' => self::UNIVERSE,
      'key' => $config['apiKey'],
    ];
    $options['formParams'] += $data;
    $requestOptions = $this->generateRequestOptions($options);

    try {
      $this->client->request('POST', $uri, $requestOptions);

      $response = [
        'code' => Response::HTTP_OK,
        'error' => '',
      ];
    }
    catch (GuzzleException $exception) {
      $response = [
        'code' => $exception->getCode(),
        'error' => $this->exceptionMessageParser->getMessage($exception),
        'data' => json_encode($data),
      ];

      // In any other case, a connection error might have occurred.
      $this->logger->error(json_encode($response));
    }

    return $response;
  }

  /**
   * {@inheritDoc}
   */
  protected function generateRequestOptions(?array $data = []): array {
    $requestOptions = parent::generateRequestOptions($data);

    // Splio Trigger API doesn't need authorization.
    unset($requestOptions[RequestOptions::HEADERS]['Authorization']);

    return $requestOptions;
  }

  /**
   * Returns the configuration to connect to Splio Trigger.
   *
   * @return array
   *   Configuration.
   */
  private function getConfigConnection(): array {
    $triggerSettings = $this->config->get(TriggerApiKeyConfigForm::SETTINGS);
    $apiKey = $this->apiKeyGetter->getApiKey(TriggerApiKeyConfigForm::SETTINGS);

    return [
      'server' => $triggerSettings->get('splio_server'),
      'connectionTimeout' => $triggerSettings->get('connection_timeout') ?? 0,
      'timeout' => $triggerSettings->get('response_timeout') ?? 0,
      'apiKey' => $apiKey,
    ];
  }

}
