<?php

namespace Drupal\splio\Services;

use Drupal\splio\Entity\SplioEntity;

/**
 * Manages contacts lists data.
 */
class SplioContactsListsManager implements SplioContactsListsManagerInterface {

  /**
   * Splio entity handler service.
   *
   * @var \Drupal\splio\Services\SplioEntityHandlerInterface
   */
  private SplioEntityHandlerInterface $splioEntityHandler;

  public function __construct(SplioEntityHandlerInterface $splioEntityHandler) {

    $this->splioEntityHandler = $splioEntityHandler;
  }

  /**
   * {@inheritDoc}
   */
  public function getIdListFromEntityStructure(array $structure): array {

    $structuredLists = $structure['lists'] ?? [];
    $idList = [];

    foreach ($structuredLists as $list) {
      $idList[] = (int) $list['id'];
    }

    return $idList;
  }

  /**
   * {@inheritDoc}
   */
  public function getIdsListFromStorage(): array {

    $idsList = [];
    $lists = $this->splioEntityHandler
      ->getLocalSplioEntitiesFields(['splio_entity' => SplioEntity::TYPE_CONTACTS_LISTS]);

    if (!empty($lists)) {
      /** @var \Drupal\splio\Entity\SplioFieldInterface $list */
      foreach ($lists as $list) {
        $idsList[] = (int) $list->getListIndex();
      }
    }

    return $idsList;
  }

}
