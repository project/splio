<?php

namespace Drupal\splio\Services;

/**
 * URI generator for Splio API.
 */
interface SplioUriGeneratorInterface {

  /**
   * Generates Splio base uri.
   *
   * @param array|null $options
   *   Options for alternative server.
   *
   * @return string
   *   Splio base uri.
   */
  public function generateBaseUri(array $options = NULL): string;

  /**
   * Gets Splio uri.
   *
   * @param string $path
   *   Splio desired service path.
   * @param array|null $options
   *   Options for query params.
   *
   * @return string
   *   Splio uri.
   */
  public function getUri(string $path, ?array $options = NULL): string;

}
