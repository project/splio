<?php

namespace Drupal\splio\Services;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\splio\Entity\SplioField;
use Psr\Log\LoggerInterface;

class EntityValueHandler implements EntityValueHandlerInterface {

  const DATE_FORMAT = 'Y-m-d H:i:s';

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private EntityTypeManagerInterface $entityTypeManager;

  /**
   * Logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  private LoggerInterface $logger;

  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    LoggerInterface $logger
  ) {

    $this->entityTypeManager = $entityTypeManager;
    $this->logger = $logger;
  }

  /**
   * {@inheritDoc}
   */
  public function getFieldValue(string $field, EntityInterface $entity) {

    $value = [];

    if (!empty($field)) {
      // If the received field turns out to be an entity reference.
      if (strncmp($field, "{{", 2) == 0) {
        // Remove the two first and last chars which are "{{ }}".
        $field = substr($field, 2, -2);
        $value = $this->getEntityReferenceValue($field, $entity);
      }
      else {
        $fieldValues = $entity->get($field)->getValue();

        // In case the received field contains an array of elements,
        // send them as a comma-separated string.
        if (is_array($fieldValues) && count($fieldValues) > 1) {
          foreach ($fieldValues as $fieldDef) {
            $value[] = end($fieldDef);
          }

          $value = implode(",", $value);
        }
        else {
          // In other cases, just unpack the value.
          $value = $entity->get($field)->getValue();

          if (is_array($value)) {
            if (\count($value) > 0) {
              $value = end($value[0]);
            }
            else {
              $value = '';
            }
          }
        }
      }
    }

    return $value;
  }

  /**
   * {@inheritDoc}
   */
  public function formatField(SplioField $field, $fieldValue) {

    if ('date' === $field->getTypeField()) {
      if (is_numeric($fieldValue) && $fieldValue != 0) {
        $fieldValue = date(self::DATE_FORMAT, $fieldValue);
      }
      else {
        // Set unknown dates as empty to avoid 1970-01-01.
        $fieldValue = '';
      }
    }

    return $fieldValue;
  }

  /**
   * Returns the value of the referenced entity fields.
   *
   * Receives a string containing the current entity attribute which contains:
   * {current entity field}.{referenced entity type}.{referenced entity field}
   * Also receives the current entity. Returns the final value for the
   * received reference entity. Makes a recursive call in case the referenced
   * field contains another reference entity.
   *
   * @param string $drupalField
   *   String containing the current entity attributes following the structure:
   *   {this entity field}.{referenced entity type}.{referenced entity field}.
   * @param object $entity
   *   Current entity that contains an entity reference in one of it's fields.
   *
   * @return mixed
   *   Returns a string containing the value binded to the entity reference.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *
   * TODO: This method may need some small rework in the future when the
   *   setupForm() method from SplioFieldForm is updated to allow an undefined
   *   number of entityReferences in a field. This may lead to  SplioField
   *   schema updates as well (string -> array).
   */
  private function getEntityReferenceValue(
    string $drupalField,
    object $entity
  ) {

    $drupalFieldEntityReference = explode('.', $drupalField);
    $entityRefId = $entity
      ->get($drupalFieldEntityReference[0])
      ->getValue();

    if (empty($entityRefId[0])) {
      return NULL;
    }

    $entityRef = $this->entityTypeManager
      ->getStorage($drupalFieldEntityReference[1])
      ->load(end($entityRefId[0]));

    if (empty($entityRef)) {
      return NULL;
    }

    try {
      $entityRefField = $entityRef->get($drupalFieldEntityReference[2]);
    }
    catch (\Error $error) {
      $this->logger->error(
        "Error trying to fetch the %drupalFieldEntityReference[1] entity referenced from the field %drupalFieldEntityReference[0] of the %entity->getEntityTypeId() entity. Check that your fields are configured properly.",
        [
          '%drupalFieldEntityReference[1]' => $drupalFieldEntityReference[1],
          '%drupalFieldEntityReference[0]' => $drupalFieldEntityReference[0],
          '%entity->getEntityTypeId()' => $entity->getEntityTypeId(),
        ]
      );

      return [];
    }

    $entityRefFieldType = $entityRefField->getFieldDefinition()->getType();

    if ($entityRefFieldType === "entity_reference") {
      $fieldName = $entityRefField
        ->getFieldDefinition()
        ->getname();
      $entityRefType = $entityRefField
        ->getFieldDefinition()
        ->getSettings()['target_type'];
      $entityRefFieldName = key(end($entityRefField->getValue()));
      $entityRefField = "$fieldName.$entityRefType.$entityRefFieldName";
      $this->getEntityReferenceValue($entityRefField, $entityRef);
    }
    else {
      $entityRefField = [
        $entityRefField->getName() => end($entityRefField->getValue()[0]),
      ];
    }

    return $entityRefField;
  }

}
