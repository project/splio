<?php

namespace Drupal\splio\Services;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\splio\Entity\SplioEntity;
use Drupal\splio\Exception\EntitiesNotConfiguredYetException;
use Drupal\splio\Exception\EntityIsEmptyException;
use Drupal\splio\Exception\EntityTypeNotFoundInSplioException;
use Psr\Log\LoggerInterface;

/**
 * Splio entity handler.
 */
class SplioEntityHandler implements SplioEntityHandlerInterface {

  const PREFIX_ORDER_LINES = 'order_lines_';

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private EntityTypeManagerInterface $entityTypeManager;

  /**
   * Entity value handler.
   *
   * @var \Drupal\splio\Services\EntityValueHandlerInterface
   */
  private EntityValueHandlerInterface $entityValueHandler;

  /**
   * Logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  private LoggerInterface $logger;

  /**
   * Config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  private ConfigFactory $config;

  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    EntityValueHandlerInterface $entityValueHandler,
    LoggerInterface $logger,
    ConfigFactory $config
  ) {

    $this->entityTypeManager = $entityTypeManager;
    $this->entityValueHandler = $entityValueHandler;
    $this->logger = $logger;
    $this->config = $config;
  }

  /**
   * {@inheritDoc}
   */
  public function getLocalSplioEntitiesConfig() {

    $entitiesConfig = $this->config
      ->get('splio.entity.config')
      ->get('splio_entities');

    if (empty($entitiesConfig)) {
      $message = t('You have not configured the relation of your entities with Splio ones yet.');

      $this->logger->warning($message);

      throw new EntitiesNotConfiguredYetException();
    }

    return $entitiesConfig;
  }

  /**
   * {@inheritDoc}
   */
  public function getLocalSplioEntitiesFields(
    array $propertiesFilter
  ): array {

    return $this->entityTypeManager
      ->getStorage('splio_field')
      ->loadByProperties($propertiesFilter);
  }

  /**
   * {@inheritDoc}
   */
  public function getSplioEntityTypeFromLocalEntity(
    ?EntityInterface $entity
  ): ?string {

    if (NULL === $entity) {
      throw new EntityIsEmptyException();
    }

    $splioEntities = $this->getLocalSplioEntitiesConfig();

    foreach ($splioEntities as $type => $definition) {
      if ($entity->getEntityType()->id() === $definition['local_entity']) {
        return $type;
      }
    }

    throw new EntityTypeNotFoundInSplioException(
      $entity->getEntityType()->id()
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getProperEntityType(string $entityType): string {

    if (SplioEntity::TYPE_ORDER_LINES === $entityType) {
      $entityType = SplioEntity::TYPE_RECEIPTS;
    }

    return $entityType;
  }

  /**
   * {@inheritDoc}
   */
  public function getProperEntity(
    string $entityType,
    EntityInterface $entity
  ): ?EntityInterface {

    if (SplioEntity::TYPE_ORDER_LINES === $entityType) {
      $orderEntity = $this->getOrderFromOrderLine($entity);

      if (!empty($orderEntity)) {
        $original = $entity;
        $entity = $orderEntity;
        $entity->original = $original;
      }
      else {
        $this->logger->warning(
          'Could not retrieve an order for the received %entity order_lines entity.',
          [
            '%entity' => $entity->getEntityTypeId(),
          ]
        );

        return NULL;
      }
    }

    return $entity;
  }

  /**
   * {@inheritDoc}
   */
  public function getEntityKeyFieldValue(
    string $entityType,
    EntityInterface $entity
  ): string {

    $keyField = $this->getEntityTypeKeyField($entityType);

    return $entity->get($keyField)->value ?? '';
  }

  /**
   * {@inheritDoc}
   */
  public function getEntityTypeKeyField(string $entityType): string {

    $fields = $this->getLocalSplioEntitiesFields(
      ['splio_entity' => $entityType]
    );

    /** @var \Drupal\splio\Entity\SplioField $field */
    foreach ($fields as $field) {
      if ($field->isKeyField()) {
        return $field->getDrupalField();
      }
    }

    return '';
  }

  /**
   * {@inheritDoc}
   */
  public function getDrupalFieldFromSplioFieldEntity(
    string $entityType,
    string $fieldName
  ): string {

    $drupalField = '';
    $fields = $this->getLocalSplioEntitiesFields(
      ['splio_entity' => $entityType]
    );

    /** @var \Drupal\splio\Entity\SplioFieldInterface $field */
    foreach ($fields as $field) {
      if ($field->getDrupalField() === $fieldName) {
        $drupalField = $field->getDrupalField();

        break;
      }
    }

    return $drupalField;
  }

  /**
   * Receives an order_line entity and returns the receipt that it belongs to.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The received order_line entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   Returns a receipt (order) entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\splio\Exception\EntitiesNotConfiguredYetException
   */
  private function getOrderFromOrderLine(
    EntityInterface $entity
  ): ?EntityInterface {

    // Retrieve the Drupal field that contains the order id for this
    // order line item.
    $splioEntityFields = $this->getLocalSplioEntitiesFields(
      ['splio_entity' => SplioEntity::TYPE_ORDER_LINES]
    );
    $orderIdDrupalField = $splioEntityFields[self::PREFIX_ORDER_LINES . 'id_order']
      ->getDrupalField();

    // Next, obtain the value of the Order id,
    // which this order line belongs to.
    if (!empty($orderIdDrupalField)) {
      $orderIdValue = $this->entityValueHandler->getFieldValue(
        $orderIdDrupalField,
        $entity
      );
    }
    else {
      $this
        ->logger
        ->error("Error trying to fetch Receipts key field. Check that your Receipts key field is configured properly.");
      return NULL;
    }

    $orderIdValue = is_array($orderIdValue)
      ? $orderIdValue[array_keys($orderIdValue)[0]] : $orderIdValue;

    $orderEntityType = '';

    if (!empty($orderIdValue)) {
      try {
        $orderKeyFieldData = $this->getLocalSplioEntitiesFields(
          [
            'splio_entity' => SplioEntity::TYPE_RECEIPTS,
            'is_key_field' => '1',
          ]
        );

        if (1 !== \count($orderKeyFieldData)) {
          throw new \Exception();
        }

        $entities = $this->getLocalSplioEntitiesConfig();
        $orderEntityType = $entities[SplioEntity::TYPE_RECEIPTS]['local_entity'];

        /** @var \Drupal\splio\Entity\SplioField $orderKeyField */
        $orderKeyField = end($orderKeyFieldData);
        $orderKeyDrupalField = $orderKeyField->getDrupalField();
      }
      catch (
        \Exception
        | EntitiesNotConfiguredYetException
        $exception
      ) {
        $this
          ->logger
          ->error("Error trying to fetch Receipts key field. Check that your Receipts key field, and the 'id_order' field from Order Lines, are configured properly.");
      }

      // Finally, load the proper $orderEntityType by passing the
      // $orderIdValue as the $orderKeyField.
      if (!empty($orderKeyDrupalField)) {
        $entities = $this->entityTypeManager
          ->getStorage($orderEntityType)
          ->loadByProperties([$orderKeyDrupalField => $orderIdValue]);

        return $entities[$orderIdValue] ?? NULL;
      }
    }

    return NULL;
  }

}
