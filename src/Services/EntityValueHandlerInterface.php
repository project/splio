<?php

namespace Drupal\splio\Services;

use Drupal\Core\Entity\EntityInterface;
use Drupal\splio\Entity\SplioField;

interface EntityValueHandlerInterface {

  /**
   * Receives a splio field for a particular entity and returns its value.
   *
   * @param string $field
   *   The splio field which value is being requested.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The current entity to which the received field belongs to.
   *
   * @return mixed
   *   Returns a string with the requested value.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   *
   * @todo Improve how we get data through Drupal system.
   */
  public function getFieldValue(string $field, EntityInterface $entity);

  /**
   * Formats the received value to its field type.
   *
   * @param \Drupal\splio\Entity\SplioField $field
   *   The Splio received field.
   * @param mixed $fieldValue
   *   The current value for the received Splio field.
   *
   * @return mixed
   *   Returns the value formatted to the configured type.
   */
  public function formatField(SplioField $field, $fieldValue);

}
