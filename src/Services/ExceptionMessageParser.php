<?php

namespace Drupal\splio\Services;

/**
 * Parser for message exceptions.
 */
class ExceptionMessageParser implements ExceptionMessageParserInterface {

  /**
   * {@inheritDoc}
   */
  public function getMessage(\Exception $exception): string {

    if (
      method_exists($exception, 'hasResponse')
      && $exception->hasResponse()
    ) {
      $message = $exception->getResponse()->getBody()->getContents();

      if (!empty($message)) {
        return $message;
      }

      $errors = json_decode($exception->getResponse()->getBody(), TRUE);

      return json_encode($errors['errors']);
    }
    else {
      return $exception->getMessage() . $exception->getTraceAsString();
    }
  }

}
