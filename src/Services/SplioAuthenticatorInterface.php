<?php

namespace Drupal\splio\Services;

interface SplioAuthenticatorInterface {

  /**
   * Returns token property value.
   *
   * @return string|null
   *   Token value.
   */
  public function getToken(): ?string;

  /**
   * Sets token value in Drupal state, and also in property.
   *
   * If NULL, it will be deleted from Drupal state.
   *
   * @param string|null $token
   *   Token.
   */
  public function setToken(?string $token): void;

  /**
   * Checks if token is expired.
   *
   * @return bool
   *   TRUE is expired; FALSE otherwise.
   *
   * @throws \Exception
   */
  public function tokenIsExpired(): bool;

  /**
   * Authenticates to Splio.
   *
   * @return string|null
   *   Token value if authentication was successful; FALSE otherwise.
   *
   * @throws \Exception
   * @throws \Drupal\splio\Exception\TokenNotReceivedException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function authenticate(): ?string;

  /**
   * Decodes JWT in order to get all values.
   *
   * @param string $token
   *   JWT.
   *
   * @return array
   *   Array with all plain values.
   */
  public function decodeJwt(string $token): array;

}
