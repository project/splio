<?php

namespace Drupal\splio\Services;

use Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\splio\Entity\SplioEntity;
use Drupal\splio\Event\SplioQueueEvent;
use Drupal\splio\Exception\EntitiesNotConfiguredYetException;
use Drupal\splio\Exception\EntityIsEmptyException;
use Drupal\splio\Exception\EntityTypeNotFoundInSplioException;
use Drupal\splio\Utility\DiffArray;
use Psr\Log\LoggerInterface;

/**
 * Handles the Splio items in Drupal queue.
 */
class SplioQueueHandler implements SplioQueueHandlerInterface {

  /**
   * Splio entity handler.
   *
   * @var \Drupal\splio\Services\SplioEntityHandlerInterface
   */
  private SplioEntityHandlerInterface $splioEntityHandler;

  /**
   * Queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  private QueueFactory $queueFactory;

  /**
   * Event dispatcher.
   *
   * @var \Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher
   */
  private ContainerAwareEventDispatcher $eventDispatcher;

  /**
   * Logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  private LoggerInterface $logger;

  /**
   * Entity helper.
   *
   * @var \Drupal\splio\Services\EntityHelperInterface
   */
  private EntityHelperInterface $entityHelper;

  const SPLIO_QUEUE_ID = 'cron_splio_sync';

  const VALID_ACTIONS = [
    'create' => 'create',
    'update' => 'update',
    'delete' => 'delete',
    'dequeue' => 'dequeue',
  ];

  public function __construct(
    SplioEntityHandlerInterface $splioEntityHandler,
    QueueFactory $queueFactory,
    ContainerAwareEventDispatcher $eventDispatcher,
    LoggerInterface $logger,
    EntityHelperInterface $entityHelper
  ) {

    $this->splioEntityHandler = $splioEntityHandler;
    $this->queueFactory = $queueFactory;
    $this->eventDispatcher = $eventDispatcher;
    $this->logger = $logger;
    $this->entityHelper = $entityHelper;
  }

  /**
   * {@inheritDoc}
   */
  public function addEntityToQueue(EntityInterface $entity, $action): void {

    $entityType = $this
      ->splioEntityHandler
      ->getSplioEntityTypeFromLocalEntity($entity);

    if ($this->areThereRelevantChanges($entity, $action) === FALSE) {
      return;
    }

    $item = $this->generateQueueItem($entityType, $entity, $action);

    // Manage the event to be dispatched.
    $queueEvent = new SplioQueueEvent($item);
    $this->eventDispatcher->dispatch(
      $queueEvent,
      SplioQueueEvent::SPLIO_ENQUEUE
    );

    // In case someone captured the event and made changes in the item,
    // update the item before inserting it into the queue.
    $item = $queueEvent->getSplioQueueItem();

    // Finally, perform a last check to ensure the set action is valid.
    if ('dequeue' === $item['action']) {
      return;
    }

    $queue = $this->queueFactory->get(self::SPLIO_QUEUE_ID);

    if (in_array($item['action'], self::VALID_ACTIONS)) {
      $queue->createItem($item);

      return;
    }

    $this->logger->error(
      "The %type[%id] entity will not be queued. Action type received: %action. Only 'create', 'update' and 'delete' actions are queued.",
      [
        '%action' => $item['action'],
        '%type' => $item['splioEntityType'],
        '%id' => $item['id'],
      ]
    );
  }

  /**
   * {@inheritDoc}
   */
  public function deleteItem(array $item) : void {

    $queue = $this->queueFactory->get(self::SPLIO_QUEUE_ID);
    $queue->deleteItem($item);
  }

  /**
   * Generates the item that will be queued later.
   *
   * @param string $entityType
   *   Entity type.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity to enqueue.
   * @param string $action
   *   Queue action.
   *
   * @return array
   *   Queue item.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\splio\Exception\EntitiesNotConfiguredYetException
   */
  private function generateQueueItem(
    string $entityType,
    EntityInterface $entity,
    string $action
  ): array {

    $item = [];
    $entity = $this->splioEntityHandler->getProperEntity($entityType, $entity);

    if (SplioEntity::TYPE_ORDER_LINES === $entityType) {
      $entityType = SplioEntity::TYPE_RECEIPTS;
      $item += ['originalSplioEntityType' => SplioEntity::TYPE_ORDER_LINES];
    }

    // In case there is an original entity (update), add it to the queue item.
    // In case there is a delete action, add the deleted item as original.
    $original = $this->entityHelper->getOriginal($entity);

    if ('delete' === $action) {
      $original = $entity;
    }

    $item += [
      'id' => $entity->id(),
      'original' => $original,
      'lang' => $entity->language()->getId(),
      'splioEntityType' => $entityType,
      'action' => $action,
    ];

    return $item;
  }

  /**
   * Check to see if we are making any relevant change before enqueuing.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity being modified.
   * @param string $action
   *   The action being applied to the entity.
   *
   * @return bool
   *   Whether the change is relevant or not.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\splio\Exception\EntitiesNotConfiguredYetException
   */
  private function areThereRelevantChanges(
    EntityInterface $entity,
    string $action
  ): bool {

    if ($action !== 'update') {
      return TRUE;
    }

    if (!isset($entity->original)) {
      return TRUE;
    }

    $ignoreList = [
      'uuid',
      'changed',
    ];
    $changes = DiffArray::diffAssocRecursive(
      $entity->original->toArray(),
      $entity->toArray()
    );

    foreach ($ignoreList as $ignore) {
      unset($changes[$ignore]);
    }

    if (empty($changes)) {
      return FALSE;
    }

    $mappedFields = $this->getMappedFields($entity);

    if (empty(array_intersect_key($mappedFields, $changes))) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Get list of local field names being synced to Splio.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity being modified.
   *
   * @return array
   *   Keyed array of field names.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\splio\Exception\EntitiesNotConfiguredYetException
   */
  private function getMappedFields(EntityInterface $entity): array {

    try {
      $entityType = $this
        ->splioEntityHandler
        ->getSplioEntityTypeFromLocalEntity($entity);
    }
    catch (
      EntityTypeNotFoundInSplioException
      | EntitiesNotConfiguredYetException
      | EntityIsEmptyException
      $exception
    ) {
      return [];
    }

    $entityFields = $this
      ->splioEntityHandler
      ->getLocalSplioEntitiesFields(['splio_entity' => $entityType]);
    $mappedFields = [];

    foreach ($entityFields as $field) {
      $fieldName = $field->get('drupal_field');

      if (substr($field->get('drupal_field'), 0, 2) === '{{') {
        $fieldName = str_replace(['{{', '}}'], '', $field->get('drupal_field'));
        $fieldName = explode(".", $fieldName);
        $fieldName = end($fieldName);
      }

      $mappedFields[$fieldName] = $fieldName;
    }

    return $mappedFields;
  }

}
