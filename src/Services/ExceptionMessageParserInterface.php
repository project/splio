<?php

namespace Drupal\splio\Services;

interface ExceptionMessageParserInterface {

  /**
   * Gets message from exception, one way or the other.
   *
   * @param \Exception $exception
   *   Exception from Splio.
   *
   * @return string
   *   Exception message.
   */
  public function getMessage(\Exception $exception): string;

}
