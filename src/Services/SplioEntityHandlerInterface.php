<?php

namespace Drupal\splio\Services;

use Drupal\Core\Entity\EntityInterface;

interface SplioEntityHandlerInterface {

  /**
   * Load Splio entities from local storage.
   *
   * @return array|mixed|null
   *   Splio type entities.
   *
   * @throws \Drupal\splio\Exception\EntitiesNotConfiguredYetException
   */
  public function getLocalSplioEntitiesConfig();

  /**
   * Gets local Splio entities fields.
   *
   * @param array $propertiesFilter
   *   ['splio_entity' => 'SOME_SPLIO_ENTITY_TYPE'].
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   List of entity fields.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getLocalSplioEntitiesFields(array $propertiesFilter): array;

  /**
   * Determines if an entity is configured by the user as a Splio entity.
   *
   * Helper function that determines if a received entity belongs to the Splio
   * configured entities. Returns the Splio entity type if true, throws an
   * exception otherwise.
   *
   * @param \Drupal\Core\Entity\EntityInterface|null $entity
   *   The entity that may belong to Splio.
   *
   * @return string|null
   *   Returns a string with the SplioEntity type in case this entity is
   *   configured as a Splio entity. Returns FALSE otherwise.
   *
   * @throws \Drupal\splio\Exception\EntityTypeNotFoundInSplioException
   * @throws \Drupal\splio\Exception\EntitiesNotConfiguredYetException
   * @throws \Drupal\splio\Exception\EntityIsEmptyException
   */
  public function getSplioEntityTypeFromLocalEntity(EntityInterface $entity): ?string;

  /**
   * Gets the proper entity type.
   *
   * @param string $entityType
   *   Given entity type.
   *
   * @return string
   *   "receipts" if given entity type is "order_lines".
   *   The given entity type itself, otherwise.
   */
  public function getProperEntityType(string $entityType): string;

  /**
   * Gets the proper entity.
   *
   * @param string $entityType
   *   Entity type.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   An order entity object, if given entity type is type of "order_lines".
   *   The given entity itself, otherwise.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\splio\Exception\EntitiesNotConfiguredYetException
   */
  public function getProperEntity(string $entityType, EntityInterface $entity): ?EntityInterface;

  /**
   * Gets the value of an entity key field.
   *
   * @param string $entityType
   *   Entity type.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity.
   *
   * @return string
   *   Key field value.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getEntityKeyFieldValue(string $entityType, EntityInterface $entity): string;

  /**
   * Gets and entity type key field.
   *
   * @param string $entityType
   *   Entity type.
   *
   * @return string
   *   Key field name.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getEntityTypeKeyField(string $entityType): string;

  /**
   * Gets the Drupal field name from an entity type field.
   *
   * @param string $entityType
   *   Entity type.
   * @param string $fieldName
   *   Field name.
   *
   * @return string
   *   Drupal field name.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getDrupalFieldFromSplioFieldEntity(string $entityType, string $fieldName): string;

}
