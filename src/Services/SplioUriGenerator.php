<?php

namespace Drupal\splio\Services;

use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Generates the necessary URI for a request to Splio API.
 */
class SplioUriGenerator implements SplioUriGeneratorInterface {

  public const SPLIO_PATH = [
    'authenticate' => '/authenticate',
    'ping' => '/ping',
    'contacts_lists' => '/data/v1/lists',
    'contacts_fields' => '/data/fields/contacts',
    'contacts_create' => '/data/contacts',
    'contacts_read' => '/data/contacts/%s',
    'contacts_update' => '/data/contacts/%s',
    'contacts_delete' => '/data/contacts/%s',
    'products_fields' => '/data/fields/products',
    'products_create' => '/data/v1/products',
    'products_read' => '/data/v1/products/%s',
    'products_update' => '/data/v1/products/%s',
    'products_delete' => '/data/products/%s',
    'receipts_fields' => '/data/fields/orders',
    'receipts_create' => '/data/v1/orders',
    'receipts_read' => '/data/v2/orders/%s',
    'receipts_update' => '/data/v3/orders/%s',
    'receipts_delete' => '/data/orders/%s',
    'order_lines_fields' => '/data/fields/order-lines',
    'stores_fields' => '/data/fields/stores',
    'blacklist_add' => '/data/blacklists/email',
    'blacklist_get' => '/data/blacklists/emails',
    'unsubscribe' => '/data/contacts/%s/lists/unsubscribe',
  ];

  /**
   * Config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * API Splio base URI.
   *
   * @var string|null
   */
  protected ?string $baseUri;

  public function __construct(ConfigFactoryInterface $configFactory) {

    $this->configFactory = $configFactory;
    $this->baseUri = NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function generateBaseUri(array $options = NULL): string {

    if (!empty($options['server'])) {
      $server = $options['server'];
    }
    else {
      // From config form.
      $splioSettings = $this->configFactory->get('splio.settings');
      $server = $splioSettings->get('splio_server');
    }

    return !empty($server) ? "https://$server" : '';
  }

  /**
   * {@inheritDoc}
   */
  public function getUri(string $path, ?array $options = NULL): string {

    $baseUri = $this->generateBaseUri($options);
    $uri = $baseUri . $path;

    if (!empty($options['queryParams'])) {
      $uri .= '?' . http_build_query($options['queryParams']);
    }

    return $uri;
  }

}
