<?php

namespace Drupal\splio\Services;

use Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Queue\DelayedRequeueException;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\SuspendQueueException;
use Drupal\key\KeyRepository;
use Drupal\splio\Entity\SplioEntity;
use Drupal\splio\Event\SplioRequestEvent;
use Drupal\splio\Event\SplioResponseEvent;
use Drupal\splio\Exception\EntityIsEmptyException;
use Drupal\splio\Exception\EntityTypeNotFoundInSplioException;
use Drupal\splio\Exception\SuspendSplioQueueException;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Pool;
use GuzzleHttp\Promise\Utils;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\RequestOptions;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class SplioConnector.
 *
 * Manages the data synchronization with Splio. Allows the user to add an
 * entity to the process queue or directly sync it with Splio platform.
 * Performs the CRUD actions for any entity or set of entities received
 * and send the requests concurrently to Splio.
 */
class SplioConnector implements SplioConnectorInterface {

  use MessengerTrait;

  /**
   * Maximum elements per page returned by Splio.
   */
  const MAX_ELEMENTS = 500;

  /**
   * Delay for queue item, in seconds.
   */
  const QUEUE_DELAY = 300;

  /**
   * Config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  protected ConfigFactory $config;

  /**
   * Key manager service.
   *
   * @var \Drupal\key\KeyRepository
   */
  protected KeyRepository $keyManager;

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Queue factory service.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected QueueFactory $queueFactory;

  /**
   * Logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * Event dispatcher service.
   *
   * @var \Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher
   */
  protected ContainerAwareEventDispatcher $eventDispatcher;

  /**
   * HTTP client service.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected ClientInterface $client;

  /**
   * HTTP authenticator service.
   *
   * @var \Drupal\splio\Services\SplioAuthenticatorInterface
   */
  protected SplioAuthenticatorInterface $authenticator;

  /**
   * Splio URI generator service.
   *
   * @var \Drupal\splio\Services\SplioUriGeneratorInterface
   */
  protected SplioUriGeneratorInterface $uriGenerator;

  /**
   * Splio entity interface service.
   *
   * @var \Drupal\splio\Services\SplioEntityHandlerInterface
   */
  protected SplioEntityHandlerInterface $splioEntityHandler;

  /**
   * Splio entity structure generator service.
   *
   * @var \Drupal\splio\Services\SplioEntityStructureGeneratorInterface
   */
  protected SplioEntityStructureGeneratorInterface $structureGenerator;

  /**
   * Exception message parser service.
   *
   * @var \Drupal\splio\Services\ExceptionMessageParserInterface
   */
  protected ExceptionMessageParserInterface $exceptionMessageParser;

  public function __construct(
    KeyRepository $keyManager,
    ConfigFactory $config,
    EntityTypeManagerInterface $entityTypeManager,
    QueueFactory $queueFactory,
    ContainerAwareEventDispatcher $eventDispatcher,
    LoggerInterface $logger,
    ClientInterface $client,
    SplioAuthenticatorInterface $authenticator,
    SplioUriGeneratorInterface $uriGenerator,
    SplioEntityHandlerInterface $splioEntityHandler,
    SplioEntityStructureGeneratorInterface $splioEntityGenerator,
    ExceptionMessageParserInterface $exceptionMessageParser
  ) {
    $this->keyManager = $keyManager;
    $this->config = $config;
    $this->entityTypeManager = $entityTypeManager;
    $this->queueFactory = $queueFactory;
    $this->eventDispatcher = $eventDispatcher;
    $this->logger = $logger;
    $this->client = $client;
    $this->authenticator = $authenticator;
    $this->uriGenerator = $uriGenerator;
    $this->splioEntityHandler = $splioEntityHandler;
    $this->structureGenerator = $splioEntityGenerator;
    $this->exceptionMessageParser = $exceptionMessageParser;
  }

  /**
   * SplioConnector create method.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Main container.
   *
   * @return \Drupal\splio\Services\SplioConnector
   *   Returns an instance of SplioConnector with the injected services.
   */
  public static function create(ContainerInterface $container): SplioConnector {

    return new static(
      $container->get('key.repository'),
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('queue'),
      $container->get('event_dispatcher'),
      $container->get('logger.channel.splio'),
      $container->get('http_client'),
      $container->get('splio.authenticator'),
      $container->get('splio.uri_generator'),
      $container->get('splio.entity_handler'),
      $container->get('splio.entity_generator'),
      $container->get('splio.exception_parser')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function ping(array $options = NULL): bool {

    $requestOptions = [];
    $uri = $this->uriGenerator->getUri(
      SplioUriGenerator::SPLIO_PATH['ping'],
      $options
    );

    try {
      $requestOptions = $this->generateRequestOptions($options);

      $this->client->get($uri, $requestOptions);
    }
    catch (\Exception $exception) {
      $message = [
        'Ping to Splio failed.',
        'URI: ' . $uri . '.',
        'Body: ' . json_encode($requestOptions) . '.',
        'Message: ' . $this->exceptionMessageParser->getMessage(
          $exception
        ),
      ];

      $this->logger->warning(implode(' ', $message), ['splio']);

      return FALSE;
    }

    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function getContactsLists(): array {
    $lists = [];

    try {
      $uri = $this->uriGenerator->getUri(
        SplioUriGenerator::SPLIO_PATH[SplioEntity::TYPE_CONTACTS_LISTS]
      );
      $options = $this->generateRequestOptions();
      $body = $this->client->get($uri, $options)->getBody();
      $response = json_decode($body, TRUE);

      if (!empty($response['elements'])) {
        foreach ($response['elements'] as $element) {
          $lists[$element['id']] = $element['name'];
        }
      }
    }
    catch (RequestException $exception) {
      $message = $this->exceptionMessageParser->getMessage($exception);

      $this->logger->error('Unable to get Splio contact lists: ' . $message);
    }

    return $lists;
  }

  /**
   * {@inheritDoc}
   */
  public function getEntityTypeFieldsFromSplio(string $entityType): array {
    $fields = [];

    try {
      $uri = $this->uriGenerator->getUri(
        SplioUriGenerator::SPLIO_PATH[$entityType . '_fields'],
        [
          'queryParams' => [
            'per_page' => self::MAX_ELEMENTS,
          ],
        ],
      );
      $options = $this->generateRequestOptions();
      $response = $this->client->get($uri, $options)->getBody();
      $fields = json_decode($response, TRUE);
    }
    catch (RequestException $exception) {
      $message = $this->exceptionMessageParser->getMessage($exception);

      $this->messenger()->addError($message);

      $this->logger->error("Unable to get Splio $entityType fields: " . $message);
    }

    return $fields;
  }

  /**
   * {@inheritDoc}
   */
  public function createEntities(
    array $entities,
    int $concurrency = 10
  ): array {
    $requests = function ($entities) {
      foreach ($entities as $entity) {
        $action = 'create';

        try {
          $entityType = $this
            ->splioEntityHandler
            ->getSplioEntityTypeFromLocalEntity($entity);
        }
        catch (
          EntityTypeNotFoundInSplioException
          | EntityIsEmptyException
          $exception
        ) {
          continue;
        }

        // If an order_line is received, then the whole receipt (order)
        // which it belongs to will be created.
        $entityTypeTemp = $entityType;
        $entityType = $this
          ->splioEntityHandler
          ->getProperEntityType($entityTypeTemp);
        $entity = $this
          ->splioEntityHandler
          ->getProperEntity(
            $entityTypeTemp,
            $entity
          );
        $entityStructure = $this
          ->structureGenerator
          ->generateEntityStructure($entity, $action);
        // Add the type to the $entityStructure for events subscribers.
        $entityStructure['splioEntityType'] = $entityType;
        // Manage the event to be dispatched.
        $requestEvent = new SplioRequestEvent(
          $entity,
          $entityStructure
        );

        $this
          ->eventDispatcher
          ->dispatch(
            $requestEvent,
            SplioRequestEvent::SPLIO_CREATE
          );

        // In case someone captured the event and made changes in the
        // entityStructure, update the entityStructure.
        $entityStructure = $requestEvent->getSplioEntity();
        // Generate the URI based on the variables that have been just set.
        $uri = $this->uriGenerator->getUri(
          SplioUriGenerator::SPLIO_PATH[$entityType . '_create']
        );

        // Returns a promise once the function has finished.
        yield function () use ($uri, $entityStructure, $entity, $entityType, $action) {
          return $this->client->postAsync(
            $uri,
            $this->generateRequestOptions(['entityStructure' => $entityStructure])
          )->then(
            function (ResponseInterface $response) use ($entityStructure, $entity, $entityType, $action) {
              // Manage the event to be dispatched.
              $responseEvent = new SplioResponseEvent(
                $response,
                $entityStructure,
                $entity,
                $entityType,
                $action
              );
              $this->eventDispatcher->dispatch(
                $responseEvent,
                SplioResponseEvent::SPLIO_EVENT
              );

              return $response;
            },
            function (RequestException $exception) use ($entityStructure, $entity, $entityType, $action) {
              $responseEvent = new SplioResponseEvent(
                $exception,
                $entityStructure,
                $entity,
                $entityType,
                $action
              );
              $this->eventDispatcher->dispatch(
                $responseEvent,
                SplioResponseEvent::SPLIO_EVENT
              );

              if (!$responseEvent->isSilentException()) {
                $this->handleSplioResponseException(
                  $exception,
                  $entityStructure,
                  $entityType,
                  $action,
                  $responseEvent
                );
              }
            }
          );
        };
      }
    };

    return Pool::batch(
      $this->client,
      $requests($entities),
      ['concurrency' => $concurrency]
    );
  }

  /**
   * {@inheritDoc}
   */
  public function readEntities(array $entities, int $concurrency = 10): array {

    $requests = function ($entities) {
      foreach ($entities as $entity) {
        $action = 'read';

        try {
          $entityType = $this
            ->splioEntityHandler
            ->getSplioEntityTypeFromLocalEntity($entity);
        }
        catch (
          EntityTypeNotFoundInSplioException
          | EntityIsEmptyException
          $exception
        ) {
          continue;
        }

        // If an order_line is received, then the whole receipt (order)
        // which it belongs to, will be created.
        $entityTypeTemp = $entityType;
        $entityType = $this->splioEntityHandler->getProperEntityType($entityTypeTemp);
        $entity = $this
          ->splioEntityHandler
          ->getProperEntity(
            $entityTypeTemp,
            $entity
          );
        $entityStructure = $this
            ->structureGenerator
            ->generateEntityStructure($entity, $action);
        // Add the type to the $entityStructure for events subscribers.
        $entityStructure['splioEntityType'] = $entityType;
        $requestEvent = new SplioRequestEvent(
          $entity,
          $entityStructure
        );

        $this->eventDispatcher->dispatch(
          $requestEvent,
          SplioRequestEvent::SPLIO_READ
        );

        $uri = $this->uriGenerator->getUri(
          sprintf(
            SplioUriGenerator::SPLIO_PATH[$entityType . '_read'],
            $this->splioEntityHandler->getEntityKeyFieldValue($entityType, $entity)
          )
        );

        // Returns a promise once the function has finished.
        yield function () use ($uri) {
          return $this->client->getAsync(
            $uri,
            $this->generateRequestOptions()
          )->then(
            function (ResponseInterface $response) {
              try {
                // Decode the received response and add the proper keyField.
                $response = json_decode($response->getBody(), TRUE);
              }
              catch (\Error $exception) {
                $this->logger->error(
                  "Unable to decode Splio API response: $exception"
                );
              }

              return $response;
            },
            function (RequestException $exception) {
              if ($exception->getCode() != Response::HTTP_NOT_FOUND) {
                $this->logger
                  ->error(
                    "Unable to retrieve data from Splio API. %message.",
                    [
                      '%message' => $exception->getMessage(),
                    ]
                  );
              }

              throw $exception;
            }
          );
        };
      }
    };

    return Pool::batch(
      $this->client,
      $requests($entities),
      ['concurrency' => $concurrency]
    );
  }

  /**
   * {@inheritDoc}
   */
  public function updateEntities(array $entities, int $concurrency = 10):array {

    $requests = function ($entities) {
      foreach ($entities as $entity) {
        $action = 'update';

        try {
          $entityType = $this
            ->splioEntityHandler
            ->getSplioEntityTypeFromLocalEntity($entity);
        }
        catch (
          EntityTypeNotFoundInSplioException
          | EntityIsEmptyException
          $exception
        ) {
          continue;
        }

        $entityTypeTemp = $entityType;
        $entityType = $this
          ->splioEntityHandler
          ->getProperEntityType($entityTypeTemp);
        $entity = $this
          ->splioEntityHandler
          ->getProperEntity(
            $entityTypeTemp,
            $entity
          );
        $entityStructure = $this
          ->structureGenerator
          ->generateEntityStructure($entity, $action);
        // Add the type to the $entityStructure for events subscribers.
        $entityStructure['splioEntityType'] = $entityType;
        $requestEvent = new SplioRequestEvent(
          $entity,
          $entityStructure
        );

        try {
          $this->eventDispatcher->dispatch(
            $requestEvent,
            SplioRequestEvent::SPLIO_UPDATE,
          );
        }
        catch (\Exception $exception) {
          $this->logger->info($exception->getMessage());

          continue;
        }

        $entityStructure = $requestEvent->getSplioEntity();
        $sourceEntity = $entity->original ?: $entity;
        $uri = $this->uriGenerator->getUri(
          sprintf(
            SplioUriGenerator::SPLIO_PATH[$entityType . '_update'],
            $this->splioEntityHandler->getEntityKeyFieldValue($entityType, $sourceEntity)
          )
        );

        yield function () use ($uri, $entityStructure, $entity, $entityType, $action) {
          return $this->client->patchAsync(
            $uri,
            $this->generateRequestOptions(['entityStructure' => $entityStructure])
          )->then(
            function (ResponseInterface $response) use ($entityStructure, $entity, $entityType, $action) {
              $responseEvent = new SplioResponseEvent(
                $response,
                $entityStructure,
                $entity,
                $entityType,
                $action
              );

              $this->eventDispatcher->dispatch(
                $responseEvent,
                SplioResponseEvent::SPLIO_EVENT
              );

              return $response;
            },
            function (RequestException $exception) use ($entityStructure, $entity, $entityType, $action) {
              $responseEvent = new SplioResponseEvent(
                $exception,
                $entityStructure,
                $entity,
                $entityType,
                $action
              );

              $this->eventDispatcher->dispatch(
                $responseEvent,
                SplioResponseEvent::SPLIO_EVENT
              );

              if (!$responseEvent->isSilentException()) {
                $this->handleSplioResponseException(
                  $exception,
                  $entityStructure,
                  $entityType,
                  $action,
                  $responseEvent
                );
              }
            }
          );
        };
      }
    };

    return Pool::batch(
      $this->client,
      $requests($entities),
      ['concurrency' => $concurrency]
    );
  }

  /**
   * {@inheritDoc}
   */
  public function deleteEntities(array $entities, int $concurrency = 10): array {

    $requests = function ($entities) {
      foreach ($entities as $entity) {
        $action = 'delete';

        try {
          $entityType = $this
            ->splioEntityHandler
            ->getSplioEntityTypeFromLocalEntity($entity);
        }
        catch (
          EntityTypeNotFoundInSplioException
          | EntityIsEmptyException
          $exception
        ) {
          continue;
        }

        $entityTypeTemp = $entityType;
        $entityType = $this
          ->splioEntityHandler
          ->getProperEntityType($entityTypeTemp);
        $entity = $this
          ->splioEntityHandler
          ->getProperEntity(
            $entityTypeTemp,
            $entity
          );
        $entityStructure = $this
          ->structureGenerator
          ->generateEntityStructure($entity, $action);
        // Add the type to the $entityStructure for events subscribers.
        $entityStructure['splioEntityType'] = $entityType;
        // Manage the event to be dispatched.
        $requestEvent = new SplioRequestEvent(
          $entity,
          $entityStructure
        );

        // No delete subscribers so far. This will be skipped.
        $this->eventDispatcher->dispatch(
          $requestEvent,
          SplioRequestEvent::SPLIO_DELETE
        );

        $entityStructure = $requestEvent->getSplioEntity();
        $sourceEntity = $entity->original ?: $entity;
        $uri = $this->uriGenerator->getUri(
          sprintf(
            SplioUriGenerator::SPLIO_PATH[$entityType . '_delete'],
            $this->splioEntityHandler->getEntityKeyFieldValue($entityType, $sourceEntity)
          )
        );

        yield function () use ($uri, $entityStructure, $entity, $entityType, $action) {
          return $this->client->deleteAsync(
            $uri,
            $this->generateRequestOptions()
          )->then(
            function (ResponseInterface $response) use ($entityStructure, $entity, $entityType, $action) {
              // Manage the event to be dispatched.
              $responseEvent = new SplioResponseEvent(
                $response,
                $entityStructure,
                $entity,
                $entityType,
                $action
              );
              $this->eventDispatcher->dispatch(
                $responseEvent,
                SplioResponseEvent::SPLIO_EVENT
              );

              return $response;
            },
            function (RequestException $exception) use ($entityStructure, $entity, $entityType, $action) {
              $responseEvent = new SplioResponseEvent(
                $exception,
                $entityStructure,
                $entity,
                $entityType,
                $action
              );

              $this->eventDispatcher->dispatch(
                $responseEvent,
                SplioResponseEvent::SPLIO_EVENT
              );

              if (!$responseEvent->isSilentException()) {
                $this->handleSplioResponseException(
                  $exception,
                  $entityStructure,
                  $entityType,
                  $action,
                  $responseEvent
                );
              }
            }
          );
        };
      }
    };

    return Pool::batch(
      $this->client,
      $requests($entities),
      ['concurrency' => $concurrency]
    );
  }

  /**
   * Parses entity structure format, to adapt to Splio necessities.
   *
   * @param array $structure
   *   Array with data that will be sent to Splio.
   *
   * @return string
   *   If json encoding is correct, json with entity structure.
   *   Empty json, otherwise.
   */
  private function parseEntityStructure(array $structure): string {

    if (!empty($structure[SplioEntityStructureGenerator::CUSTOM_FIELDS])) {
      // Remove indexes, since Splio does not accept them.
      $structure[SplioEntityStructureGenerator::CUSTOM_FIELDS] =
        array_values($structure[SplioEntityStructureGenerator::CUSTOM_FIELDS]);
    }

    $result = json_encode($structure);

    return (FALSE !== $result) ? $result : json_decode('{}');
  }

  /**
   * Handles Splio API error response, and throws exception in accordance.
   *
   * @param \Exception $exception
   *   Splio response exception.
   * @param array $entityStructure
   *   Entity structure sent to Splio.
   * @param string $entityType
   *   Splio entity type.
   * @param string $action
   *   Desired action to be applied to the entity structure.
   *
   * @throws \Exception
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  private function handleSplioResponseException(
    \Exception $exception,
    array $entityStructure,
    string $entityType,
    string $action,
    SplioResponseEvent $responseEvent
  ): void {

    $codeStatus = $exception->getCode();
    $message = $this->exceptionMessageParser->getMessage(
      $exception
    );

    // Move custom_fields to the end of the array,
    // so we can read standard fields in log.
    uksort(
      $entityStructure,
      static function ($x, $y) {
        return (SplioEntityStructureGenerator::CUSTOM_FIELDS === $y) ? -1 : 1;
      }
    );

    $logMessage = "Splio error %code (%action %entityType): %message. JSON body: %entityStructure";
    $logContext = [
      '%action' => $action,
      '%entityType' => $entityType,
      '%code' => $codeStatus,
      '%message' => $message,
      '%entityStructure' => $this->parseEntityStructure($entityStructure),
    ];

    if (Response::HTTP_UNAUTHORIZED === $codeStatus) {
      $this->authenticator->authenticate();

      $this->logger->warning($logMessage, $logContext);
    }
    elseif (
      Response::HTTP_BAD_GATEWAY === $codeStatus
      || SplioResponseEvent::LOG_LEVEL_WARNING === $responseEvent->getLogLevel()
    ) {
      $this->logger->warning($logMessage, $logContext);
    }
    else {
      $this->logger->error($logMessage, $logContext);
    }

    if (
      ($codeStatus >= 400 && $codeStatus <= 499)
      || Response::HTTP_BAD_GATEWAY === $codeStatus
    ) {
      throw new DelayedRequeueException(
        self::QUEUE_DELAY,
        $message,
        $codeStatus
      );
    }

    throw new SuspendSplioQueueException($message);
  }

  /**
   * {@inheritDoc}
   */
  public function addEmailsToBlacklist(array $emails): array {

    $uri = $this->uriGenerator->getUri(
      SplioUriGenerator::SPLIO_PATH['blacklist_add']
    );
    $body = [
      'body' => json_encode(
        [
          'data' => $emails,
        ]
      ),
    ];
    $promises[] = $this->client->postAsync(
      $uri,
      $this->generateRequestOptions($body)
    )->then(
      function (ResponseInterface $response) {
        return [
          'status_code' => $response->getStatusCode(),
          'message' => $response->getBody(),
        ];
      },
      function (RequestException $exception) {
        return [
          'status_code' => $exception->getCode(),
          'message' => $this->exceptionMessageParser->getMessage(
            $exception
          ),
        ];
      }
    );

    return Utils::unwrap($promises);
  }

  /**
   * {@inheritDoc}
   */
  public function isEmailBlacklisted(string $email): bool {

    $options = [
      'queryParams' => [
        'term' => $email,
      ],
    ];
    $uri = $this->uriGenerator->getUri(
      SplioUriGenerator::SPLIO_PATH['blacklist_get'],
      $options
    );
    $response = $this->client->get(
      $uri,
      $this->generateRequestOptions()
    );

    if (Response::HTTP_OK !== $response->getStatusCode()) {
      return FALSE;
    }

    // Since Splio returns an array with all found emails,
    // we need to check if given email is in the array.
    $results = json_decode($response->getBody(), TRUE);

    foreach ($results['elements'] as $element) {
      if ($email === $element['email']) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * Sends a request to unsubscribe a contact from a set of lists.
   *
   * @param string $keyValue
   *   Contact key field value.
   * @param array $lists
   *   Lists to unsubscribe from.
   *
   * @return array
   *   An array
   *
   * @throws \Throwable
   */
  public function unsubscribeFromLists(string $keyValue, array $lists): array {

    $uri = $this->uriGenerator->getUri(
      sprintf(
        SplioUriGenerator::SPLIO_PATH['unsubscribe'],
        $keyValue
      )
    );
    $body = [
      'json' => [
        'list_ids' => $lists,
      ],
    ];
    $promises[] = $this->client->postAsync(
      $uri,
      $this->generateRequestOptions($body)
    )->then(
      function (ResponseInterface $response) {
        return [
          'status_code' => $response->getStatusCode(),
          'message' => $response->getBody(),
        ];
      },
      function (RequestException $exception) {
        return [
          'status_code' => $exception->getCode(),
          'message' => $this->exceptionMessageParser->getMessage(
            $exception
          ),
        ];
      }
    );

    return Utils::unwrap($promises);
  }

  /**
   * Generates Guzzle client request options.
   *
   * @param array|null $data
   *   Optional input data.
   *
   * @return array
   *   Guzzle client request options.
   *
   * @throws \Drupal\splio\Exception\TokenNotReceivedException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  protected function generateRequestOptions(?array $data = []): array {

    $requestOptions = [
      RequestOptions::HEADERS => [
        'Accept' => 'application/json',
        'Content-Type' => 'application/json',
        'Authorization' => 'Bearer ' . $this->authenticator->authenticate(),
      ],
      RequestOptions::CONNECT_TIMEOUT => $data['connection_timeout'] ?? '0',
      RequestOptions::TIMEOUT => $data['timeout'] ?? '0',
    ];

    if (!empty($data['entityStructure'])) {
      $data['entityStructure'] = $this->removeNotNeededFields(
        $data['entityStructure']
      );

      $requestOptions[RequestOptions::BODY] = $this->parseEntityStructure(
        $data['entityStructure']
      );
    }
    elseif (!empty($data['body'])) {
      $requestOptions[RequestOptions::BODY] = $data['body'];
    }
    elseif (!empty($data['json'])) {
      $requestOptions[RequestOptions::JSON] = $data['json'];
    }
    elseif (!empty($data['formParams'])) {
      $requestOptions[RequestOptions::FORM_PARAMS] = $data['formParams'];
    }

    return $requestOptions;
  }

  /**
   * Removes not needed fields from Splio request entity.
   *
   * @param array $data
   *   All entity structure fields.
   *
   * @return array
   *   Splio entity structure.
   */
  private function removeNotNeededFields(array $data): array {

    $structure = [];

    foreach ($data as $key => $field) {
      if (
        !$this->structureGenerator->isFieldNotNeeded(
          $key,
          $data['splioEntityType'],
          $data['action'],
        )
        && !in_array($key, ['action', 'splioEntityType'], TRUE)
      ) {
        $structure[$key] = $field;
      }
    }

    return $structure;
  }

}
