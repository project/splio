<?php

namespace Drupal\splio\Services;

interface SplioApiKeyGetterInterface {

  /**
   * Gets api key from config.
   *
   * @param string $settings
   *   Name of settings.
   *
   * @return string|null
   *   API key value.
   */
  public function getApiKey(string $settings): ?string;

}
