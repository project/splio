<?php

namespace Drupal\splio\Services;

interface SplioContactsListsManagerInterface {

  /**
   * Gets all contacts lists ids stored in the entity that sent to Splio.
   *
   * @param array $structure
   *   Data structure.
   *
   * @return array
   *   List of ids.
   */
  public function getIdListFromEntityStructure(array $structure): array;

  /**
   * Gets all local contacts lists ids.
   *
   * @return array
   *   List of ids.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getIdsListFromStorage(): array;

}
