<?php

namespace Drupal\splio\Services;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\splio\Entity\SplioEntity;
use Drupal\splio\Exception\EntityTypeNotFoundInSplioException;

class SplioEntityStructureGenerator implements SplioEntityStructureGeneratorInterface {

  const CUSTOM_FIELDS = 'custom_fields';

  const PREFIX_ORDER_LINES = 'order_lines_';

  // These fields cannot be sent to Splio, or request will fail.
  const FIELDS_NOT_NEEDED = [
    SplioEntity::TYPE_CONTACTS => [
      'update' => [
        'email',
      ],
    ],
    SplioEntity::TYPE_PRODUCTS => [
      'create' => [
        'date_added',
      ],
      'update' => [
        'external_id',
        'date_added',
      ],
    ],
    SplioEntity::TYPE_RECEIPTS => [
      'update' => [
        'external_id',
        'date_added',
        'date_order',
        'id_store',
      ],
    ],
  ];

  /**
   * Splio entity handler service.
   *
   * @var \Drupal\splio\Services\SplioEntityHandlerInterface
   */
  private SplioEntityHandlerInterface $splioEntityHandler;

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private EntityTypeManagerInterface $entityTypeManager;

  /**
   * Entity value handler service.
   *
   * @var \Drupal\splio\Services\EntityValueHandlerInterface
   */
  private EntityValueHandlerInterface $entityValueHandler;

  /**
   * Entity data structure that will be sent to Splio.
   *
   * @var array
   */
  private array $structure = [];

  public function __construct(
    SplioEntityHandlerInterface $splioEntityHandler,
    EntityTypeManagerInterface $entityTypeManager,
    EntityValueHandlerInterface $entityValueHandler
  ) {

    $this->splioEntityHandler = $splioEntityHandler;
    $this->entityTypeManager = $entityTypeManager;
    $this->entityValueHandler = $entityValueHandler;
  }

  /**
   * {@inheritDoc}
   */
  public function setStructure(array $data): void {

    $this->structure = $data;
  }

  /**
   * {@inheritDoc}
   */
  public function getStructure(): array {

    return $this->structure;
  }

  /**
   * {@inheritDoc}
   */
  public function generateEntityStructure(
    EntityInterface $entity,
    string $action
  ): array {

    $this->structure = [
      'action' => $action,
    ];

    try {
      $entityType = $this
        ->splioEntityHandler
        ->getSplioEntityTypeFromLocalEntity($entity);
    }
    catch (EntityTypeNotFoundInSplioException $exception) {
      return $this->structure;
    }

    $entityFields = $this
      ->splioEntityHandler
      ->getLocalSplioEntitiesFields(['splio_entity' => $entityType]);

    if (\count($entityFields) > 0) {
      $this->generateStandardFields($entityFields, $entity);
    }

    switch ($entityType) {
      case SplioEntity::TYPE_PRODUCTS:
        $this->structure['price'] = (float) $this->structure['price'];

        break;

      case SplioEntity::TYPE_RECEIPTS:
        $orderLinesEntities = $this->getOrderLinesEntities($entity);

        $this->generateOrderLinesFields($orderLinesEntities);
        $this->checkOrderLines();

        // @ugly: For some reason, Splio returns "date_order" in fields endpoint,
        // but only accepts "ordered_at" ¯\_(^__^)_/¯.
        $this->structure['ordered_at'] = $this->structure['date_order'];
        unset($this->structure['date_order']);

        break;

      case SplioEntity::TYPE_CONTACTS:
        // Add "lists" index to structure.
        $contactsLists = $this->splioEntityHandler
          ->getLocalSplioEntitiesFields(['splio_entity' => SplioEntity::TYPE_CONTACTS_LISTS]);

        $this->structure['lists'] = $this->generateContactListsFields(
          $entity,
          $contactsLists
        );

        break;
    }

    return $this->structure;
  }

  /**
   * {@inheritDoc}
   */
  public function isFieldNotNeeded(
    string $field,
    string $entityType,
    string $action
  ): bool {

    return in_array($field, self::FIELDS_NOT_NEEDED[$entityType][$action] ?? []);
  }

  /**
   * Generates standard fields for given entity.
   *
   * @param array $entityFields
   *   All entity type fields.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity to get values from.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function generateStandardFields(
    array $entityFields,
    EntityInterface $entity
  ): void {

    foreach ($entityFields as $field) {
      $drupalField = $field->getDrupalField();
      $splioField = $field->getSplioField();
      $fieldValue = $this->entityValueHandler->getFieldValue($drupalField, $entity);
      $fieldValue = $this->entityValueHandler->formatField($field, $fieldValue);

      if ($field->isDefaultField()) {
        // @ugly By some reason, PK "extid" is translated to "external_id".
        if ($field->isKeyField()) {
          $this->structure['external_id'] = $fieldValue;
        }
        else {
          // Arrays are not allowed as field values.
          $this->structure[$splioField] = is_array($fieldValue)
            ? end($fieldValue) : $fieldValue;
        }
      }
      else {
        $this->structure[self::CUSTOM_FIELDS][$splioField] = [
          'name' => $splioField,
          'value' => (string) (is_array($fieldValue) ? end($fieldValue) : $fieldValue),
        ];
      }
    }
  }

  /**
   * Generates the inner 'order_lines' JSON structure for any order received.
   *
   * Receives a receipt and loads all the order lines associated to it.
   * Generates the proper JSON structure and returns it as an array.
   *
   * @param \Drupal\Core\Entity\EntityInterface $orderLineEntity
   *   The receipt entity that will be used as reference to generate the inner
   *   order_lines structure.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\splio\Exception\EntitiesNotConfiguredYetException
   */
  private function getOrderLinesEntities(
    EntityInterface $orderLineEntity
  ): array {

    // Load the local entity configured as Splio order_lines.
    $entities = $this->splioEntityHandler->getLocalSplioEntitiesConfig();
    $orderLineType = $entities[SplioEntity::TYPE_ORDER_LINES]['local_entity'];
    $entityFields = $this->splioEntityHandler
      ->getLocalSplioEntitiesFields(['splio_entity' => SplioEntity::TYPE_ORDER_LINES]);
    // Get the local field configured as [order_lines_]id_order.
    $orderIdField = $entityFields[self::PREFIX_ORDER_LINES . 'id_order']
      ->getDrupalField();

    // If the value turns to be an entity reference then store it's key.
    $orderIdField = (strncmp($orderIdField, "{{", 2) == 0)
      ? array_keys($this->entityValueHandler->getFieldValue($orderIdField, $orderLineEntity))[0]
      : $orderIdField;
    $orderId = $this->entityValueHandler->getFieldValue(
      $orderIdField,
      $orderLineEntity
    );
    $orderId = is_array($orderId) ? end($orderId) : $orderId;

    // Load all the order_lines that belong to that orderId.
    return $this->entityTypeManager
      ->getStorage($orderLineType)
      ->loadByProperties([$orderIdField => $orderId]);
  }

  /**
   * Load the splio entity fields for order_lines.
   *
   * @param array $orderLinesEntities
   *   Order lines entities from an order entity.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function generateOrderLinesFields(array $orderLinesEntities) {

    $entityFields = $this
      ->splioEntityHandler
      ->getLocalSplioEntitiesFields(['splio_entity' => SplioEntity::TYPE_ORDER_LINES]);

    foreach ($orderLinesEntities as $orderLineEntity) {
      $orderLineFields = [];

      foreach ($entityFields as $field) {
        $drupalField = $field->getDrupalField();
        $splioField = $field->getSplioField();
        $fieldValue = $this->entityValueHandler->getFieldValue(
          $drupalField,
          $orderLineEntity
        );

        if (!empty($fieldValue)) {
          $fieldValue = is_array($fieldValue) ? end($fieldValue) : $fieldValue;
        }

        if ($field->isDefaultField()) {
          $orderLineFields[$splioField] = $fieldValue;
        }
        else {
          $orderLineFields[self::CUSTOM_FIELDS][] = [
            'name' => $splioField,
            'value' => $fieldValue,
          ];
        }
      }

      $orderLinesStructure[] = $orderLineFields;
    }

    $this->structure[SplioEntity::TYPE_PRODUCTS] = $orderLinesStructure;
  }

  /**
   * Performs a check to ensure there are no order_lines with empty key.
   */
  private function checkOrderLines() {

    if (empty($this->structure[SplioEntity::TYPE_PRODUCTS])) {
      return;
    }

    foreach ($this->structure[SplioEntity::TYPE_PRODUCTS] as $productKey => $productDef) {
      if (empty($productDef['id_order']) || empty($productDef['id_product'])) {
        unset($this->structure[SplioEntity::TYPE_PRODUCTS][$productKey]);
      }
      else {
        // For some reason, Splio returns "id_product" in fields endpoint,
        // but only accepts "product_id" ¯\_(^__^)_/¯.
        $this->structure[SplioEntity::TYPE_PRODUCTS][$productKey]['product_id'] = $productDef['id_product'];
        // Splio expects 'quantity' to be an int.
        $this->structure[SplioEntity::TYPE_PRODUCTS][$productKey]['quantity'] = (int) ($productDef['quantity'] ?? 0);

        unset($this->structure[SplioEntity::TYPE_PRODUCTS][$productKey]['id_product']);
      }
    }
  }

  /**
   * Generates the 'contacts_lists' structure for any contact received.
   *
   * Receives a contact, generates the proper structure for the lists
   * it belongs to and returns it as an array.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The contact entity that will be used as reference to generate the inner
   *   contacts_list structure.
   * @param array $contactsLists
   *   Array of lists that the user will be subscribed to.
   *
   * @return array
   *   Returns an array containing the inner 'contacts_list' structure
   *   for the given contact.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function generateContactListsFields(
    EntityInterface $entity,
    array $contactsLists
  ): array {

    $structure = [];

    /** @var \Drupal\splio\Entity\SplioField $list */
    foreach ($contactsLists as $list) {
      // Drupal field storing the subscription of the user for the current list.
      $contactsListsDrupalField = $list->getDrupalField();
      $contactFieldValue = $this->entityValueHandler->getFieldValue(
        $contactsListsDrupalField,
        $entity
      );

      // If the value is not empty, and it is TRUE, 1, or it contains the name
      // of the current list, the user will be subscribed to that list. In any
      // other case, the user will be unsubscribed from that particular list,
      // except when the local list field is set to none by the user.
      if (!empty($contactFieldValue)) {
        if (
          TRUE === $contactFieldValue
          || 1 == $contactFieldValue
          || $list->getSplioField() == $contactFieldValue
          || (
            is_array($contactFieldValue)
            && in_array($list->getSplioField(), $contactFieldValue)
          )
        ) {
          $structure[] = [
            "id" => $list->getListIndex(),
          ];
        }
      }
    }

    return $structure;
  }

}
