<?php

namespace Drupal\splio\Services;

use Drupal\Core\Entity\EntityInterface;

/**
 * Entity helper to get dynamic "original" property, while running tests.
 */
class EntityHelper implements EntityHelperInterface {

  /**
   * {@inheritDoc}
   */
  public function getOriginal(EntityInterface $entity) {

    return $entity->original ?? NULL;
  }

}
