<?php

namespace Drupal\splio\Services;

use Drupal\Core\Entity\EntityInterface;

interface SplioEntityStructureGeneratorInterface {

  /**
   * Sets structure values.
   *
   * @param array $data
   *   Values set.
   */
  public function setStructure(array $data): void;

  /**
   * Gets structure values.
   *
   * @return array
   *    Structure values.
   */
  public function getStructure(): array;

  /**
   * Creates the Splio entity's JSON structure to make requests to Splio API.
   *
   * Receives a Drupal entity and generates the proper JSON structure.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Receives a Drupal Entity.
   * @param string $action
   *   Request method.
   *
   * @return array
   *   Returns an array containing the entity structure ready to be encoded
   *   to JSON.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\splio\Exception\EntitiesNotConfiguredYetException
   * @throws \Drupal\splio\Exception\EntityIsEmptyException
   */
  public function generateEntityStructure(EntityInterface $entity, string $action): array;

  /**
   * Checks field is not needed for a particular entity type and action.
   *
   * Splio may return and exception if there is an unwanted field in the
   * request.
   *
   * @param string $field
   *   Entity field.
   * @param string $entityType
   *   Entity type.
   * @param string $action
   *   Action (create, update, read, delete).
   *
   * @return bool
   *   TRUE if it is not needed, FALSE otherwise.
   */
  public function isFieldNotNeeded(string $field, string $entityType, string $action): bool;

}
