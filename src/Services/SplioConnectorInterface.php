<?php

namespace Drupal\splio\Services;

interface SplioConnectorInterface {

  /**
   * Ping to Splio.
   *
   * @param array|null $options
   *   Options for request.
   *
   * @return bool
   *   TRUE if ping was successful. FALSE, otherwise.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function ping(array $options = NULL): bool;

  /**
   * Returns an array containing the contacts lists defined in Splio.
   *
   * @return array
   *   Array containing the contact lists.
   *
   * @throws \Drupal\splio\Exception\TokenNotReceivedException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getContactsLists(): array;

  /**
   * Gets all the fields from a Splio entity type.
   *
   * @param string $entityType
   *   Desired entity type to get its fields.
   *
   * @return array
   *   Entity type fields.
   *
   * @throws \Drupal\splio\Exception\TokenNotReceivedException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getEntityTypeFieldsFromSplio(string $entityType): array;

  /**
   * Creates new users in the Splio platform.
   *
   * Receives an array of Drupal entities which will be created in the Splio
   * platform. Finds the proper Splio entity type for each Drupal entity
   * received, generates the proper JSON structure and makes a POST request to
   * Splio.
   *
   * @param array $entities
   *   Receives an array of Drupal entities.
   * @param int $concurrency
   *   Defines how many requests should be sent concurrently. By default, 10.
   *
   * @return array
   *   The result of the request.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\splio\Exception\EntitiesNotConfiguredYetException
   * @throws \Drupal\splio\Exception\EntityIsEmptyException
   * @throws \Drupal\splio\Exception\EntityTypeNotFoundInSplioException
   */
  public function createEntities(array $entities, int $concurrency = 10): array;

  /**
   * Returns a list of existing users from the Splio platform.
   *
   * Receives an array of users which will be retrieved from the Splio platform.
   * Returns an array with the desired entities. The structure of each entity
   * will be the one Splio returns for the GET requests.
   *
   * @param array $entities
   *   Receives an array of Drupal entities.
   * @param int $concurrency
   *   Defines how many requests should be sent concurrently. By default, 10.
   *
   * @return array
   *   Returns an array containing the entities requested to Splio.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\splio\Exception\EntitiesNotConfiguredYetException
   * @throws \Drupal\splio\Exception\EntityIsEmptyException
   * @throws \Drupal\splio\Exception\EntityTypeNotFoundInSplioException
   */
  public function readEntities(array $entities, int $concurrency = 10): array;

  /**
   * Updates a set of entities in the Splio platform.
   *
   * Receives an array of Drupal entities which will be updated in the Splio
   * platform. Finds the proper Splio entity type for each Drupal entity
   * received, generates the proper JSON structure and makes a PUT request
   * to Splio.
   *
   * @param array $entities
   *   Receives an array of Drupal entities.
   * @param int $concurrency
   *   Defines how many requests should be sent concurrently. By default, 10.
   *
   * @return array
   *   The result of the request.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\splio\Exception\EntitiesNotConfiguredYetException
   * @throws \Drupal\splio\Exception\EntityIsEmptyException
   * @throws \Drupal\splio\Exception\EntityTypeNotFoundInSplioException
   */
  public function updateEntities(array $entities, int $concurrency = 10): array;

  /**
   * Deletes a set of users from the Splio platform.
   *
   * Receives an array of entities to be deleted from the Splio platform.
   *
   * @param array $entities
   *   Receives an array of Drupal entities.
   * @param int $concurrency
   *   Defines how many requests should be sent concurrently. By default, 10.
   *
   * @return array
   *   The result of the request.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\splio\Exception\EntitiesNotConfiguredYetException
   * @throws \Drupal\splio\Exception\EntityIsEmptyException
   * @throws \Drupal\splio\Exception\EntityTypeNotFoundInSplioException
   */
  public function deleteEntities(array $entities, int $concurrency = 10) : array;

  /**
   * Adds a list of given emails to Splio blacklist.
   *
   * @param string[] $emails
   *   Emails to add to blacklist.
   *
   * @return array
   *   Response with results.
   *   [
   *      'successes': array of emails blacklisted,
   *      'failures': array of emails not blacklisted,
   *   ]
   *
   * @throws \Throwable
   */
  public function addEmailsToBlacklist(array $emails): array;

  /**
   * Checks if a given email is blacklisted in Splio.
   *
   * @param string $email
   *   Email string.
   *
   * @return bool
   *   TRUE if it is blacklisted; FALSE otherwise.
   *
   * @throws \Drupal\splio\Exception\TokenNotReceivedException
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function isEmailBlacklisted(string $email): bool;

}
