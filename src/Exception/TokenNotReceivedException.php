<?php

namespace Drupal\splio\Exception;

/**
 * Exception when Splio does not return a token, when authentication.
 */
class TokenNotReceivedException extends \Exception {

}
