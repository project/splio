<?php

namespace Drupal\splio\Exception;

use Symfony\Component\HttpFoundation\Response;

class EntityIsEmptyException extends \Exception {

  /**
   * Constructor.
   */
  public function __construct() {
    parent::__construct(
      'Entity is empty',
      Response::HTTP_NOT_FOUND
    );
  }

}
