<?php

namespace Drupal\splio\Exception;

/**
 * Exception when the queue item has no valid data.
 */
class QueueItemHasNotValidDataException extends \Exception {

  /**
   * Constructor.
   */
  public function __construct() {
    parent::__construct("Queue item has no valid data.");
  }

}
