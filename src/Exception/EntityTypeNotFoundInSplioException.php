<?php

namespace Drupal\splio\Exception;

use Symfony\Component\HttpFoundation\Response;

/**
 * Exception when a local entity is not found in Splio.
 */
class EntityTypeNotFoundInSplioException extends \Exception {

  /**
   * Constructor.
   *
   * @param string $entityType
   *   Given entity.
   */
  public function __construct(string $entityType) {
    parent::__construct(
      "The entity $entityType does not exist in Splio. Make sure you have mapped properly the Splio entities from the module config.",
      Response::HTTP_NOT_FOUND
    );
  }

}
