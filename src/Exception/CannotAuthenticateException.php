<?php

namespace Drupal\splio\Exception;

/**
 * Exception when it is not possible to authenticate to Splio.
 */
class CannotAuthenticateException extends \Exception {

}
