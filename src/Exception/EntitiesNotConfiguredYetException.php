<?php

namespace Drupal\splio\Exception;

use Symfony\Component\HttpFoundation\Response;

/**
 * Exception when Splio entities have not been configured in Drupal.
 */
class EntitiesNotConfiguredYetException extends \Exception {

  /**
   * Constructor.
   */
  public function __construct() {
    parent::__construct(
      t('You have not configured the relation of your entities with Splio ones yet.'),
      Response::HTTP_CONFLICT
    );
  }

}
