<?php

namespace Drupal\splio\Exception;

/**
 * Exception when a user tried to modify their key field value.
 */
class UserModifyUniqueKeyFieldValueException extends \Exception {

  /**
   * Constructor.
   *
   * @param string $oldValue
   *   Key field old value.
   * @param string $newValue
   *   Key field new value.
   */
  public function __construct(string $oldValue, string $newValue) {
    parent::__construct(
      "User tried to modify their unique key field value from $oldValue to $newValue"
    );
  }

}
