<?php

namespace Drupal\splio_utils\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\splio\Exception\TokenNotReceivedException;
use Drupal\splio\Services\SplioConnectorInterface;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class SplioBlacklistResource.
 *
 * @RestResource(
 *   id = "splio_blacklist_manager",
 *   label = @Translation("Splio blacklist manager"),
 *   uri_paths = {
 *     "canonical" = "/splio/blacklist/{email}",
 *     "create" = "/splio/blacklist",
 *   }
 * )
 */
class SplioBlacklistResource extends AbstractResource {

  /**
   * Connector to Splio services.
   *
   * @var \Drupal\splio\services\SplioConnectorInterface
   */
  private SplioConnectorInterface $splioConnector;

  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    SplioConnectorInterface $splioConnector
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $serializer_formats,
      $logger
    );

    $this->splioConnector = $splioConnector;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('splio_blacklist'),
      $container->get('splio.splio_connector')
    );
  }

  /**
   * Returns whether an email address is blacklisted in Splio.
   *
   * @param string $email
   *   The email address to be checked.
   *
   * @return \Drupal\rest\ResourceResponse
   *   - status_code
   *   - message:
   *    {
   *      "blacklisted":
   *        "true" if email address is blacklisted; "false" otherwise.
   *    }
   */
  public function get(string $email): ResourceResponse {
    $isBlacklisted = NULL;
    $message = '';

    try {
      $isBlacklisted = $this->splioConnector->isEmailBlacklisted($email);
      $statusCode = Response::HTTP_OK;

    }
    catch (
      TokenNotReceivedException
      | GuzzleException
      $exception
    ) {
      $message = $exception->getMessage();
      $statusCode = Response::HTTP_INTERNAL_SERVER_ERROR;
    }

    return $this->returnResponse(
      $statusCode,
      [
        'blacklisted' => $isBlacklisted,
        'message' => $message,
      ]
    );
  }

  /**
   * Adds various email addresses to the Splio Blacklist.
   *
   * @param array $data
   *   The email addresses to be blacklisted.
   *
   * @return \Drupal\rest\ResourceResponse
   *   Response with results:
   *   [
   *      'successes': array of emails blacklisted,
   *      'failures': array of emails not blacklisted,
   *   ]
   */
  public function post(array $data): ResourceResponse {
    try {
      $response = $this->splioConnector->addEmailsToBlacklist($data);
      $statusCode = Response::HTTP_INTERNAL_SERVER_ERROR;

      if (!empty($response)) {
        {
          if (!empty($response[0]['status_code'])) {
            $statusCode = $response[0]['status_code'];
          }

          if (!empty($response[0]['message'])) {
            $data = ['message' => $response[0]['message']];
          }
        }
      }
    }
    catch (\Throwable $exception) {
      $statusCode = Response::HTTP_INTERNAL_SERVER_ERROR;
      $data = ['message' => $exception->getMessage()];
    }

    return $this->returnResponse(
      $statusCode,
      $data
    );
  }

}
