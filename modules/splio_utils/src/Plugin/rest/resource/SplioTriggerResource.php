<?php

namespace Drupal\splio_utils\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\splio\Exception\TokenNotReceivedException;
use Drupal\splio\Services\SplioConnectorInterface;
use Drupal\splio\Services\SplioTriggerConnector;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class SplioTriggerResource.
 *
 * @RestResource(
 *   id = "splio_trigger_manager",
 *   label = @Translation("Splio trigger manager"),
 *   uri_paths = {
 *     "canonical" = "/splio/trigger",
 *     "create" = "/splio/trigger",
 *   }
 * )
 */
class SplioTriggerResource extends AbstractResource {

  /**
   * Connector to Splio trigger service.
   *
   * @var \Drupal\splio\Services\SplioTriggerConnector
   */
  private SplioTriggerConnector $splioTriggerConnector;

  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializerFormats,
    LoggerInterface $logger,
    SplioTriggerConnector $splioTriggerConnector
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $serializerFormats,
      $logger
    );

    $this->splioTriggerConnector = $splioTriggerConnector;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('splio_trigger'),
      $container->get('splio.splio_trigger_connector')
    );
  }

  /**
   * Sends a trigger to Splio, so the platform sends a message to the
   * recipients.
   *
   * @param array $data
   *   The trigger data to be fired in Splio.
   *   [
   *      message => 'xxxxx',
   *      opcode => [
   *        type => 'xxxx',
   *        success_message => 'xxx',
   *        failure_message => 'xxx',
   *      ],
   *   ].
   *
   * @return \Drupal\rest\ResourceResponse
   *   [
   *     code => HTTP response status code,
   *     error => has value if an error occurred
   *   ].
   */
  public function post(array $data): ResourceResponse {
    try {
      $response = $this->splioTriggerConnector->triggerMessage($data);
      $statusCode = $response['code'];
      $message = $response;
    }
    catch (
      TokenNotReceivedException
      | GuzzleException
    $exception
    ) {
      $statusCode = Response::HTTP_INTERNAL_SERVER_ERROR;
      $message = $exception->getMessage();
    }

    return $this->returnResponse(
      $statusCode,
      [json_encode($message)]
    );
  }

}
