<?php

namespace Drupal\splio_utils\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;

abstract class AbstractResource extends ResourceBase {

  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $serializer_formats,
      $logger
    );
  }

  public function returnResponse(
    int $statusCode,
    array $data
  ): ResourceResponse {
    $response = new ResourceResponse();

    $response->setStatusCode($statusCode);
    $response->setContent(
      json_encode($data)
    );
    $response->addCacheableDependency(
      [
        '#cache' => [
          'max-age' => 0,
        ],
      ]
    );

    return $response;
  }

}