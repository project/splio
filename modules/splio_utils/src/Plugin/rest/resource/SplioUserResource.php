<?php

namespace Drupal\splio_utils\Plugin\rest\resource;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\rest\ResourceResponse;
use Drupal\splio\Services\SplioConnectorInterface;
use Drupal\splio\Services\SplioEntityHandlerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class SplioUserResource.
 *
 * @RestResource(
 *   id = "splio_user_manager",
 *   label = @Translation("Splio user manager"),
 *   uri_paths = {
 *     "canonical" = "/splio/user/{uid}",
 *     "create" = "/splio/user",
 *   }
 * )
 */
class SplioUserResource extends AbstractResource {

  /**
   * Connector to Splio services.
   *
   * @var \Drupal\splio\services\SplioConnectorInterface
   */
  private SplioConnectorInterface $splioConnector;

  /**
   * Entity manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  private EntityTypeManagerInterface $entityManager;

  private SplioEntityHandlerInterface $splioEntityHandler;

  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    EntityTypeManagerInterface $entityManager,
    SplioConnectorInterface $splioConnector,
    SplioEntityHandlerInterface $splioEntityHandler
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $serializer_formats,
      $logger
    );

    $this->entityManager = $entityManager;
    $this->splioConnector = $splioConnector;
    $this->splioEntityHandler = $splioEntityHandler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('splio_user'),
      $container->get('entity_type.manager'),
      $container->get('splio.splio_connector'),
      $container->get('splio.entity_handler')
    );
  }

  /**
   * Returns a Splio user.
   *
   * @param $uid
   *   The user ID to be checked.
   *
   * @return \Drupal\rest\ResourceResponse
   *   - status_code
   *   - message:
   *    {
   *    }
   */
  public function get($uid): ResourceResponse {
    $data = [];
    $message = '';

    try {
      $entities = $this
        ->splioEntityHandler
        ->getLocalSplioEntitiesConfig();
      $entity = $this
        ->entityManager
        ->getStorage($entities['contacts']['local_entity'])
        ->load($uid);

      $result = $this->splioConnector->readEntities([$entity]);
      $data = $result[0] ?? [];
      $statusCode = Response::HTTP_OK;

      if (0 === \count($data)) {
        $data = new \stdClass();
        $statusCode = Response::HTTP_NOT_FOUND;
        $message = sprintf(
          'Rest resource SplioUser: User %d not found in Splio',
          $uid
        );
      }
    }
    catch (\Throwable $exception) {
      $statusCode = $exception->getCode();
      $message = $exception->getMessage();
    }

    return $this->returnResponse(
      $statusCode,
      [
        'user' => $data,
        'message' => $message,
      ]
    );
  }

}
