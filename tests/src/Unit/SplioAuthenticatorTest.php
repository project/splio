<?php

namespace Drupal\Tests\splio\Unit;

use Drupal\Core\State\State;
use Drupal\Core\State\StateInterface;
use Drupal\splio\Exception\CannotAuthenticateException;
use Drupal\splio\Services\ExceptionMessageParserInterface;
use Drupal\splio\Services\SplioApiKeyGetterInterface;
use Drupal\splio\Services\SplioAuthenticator;
use Drupal\splio\Services\SplioAuthenticatorInterface;
use Drupal\splio\Services\SplioUriGeneratorInterface;
use Drupal\Tests\splio\Unit\Common\SplioUnitTestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;

class SplioAuthenticatorTest extends SplioUnitTestCase {

  private StateInterface $state;

  private SplioAuthenticatorInterface $splioAuthenticator;

  protected function setUp(): void {
    parent::setUp();

    $this->setClientMock();

    $logger = $this
      ->getMockBuilder(LoggerInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $splioUriGenerator = $this
      ->getMockBuilder(SplioUriGeneratorInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $this->state = $this
      ->getMockBuilder(State::class)
      ->disableOriginalConstructor()
      ->getMock();
    $apiKeyGetter = $this
      ->getMockBuilder(SplioApiKeyGetterInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $exceptionMessageParser = $this
      ->getMockBuilder(ExceptionMessageParserInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $this->splioAuthenticator = new SplioAuthenticator(
      $this->client,
      $logger,
      $splioUriGenerator,
      $this->state,
      $apiKeyGetter,
      $exceptionMessageParser
    );
  }

  public function testDecodeJwtOk() {
    $expirationDate = strtotime(date('Y-m-d') . '+1 day');
    $token = $this->generateJwt($expirationDate);
    $jwt = $this->splioAuthenticator->decodeJwt($token);

    self::arrayHasKey('name');
    self::arrayHasKey('exp');
    self::assertSame('bodeboca', $jwt['name']);
    self::assertSame($expirationDate, $jwt['exp']);
  }

  /**
   * @depends testDecodeJwtOk
   * @dataProvider setTokenDataOkProvider
   */
  public function testSetTokenDataOk(int $expirationDate) {
    $this->setTokenDataManually($expirationDate);

    // Stored token, received from Splio.
    $token = $this->splioAuthenticator->getToken();
    $tokenData = $this->splioAuthenticator->decodeJwt($token);

    self::assertArrayHasKey('name', $tokenData);
    self::assertArrayHasKey('exp', $tokenData);
    self::assertSame($expirationDate, $tokenData['exp']);
  }

  /**
   * @depends testSetTokenDataOk
   * @dataProvider setTokenDataOkProvider
   */
  public function testTokenIsNotExpired(int $expirationDate) {
    $this->setTokenDataManually($expirationDate);

    self::assertFalse($this->splioAuthenticator->tokenIsExpired());
  }

  public function setTokenDataOkProvider(): array {
    return [
      [
        'tomorrow' => strtotime(date('Y-m-d') . '+1 day'),
      ],
    ];
  }

  /**
   * @depends testSetTokenDataOk
   * @dataProvider tokenIsExpiredProvider
   */
  public function testTokenIsExpired(int $expirationDate) {
    $this->setTokenDataManually($expirationDate);

    self::assertTrue($this->splioAuthenticator->tokenIsExpired());
  }

  public function tokenIsExpiredProvider(): array {
    return [
      [
        'yesterday' => strtotime(date('Y-m-d') . '-1 day'),
      ],
    ];
  }

  /**
   * @dataProvider authenticateFromClassPropertyOkProvider
   */
  public function testAuthenticateFromClassPropertyOk(int $expirationTimestamp) {
    // Generates token manually.
    $manualToken = $this->generateJwt($expirationTimestamp);

    // This mock is needed in next instruction.
    $this->state
      ->method('get')
      ->willReturn($manualToken);

    // This will set class property value.
    $this->splioAuthenticator->setToken($manualToken);

    // This will return the token in class property.
    $token = $this->splioAuthenticator->authenticate();

    self::assertSame($manualToken, $token);
  }

  public function authenticateFromClassPropertyOkProvider() {
    return [
      [
        'expiration_date' => strtotime(date('Y-m-d') . '+1 day'),
      ],
    ];
  }

  /**
   * @dataProvider authenticateFromStateOkProvider
   */
  public function testAuthenticateFromStateOk(int $expirationTimestamp) {
    $manualToken = $this->generateJwt($expirationTimestamp);

    $this->state
      ->method('get')
      ->willReturn($manualToken);

    $token = $this->splioAuthenticator->authenticate();

    self::assertSame($manualToken, $token);
  }

  public function authenticateFromStateOkProvider() {
    return [
      [
        'expiration_date' => strtotime(date('Y-m-d') . '+1 day'),
      ],
    ];
  }

  /**
   * @dataProvider authenticateFromSplioOkProvider
   */
  public function testAuthenticateFromSplioOk(array $data) {
    $expiredToken = $this->generateJwt($data['expiration_date_yesterday']);
    $validToken = $this->generateJwt($data['expiration_date_tomorrow']);

    $this->state
      ->method('get')
      ->willReturn($expiredToken);

    // Pool the mock responses "from" Splio.
    if (!empty($data['splio_responses'])) {
      $this->poolClientMockHandlerResponses($data['splio_responses']);
    }

    $token = $this->splioAuthenticator->authenticate();

    self::assertSame($validToken, $token);
  }

  /**
   * Provider for testAuthenticateFromSplioOk.
   *
   * @return array
   *   Provided data.
   */
  public function authenticateFromSplioOkProvider(): array {
    $expirationDateYesterday = strtotime(date('Y-m-d') . '-1 day');
    $expirationDateTomorrow = strtotime(date('Y-m-d') . '+1 day');
    $tokenExpirationDateTomorrow = $this->generateJwt($expirationDateTomorrow);

    return [
      [
        'token_from_splio' => [
          'expiration_date_yesterday' => $expirationDateYesterday,
          'expiration_date_tomorrow' => $expirationDateTomorrow,
          'splio_responses' => [
            [
              'code' => Response::HTTP_OK,
              'body' => ['token' => $tokenExpirationDateTomorrow],
            ],
          ],
        ],
      ],
    ];
  }

  /**
   * Test for failing authentication to Splio.
   *
   * @dataProvider authenticateFailProvider
   *
   * @param array $data
   */
  public function testAuthenticateFail(array $data) {
    self::expectException(CannotAuthenticateException::class);

    $manualToken = $this->generateJwt($data['expiration_date']);

    $this->state
      ->method('get')
      ->willReturn($manualToken);

    // Pool the mock responses "from" Splio.
    if (!empty($data['splio_responses'])) {
      $this->poolClientMockHandlerResponses($data['splio_responses']);
    }

    $this->splioAuthenticator->authenticate();
  }

  /**
   * Provider for testAuthenticateFail.
   *
   * @return array
   *   Data provided.
   */
  public function authenticateFailProvider(): array {
    $expirationDateYesterday = strtotime(date('Y-m-d') . '-1 day');

    return [
      [
        'token_from_splio_three_tries' => [
          'expiration_date' => $expirationDateYesterday,
          'splio_responses' => [
            [
              'code' => Response::HTTP_INTERNAL_SERVER_ERROR,
              'body' => ['token' => NULL],
            ],
          ],
        ],
      ],
    ];
  }

  /**
   * Generates a token, and stores it in Drupal state, and class property.
   *
   * @param int|null $expirationTimestamp
   *   Token expiration timestamp.
   *
   * @return string|null
   *   Token.
   */
  private function setTokenDataManually(?int $expirationTimestamp): ?string {
    $token = $this->generateJwt($expirationTimestamp);

    // This mock is needed in next instruction.
    $this->state
      ->method('get')
      ->willReturn($token);

    $this->splioAuthenticator->setToken($token);

    return $token;
  }

  private function generateJwt(?int $expirationTimestamp): ?string {
    if (empty($expirationTimestamp)) {
      return NULL;
    }

    // Create token header as a JSON string.
    $header = json_encode(['typ' => 'JWT', 'alg' => 'HS256']);
    // Create token payload as a JSON string.
    $payload = '{"iss":"7bc944de-497b-4e58-8544-388e44381f3d","sub":"user","aud":"api.client","is_api":true,"exp":' . $expirationTimestamp . ',"iat":1664784541,"jti":"1713.633a989d2a847","name":"bodeboca","locale":null,"user":{"id":1713,"universe":"bodeboca_sandbox"}}';
    // Encode Header to Base64Url String.
    $base64UrlHeader = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($header));
    // Encode Payload to Base64Url String.
    $base64UrlPayload = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($payload));
    // Create Signature Hash.
    $signature = hash_hmac('sha256', $base64UrlHeader . "." . $base64UrlPayload, 'abC123!', TRUE);
    // Encode Signature to Base64Url String.
    $base64UrlSignature = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($signature));

    // Create JWT.
    return $base64UrlHeader . '.' . $base64UrlPayload . '.' . $base64UrlSignature;
  }

}
