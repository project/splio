<?php

namespace Drupal\Tests\splio\Unit;

use Drupal\bb_product\Entity\BodebocaProduct;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldItemList;
use Drupal\node\Entity\Node;
use Drupal\splio\Entity\SplioEntity;
use Drupal\splio\Entity\SplioField;
use Drupal\splio\Services\EntityValueHandlerInterface;
use Drupal\splio\Services\SplioEntityHandler;
use Drupal\splio\Services\SplioEntityHandlerInterface;
use Drupal\Tests\splio\Unit\Common\SplioUnitTestCase;
use Drupal\Tests\splio\Unit\Provider\SplioConfigEntitiesProvider;
use Psr\Log\LoggerInterface;

class SplioEntityHandlerTest extends SplioUnitTestCase {

  const EMAIL_VALID = 'some@email.com';

  const ID_VALID = '123456';

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  private EntityTypeManagerInterface $entityTypeManager;

  /**
   * Entity value handler service.
   *
   * @var \Drupal\splio\Services\EntityValueHandlerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  private EntityValueHandlerInterface $entityValueHandler;

  /**
   * Config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactory|\PHPUnit\Framework\MockObject\MockObject
   */
  private ConfigFactory $config;

  /**
   * Splio entity handler service.
   *
   * @var \Drupal\splio\Services\SplioEntityHandlerInterface|\Drupal\splio\Services\SplioEntityHandler
   */
  private SplioEntityHandlerInterface $splioEntityHandler;

  private array $configEntitiesContents;

  /**
   * Initial set up.
   */
  protected function setUp(): void {
    parent::setUp();

    $provider = new SplioConfigEntitiesProvider();
    $this->configEntitiesContents = $provider->execute();

    $immutableConfig = $this->createMock(ImmutableConfig::class);
    $immutableConfig
      ->method('get')
      ->willReturn($this->configEntitiesContents);

    $this->entityTypeManager = $this->createMock(EntityTypeManagerInterface::class);
    $this->entityValueHandler = $this->createMock(EntityValueHandlerInterface::class);
    $logger = $this->createMock(LoggerInterface::class);

    $this->config = $this->createMock(ConfigFactory::class);
    $this->config
      ->method('get')
      ->willReturn($immutableConfig);

    $this->splioEntityHandler = new SplioEntityHandler(
      $this->entityTypeManager,
      $this->entityValueHandler,
      $logger,
      $this->config,
    );
  }

  /**
   * Test for getSplioEntityTypeFromLocalEntity ok.
   *
   * @dataProvider getSplioEntityTypeFromLocalEntityOkProvider
   */
  public function testGetSplioEntityTypeFromEntityOk(array $data) {
    $entityType = $this->createMock(EntityTypeInterface::class);
    $entityType
      ->method('id')
      ->willReturn($data['entityType']);

    $entity = $this->createMock($data['class']);
    $entity
      ->method('getEntityType')
      ->willReturn($entityType);

    // Create container.
    $container = new ContainerBuilder();

    // Set container.
    \Drupal::setContainer($container);

    // Set object to service.
    $container->set('config.factory', $this->config);

    $storage = $this->createMock(EntityStorageInterface::class);
    $storage
      ->method('loadByProperties')
      ->willReturn([$this->configEntitiesContents]);

    $this->entityTypeManager
      ->method('getStorage')
      ->willReturn($storage);

    $entityType = $this->splioEntityHandler
      ->getSplioEntityTypeFromLocalEntity($entity);

    self::assertSame($data['expected'], $entityType);
  }

  /**
   * Provider for testGetSplioEntityTypeFromLocalEntityOk.
   *
   * @return array
   *   Data provided.
   */
  public function getSplioEntityTypeFromLocalEntityOkProvider(): array {
    return [
      [
        'get_product_entity_type' => [
          'expected' => 'products',
          'class' => Node::class,
          'entityType' => 'bb_product',
        ],
      ],
    ];
  }

  /**
   * Test for getProperEntityType.
   *
   * @dataProvider getProperEntityTypeProvider
   */
  public function testGetProperEntityType(array $data) {
    $result = $this->splioEntityHandler->getProperEntityType($data['entityType']);

    self::assertSame($data['expected'], $result);
  }

  /**
   * Provider for testGetProperEntityType.
   *
   * @return array
   *   Data provided.
   */
  public function getProperEntityTypeProvider(): array {
    return [
      [
        'type_products' => [
          'expected' => SplioEntity::TYPE_PRODUCTS,
          'entityType' => SplioEntity::TYPE_PRODUCTS,
        ],
      ],
      [
        'type_order_lines' => [
          'expected' => SplioEntity::TYPE_RECEIPTS,
          'entityType' => SplioEntity::TYPE_ORDER_LINES,
        ],
      ],
    ];
  }

  /**
   * Provider for getProperEntityTypeProvider.
   *
   * @dataProvider getProperEntityOkProvider
   */
  public function testGetProperEntityOk(array $data) {
    $splioField = $this->createMock(SplioField::class);
    $splioField
      ->method('getDrupalField')
      ->willReturn($data['drupal_field']);

    $storage = $this->createMock(EntityStorageInterface::class);
    $storage
      ->method('loadByProperties')
      ->willReturnOnConsecutiveCalls(
        ['order_lines_id_order' => $splioField],
        ['receipts_extid' => $splioField],
        [$data['order_id'] => $this->createMock(Node::class)]
      );

    $this->entityTypeManager
      ->method('getStorage')
      ->willReturn($storage);

    $this->entityValueHandler
      ->method('getFieldValue')
      ->willReturn($data['order_id']);

    $entity = $this->splioEntityHandler->getProperEntity(
      $data['entity_type'],
      $this->createMock($data['class'])
    );

    self::assertInstanceOf($data['expected'], $entity);
  }

  /**
   * Provider for testGetProperEntityOk.
   *
   * @return array
   *   Data provided.
   */
  public function getProperEntityOkProvider(): array {
    return [
      [
        'order' => [
          'expected' => Node::class,
          'class' => Node::class,
          'drupal_field' => 'drupal_field',
          'order_id' => self::ID_VALID,
          'entity_type' => SplioEntity::TYPE_RECEIPTS,
        ],
      ],
      [
        'order_lines' => [
          'expected' => Node::class,
          'class' => Node::class,
          'drupal_field' => 'drupal_field',
          'order_id' => self::ID_VALID,
          'entity_type' => SplioEntity::TYPE_ORDER_LINES,
        ],
      ],
    ];
  }

  /**
   * Test for testGetProperEntity fail.
   *
   * @dataProvider getProperEntityFailProvider
   */
  public function testGetProperEntityFail(array $data) {
    $splioField = $this->createMock(SplioField::class);
    $splioField
      ->method('getDrupalField')
      ->willReturn($data['drupal_field']);

    $storage = $this->createMock(EntityStorageInterface::class);
    $storage
      ->method('loadByProperties')
      ->willReturnOnConsecutiveCalls(
        ['order_lines_id_order' => $splioField]
      );

    $this->entityTypeManager
      ->method('getStorage')
      ->willReturn($storage);

    $this->entityValueHandler
      ->method('getFieldValue')
      ->willReturn($data['order_id']);

    $entity = $this->splioEntityHandler->getProperEntity(
      $data['entity_type'],
      $this->createMock($data['class'])
    );

    self::assertSame($data['expected'], $entity);
  }

  /**
   * Provider for testGetProperEntityOk.
   *
   * @return array
   *   Data provided.
   */
  public function getProperEntityFailProvider(): array {
    return [
      [
        'order' => [
          'expected' => NULL,
          'class' => Node::class,
          'drupal_field' => NULL,
          'order_id' => self::ID_VALID,
          'entity_type' => SplioEntity::TYPE_ORDER_LINES,
        ],
      ],
    ];
  }

  /**
   * Test for getEntityId.
   *
   * @dataProvider getEntityIdProvider
   */
  public function testGetEntityId(array $data) {
    self::markTestSkipped();
    $entity = $this->createMock($data['class']);

    $fieldItemList = $this->createMock(FieldItemList::class);
    $fieldItemList
      ->method('__get')
      ->with('value')
      ->willReturn($data[$data['drupal_field']]);

    $entity
      ->method('get')
      ->with($data['drupal_field'])
      ->willReturn($fieldItemList);

    $splioField = $this->createMock(SplioField::class);
    $splioField
      ->method('isKeyField')
      ->willReturn($data['is_key_field']);
    $splioField
      ->method('getDrupalField')
      ->willReturn($data['drupal_field']);

    $storage = $this->createMock(EntityStorageInterface::class);
    $storage
      ->method('loadByProperties')
      ->willReturnOnConsecutiveCalls(
        [$data['field_name'] => $splioField]
      );

    $this->entityTypeManager
      ->method('getStorage')
      ->willReturn($storage);

    $id = $this->splioEntityHandler->getEntityKeyFieldValue($data['entity_type'], $entity);

    self::assertSame($data['expected'], $id);
  }

  /**
   * Provider for testGetEntityId.
   *
   * @return array
   *   Data provided.
   */
  public function getEntityIdProvider(): array {
    return [
      [
        'user_id' => [
          'expected' => self::EMAIL_VALID,
          'class' => Node::class,
          'entity_type' => SplioEntity::TYPE_CONTACTS,
          'email' => self::EMAIL_VALID,
          'is_key_field' => TRUE,
          'field_name' => 'contacts_email',
          'drupal_field' => 'email',
        ],
      ],
      [
        'product_id' => [
          'expected' => self::ID_VALID,
          'class' => Node::class,
          'entity_type' => SplioEntity::TYPE_PRODUCTS,
          'extid' => self::ID_VALID,
          'is_key_field' => TRUE,
          'field_name' => 'products_extid',
          'drupal_field' => 'extid',
        ],
      ],
    ];
  }

}
