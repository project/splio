<?php

namespace Drupal\Tests\splio\Unit;

use Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher;
use Drupal\Core\Language\Language;
use Drupal\Core\Queue\QueueFactory;
use Drupal\node\Entity\Node;
use Drupal\splio\Entity\SplioEntity;
use Drupal\splio\Services\EntityHelperInterface;
use Drupal\splio\Services\SplioEntityHandlerInterface;
use Drupal\splio\Services\SplioQueueHandler;
use Drupal\splio\Services\SplioQueueHandlerInterface;
use Drupal\Tests\splio\Unit\Common\SplioUnitTestCase;
use Drupal\Tests\splio\Unit\Provider\QueueContactItemProvider;
use Psr\Log\LoggerInterface;

class SplioQueueHandlerTest extends SplioUnitTestCase {

  /**
   * Splio entity handler.
   *
   * @var \Drupal\splio\Services\SplioEntityHandlerInterface
   */
  private SplioEntityHandlerInterface $splioEntityHandler;

  /**
   * Queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  private QueueFactory $queueFactory;

  /**
   * Event dispatcher.
   *
   * @var \Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher
   */
  private ContainerAwareEventDispatcher $eventDispatcher;

  /**
   * Logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  private LoggerInterface $logger;

  /**
   * Splio queue handler.
   *
   * @var \Drupal\splio\Services\SplioQueueHandlerInterface
   */
  private SplioQueueHandlerInterface $splioQueueHandler;

  private EntityHelperInterface $entityHelper;

  protected function setUp(): void {
    parent::setUp();

    $this->splioEntityHandler = $this->createMock(SplioEntityHandlerInterface::class);
    $this->queueFactory = $this->createMock(QueueFactory::class);
    $this->eventDispatcher = $this->createMock(ContainerAwareEventDispatcher::class);
    $this->logger = $this->createMock(LoggerInterface::class);
    $this->entityHelper = $this->createMock(EntityHelperInterface::class);

    $this->splioQueueHandler = new SplioQueueHandler(
      $this->splioEntityHandler,
      $this->queueFactory,
      $this->eventDispatcher,
      $this->logger,
      $this->entityHelper,
    );
  }

  /**
   * @dataProvider generateItemProvider
   */
  public function testGenerateItem(array $data) {
    $language = $this->createMock(Language::class);
    $language
      ->method('getId')
      ->willReturn($data['language_id']);

    $entity = $this->createMock($data['entity']);
    $entity
      ->method('id')
      ->willReturn($data['id']);
    $entity
      ->method('language')
      ->willReturn($language);

    $this->splioEntityHandler
      ->method('getProperEntity')
      ->willReturn($entity);

    $provider = new QueueContactItemProvider();
    $contents = $provider->execute();
    $expected = $contents[$data['entity_type'] . '_' . $data['action']];

    if (in_array($data['action'], ['update', 'delete'])) {
      $this->entityHelper
        ->method('getOriginal')
        ->willReturn($entity);

      $expected['original'] = $entity;
    }
    else {
      $this->entityHelper
        ->method('getOriginal')
        ->willReturn(NULL);
    }

    $result = $this->invokeMethod(
      $this->splioQueueHandler,
      'generateQueueItem',
      [
        $data['entity_type'],
        $entity,
        $data['action'],
      ]
    );

    self::assertSame($expected, $result);
  }

  /**
   * Test for testGenerateItem.
   *
   * @return array
   *   Data provided.
   */
  public function generateItemProvider():array {
    return [
      [
        'create_contact' => [
          'entity_type' => SplioEntity::TYPE_CONTACTS,
          'action' => 'create',
          'entity' => Node::class,
          'language_id' => 'und',
          'id' => '3297079',
        ],
      ],
      [
        'update_contact' => [
          'entity_type' => SplioEntity::TYPE_CONTACTS,
          'action' => 'update',
          'entity' => Node::class,
          'language_id' => 'und',
          'id' => '3297079',
        ],
      ],
      [
        'delete_contact' => [
          'entity_type' => SplioEntity::TYPE_CONTACTS,
          'action' => 'delete',
          'entity' => Node::class,
          'language_id' => 'und',
          'id' => '3297079',
        ],
      ],
      [
        'create_order_line' => [
          'entity_type' => SplioEntity::TYPE_ORDER_LINES,
          'action' => 'create',
          'entity' => Node::class,
          'language_id' => 'und',
          'id' => '3297079',
        ],
      ],
    ];
  }

}
