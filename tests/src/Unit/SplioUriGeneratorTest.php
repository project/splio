<?php

namespace Drupal\Tests\splio\Unit;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\splio\Services\SplioUriGenerator;
use Drupal\Tests\UnitTestCase;

class SplioUriGeneratorTest extends UnitTestCase {

  const URL_SCHEME = [
    'https' => 'https://',
  ];

  const HOSTS = [
    'splio_api' => 'api.splio.com',
    'other' => 'www.otherhost.com',
  ];

  const PATH = [
    'ping' => SplioUriGenerator::SPLIO_PATH['ping'],
    'blacklist' => SplioUriGenerator::SPLIO_PATH['blacklist_get'],
    'other' => '/highway/to/hell',
  ];

  const QUERY_PARAMS = [
    'blacklist' => ['term' => 'some@email.com'],
  ];

  /**
   * SplioUriGenerator.
   *
   * @var \Drupal\splio\Services\SplioUriGenerator
   */
  private SplioUriGenerator $splioUriGenerator;

  protected function setUp(): void {
    parent::setUp();

    /** @var \Drupal\Core\Config\ConfigFactoryInterface $configFactory */
    $configFactory = $this
      ->getMockBuilder(ConfigFactoryInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $config = $this->getMockBuilder(ImmutableConfig::class)
      ->disableOriginalConstructor()
      ->getMock();
    $configFactory->method('get')->willReturn($config);
    $config->method('get')->willReturn('api.splio.com');

    $this->splioUriGenerator = new SplioUriGenerator($configFactory);
  }

  /**
   * Test for generateBaseUri.
   *
   * @dataProvider generateBaseUriSuccessfullyProvider
   */
  public function testGenerateBaseUriSuccessfully(array $data) {
    $baseUri = $this->splioUriGenerator->generateBaseUri($data['options']);

    self::assertSame($data['expected'], $baseUri);
  }

  /**
   * Provider for testGenerateBaseUri. Success.
   *
   * @return array
   *   Data provided.
   */
  public function generateBaseUriSuccessfullyProvider(): array {
    return [
      [
        'splio_api_host' => [
          'expected' => self::URL_SCHEME['https'] . self::HOSTS['splio_api'],
          'options' => NULL,
        ],
      ],
      [
        'other_host' => [
          'expected' => self::URL_SCHEME['https'] . self::HOSTS['other'],
          'options' => [
            'server' => self::HOSTS['other'],
          ],
        ],
      ],
    ];
  }

  /**
   * Test for testGenerateBaseUri. Unsuccess.
   *
   * @dataProvider generateBaseUriUnsuccessfullyProvider
   */
  public function testGenerateBaseUriUnsuccessfully(array $data) {
    $baseUri = $this->splioUriGenerator->generateBaseUri($data['options']);

    self::assertNotSame($data['expected'], $baseUri);
  }

  /**
   * Provider for testGenerateBaseUriUnsuccessfully.
   *
   * @return array
   *   Data provided.
   */
  public function generateBaseUriUnsuccessfullyProvider(): array {
    return [
      [
        'other_host' => [
          'expected' => self::URL_SCHEME['https'] . self::HOSTS['other'],
          'options' => NULL,
        ],
      ],
    ];
  }

  /**
   * Test for getUri successfully.
   *
   * @dataProvider getUriSuccessfullyProvider
   */
  public function testGetUriSuccessfully(array $data) {
    $uri = $this->splioUriGenerator->getUri($data['path'], $data['options']);

    self::assertSame($data['expected'], $uri);
  }

  /**
   * Provider for testGetUriSuccessfully.
   *
   * @return array
   *   Data provided.
   */
  public function getUriSuccessfullyProvider(): array {
    return [
      [
        'splio_ping_uri' => [
          'expected' => self::URL_SCHEME['https'] . self::HOSTS['splio_api'] . self::PATH['ping'],
          'path' => self::PATH['ping'],
          'options' => NULL,
        ],
      ],
      [
        'external' => [
          'expected' => self::URL_SCHEME['https'] . self::HOSTS['other'] . self::PATH['other'],
          'path' => self::PATH['other'],
          'options' => [
            'server' => self::HOSTS['other'],
          ],
        ],
      ],
      [
        'splio_blacklist' => [
          'expected' => self::URL_SCHEME['https'] . self::HOSTS['splio_api'] . self::PATH['blacklist'] . '?' . http_build_query(self::QUERY_PARAMS['blacklist']),
          'path' => self::PATH['blacklist'],
          'options' => [
            'queryParams' => self::QUERY_PARAMS['blacklist'],
          ],
        ],
      ],
    ];
  }

  /**
   * Test for getUri unsuccessfully.
   *
   * @dataProvider getUriUnsuccessfullyProvider
   */
  public function testGetUriUnsuccessfully(array $data) {
    $uri = $this->splioUriGenerator->getUri($data['path'], $data['options']);

    self::assertNotSame($data['expected'], $uri);
  }

  /**
   * Provider for testGetUriUnsuccessfully.
   *
   * @return array
   *   Data provided.
   */
  public function getUriUnsuccessfullyProvider(): array {
    return [
      [
        'splio_blacklist' => [
          'expected' => self::URL_SCHEME['https'] . self::HOSTS['splio_api'] . self::PATH['blacklist'] . '?' . http_build_query(self::QUERY_PARAMS['blacklist']),
          'path' => self::PATH['blacklist'],
          'options' => NULL,
        ],
      ],
    ];
  }

}
