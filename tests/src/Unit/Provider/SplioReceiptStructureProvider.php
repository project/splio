<?php

namespace Drupal\Tests\splio\Unit\Provider;

class SplioReceiptStructureProvider implements DataProviderInterface  {

  public function execute(): array  {
    return [
      [
        [
          'expected' => [
            'action' => 'update',
            'date_added' => '1664292289',
            'date_order' => '1664292363',
            'custom_fields' =>  [
                  'deliveredDate' =>  [
                    'name' => 'deliveredDate',
                'value' => '0'
              ],
              'deliveryAgency' =>  [
                    'name' => 'deliveryAgency',
                'value' => 'CELERITAS'
              ],
              'orderStatus' =>  [
                    'name' => 'orderStatus',
                'value' => 'payment_received'
              ],
              'paymentMethod' =>  [
                    'name' => 'paymentMethod',
                'value' => 'tpv'
              ],
              'premiumApplied' =>  [
                    'name' => 'premiumApplied',
                'value' => '0'
              ]
            ],
            'discount_amount' => '0.00',
            'external_id' => '4186987',
            'id_store' => 'bodeboca.com',
            'shipping_amount' => '5.90',
            'total_price' => '123.40',
            'contact_id' => 'some.email@bodeboca.com',
            'products' => [
              [
                'id_order' => '4186987',
                'quantity' => 1,
                'unit_price' => '26.50',
                'product_id' => '983748'
              ],
            ],
          ],
          'sent_data' => [
            'action' => 'update',
            'date_added' => '1664292289',
            'date_order' => '1664292363',
            'custom_fields' =>  [
                  'deliveredDate' =>  [
                    'name' => 'deliveredDate',
                'value' => '0'
              ],
              'deliveryAgency' =>  [
                    'name' => 'deliveryAgency',
                'value' => 'CELERITAS'
              ],
              'orderStatus' =>  [
                    'name' => 'orderStatus',
                'value' => 'payment_received'
              ],
              'paymentMethod' =>  [
                    'name' => 'paymentMethod',
                'value' => 'tpv'
              ],
              'premiumApplied' =>  [
                    'name' => 'premiumApplied',
                'value' => '0'
              ]
            ],
            'discount_amount' => '0.00',
            'external_id' => '4186987',
            'id_store' => 'bodeboca.com',
            'shipping_amount' => '5.90',
            'total_price' => '123.40',
            'contact_id' => 'some.email@bodeboca.com',
            'products' => [
              [
                'id_order' => '4186987',
                'quantity' => 1,
                'unit_price' => '26.50',
                'id_product' => '983748'
              ],
            ],
          ],
        ],
      ],
    ];
  }
}