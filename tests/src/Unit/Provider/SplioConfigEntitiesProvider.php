<?php

namespace Drupal\Tests\splio\Unit\Provider;

class SplioConfigEntitiesProvider implements DataProviderInterface {
  
  public function execute(): array {

    return [
      'contacts' => [
        'label' => 'Contacts',
        'local_entity' => 'bb_user',
        'local_entity_bundle' => '',
        'splio_entity_key_field' => 'extid'
      ],
      'contacts_lists' => [
        'label' => 'Contacts lists',
        'local_entity' => 'bb_user',
        'local_entity_bundle' => '',
        'splio_entity_key_field' => 'extid'
      ],
      'products' => [
        'label' => 'Products',
        'local_entity' => 'bb_product',
        'local_entity_bundle' => '',
        'splio_entity_key_field' => 'extid'
      ],
      'receipts' => [
        'label' => 'Receipts',
        'local_entity' => 'bb_order',
        'local_entity_bundle' => '',
        'splio_entity_key_field' => 'extid'
      ],
      'order_lines' => [
        'label' => 'Order lines',
        'local_entity' => 'bb_order_line_item',
        'local_entity_bundle' => '',
        'splio_entity_key_field' => 'extid'
      ],
      'stores' => [
        'label' => 'Stores',
        'local_entity' => '',
        'local_entity_bundle' => '',
        'splio_entity_key_field' => ''
      ]
    ];
  }

}