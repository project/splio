<?php

namespace Drupal\Tests\splio\Unit\Provider;

interface DataProviderInterface {

  public function execute(): array;

}