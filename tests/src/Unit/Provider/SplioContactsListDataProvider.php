<?php

namespace Drupal\Tests\splio\Unit\Provider;

use Symfony\Component\HttpFoundation\Response;

class SplioContactsListDataProvider implements DataProviderInterface {

  public function execute(): array {

    return [
      [
        'response_200' => [
          'expected' => [
            0 => 'bodeboca.com',
            1 => 'bodeboca.fr',
            2 => 'bodeboca.co.uk',
          ],
          'client_response' => [
            'code' => Response::HTTP_OK,
            'body' => [
              'count_element'=> 7,
              'current_page'=> 1,
              'per_page'=> 50,
              'sort'=> [],
              'elements'=> [
                [
                  'id'=> 0,
                  'name'=> 'bodeboca.com',
                  'nb_emails'=> 1,
                  'nb_sms'=> 1
                ],
                [
                  'id'=> 1,
                  'name'=> 'bodeboca.fr',
                  'nb_emails'=> 0,
                  'nb_sms'=> 0
                ],
                [
                  'id'=> 2,
                  'name'=> 'bodeboca.co.uk',
                  'nb_emails'=> 0,
                  'nb_sms'=> 0
                ]
              ]
            ],
          ],
        ],
      ]
    ];
  }

}