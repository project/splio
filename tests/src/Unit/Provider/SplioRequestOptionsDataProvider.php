<?php

namespace Drupal\Tests\splio\Unit\Provider;

class SplioRequestOptionsDataProvider implements DataProviderInterface {

  public function execute(): array {

    return [
      [
        'Valid' => [
          'headers' => [
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer AbCdEf',
          ],
          'connect_timeout' => '0',
          'timeout' => '0',
          'body' => '{"needed" =>["field"]}',
        ]
      ]
    ];
  }

}