<?php

namespace Drupal\Tests\splio\Unit\Provider;

class QueueContactItemProvider implements DataProviderInterface {

  public function execute(): array {
    return [
      'contacts_create' => [
        'id' => '3297079',
        'original' => NULL,
        'lang' => 'und',
        'splioEntityType' => 'contacts',
        'action' => 'create',
      ],
      'contacts_update' => [
        'id' => '3297079',
        'original' => NULL,
        'lang' => 'und',
        'splioEntityType' => 'contacts',
        'action' => 'update',
      ],
      'contacts_delete' => [
        'id' => '3297079',
        'original' => NULL,
        'lang' => 'und',
        'splioEntityType' => 'contacts',
        'action' => 'delete',
      ],
      'order_lines_create' => [
        'originalSplioEntityType' => 'order_lines',
        'id' => '3297079',
        'original' => NULL,
        'lang' => 'und',
        'splioEntityType' => 'receipts',
        'action' => 'create',
      ],
    ];
  }

}