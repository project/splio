<?php

namespace Drupal\Tests\splio\Unit\Common;

use Drupal\Tests\UnitTestCase;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response as GuzzleResponse;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;

/**
 * Class with common methods.
 */
class SplioUnitTestCase extends UnitTestCase {

  const RESOURCE_PATH = '../modules/contrib/splio/tests/src/Resource/';

  /**
   * HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected ClientInterface $client;

  protected MockHandler $mockHandler;

  /**
   * Returns the contents of the given file.
   *
   * @param string $filename
   *   File name.
   *
   * @return string
   *   The contents of the given file.
   */
  protected function getResourceContents(string $filename): string {
    $data = file_get_contents(self::RESOURCE_PATH . $filename);

    if (FALSE === $data) {
      throw new FileNotFoundException($filename . ' not found.');
    }

    return $data;
  }

  /**
   * Invoked private method of a certain class.
   *
   * @param mixed $object
   *   SplioEntityStructureGenerator object.
   * @param string $methodName
   *   Private method to invoke.
   * @param array $parameters
   *   Method parameters.
   *
   * @return mixed
   *   Data returned.
   *
   * @throws \ReflectionException
   */
  protected function invokeMethod(
    &$object,
    string $methodName,
    array $parameters = []
  ) {
    $reflection = new \ReflectionClass(get_class($object));
    $method = $reflection->getMethod($methodName);
    $method->setAccessible(TRUE);

    return $method->invokeArgs($object, $parameters);
  }

  protected function setClientMock() {
    $this->mockHandler = new MockHandler();
    $handlerStack = HandlerStack::create($this->mockHandler);
    $this->client = new Client(['handler' => $handlerStack]);
  }

  /**
   * Pools the responses that Splio services will return
   * in consecutive requests.
   *
   * @param array $data
   *   Array of consecutive Splio responses.
   */
  protected function poolClientMockHandlerResponses(array $data): void {
    foreach ($data as $item) {
      $response = new GuzzleResponse(
        $item['code'],
        $item['header'] ?? [],
        json_encode($item['body'])
      );

      $this->mockHandler->append($response);
    }
  }

}
