<?php

namespace Drupal\Tests\splio\Unit;

use Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Queue\QueueFactory;
use Drupal\key\KeyRepository;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;
use Drupal\splio\Entity\SplioEntity;
use Drupal\splio\Services\ExceptionMessageParserInterface;
use Drupal\splio\Services\SplioAuthenticatorInterface;
use Drupal\splio\Services\SplioConnector;
use Drupal\splio\Services\SplioConnectorInterface;
use Drupal\splio\Services\SplioEntityHandlerInterface;
use Drupal\splio\Services\SplioEntityStructureGeneratorInterface;
use Drupal\splio\Services\SplioUriGeneratorInterface;
use Drupal\Tests\splio\Unit\Common\SplioUnitTestCase;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Provides a series of unit tests for the Splio module.
 *
 */
class SplioConnectorTest extends SplioUnitTestCase {

  const TOKEN_VALID = 'AbCdEf';

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  private ConfigFactory $config;

  /**
   * Key manager.
   *
   * @var \Drupal\key\KeyRepository
   */
  private KeyRepository $keyManager;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private EntityTypeManagerInterface $entityTypeManager;

  /**
   * Queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  private QueueFactory $queueFactory;

  /**
   * Logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  private LoggerInterface $logger;

  /**
   * Event dispatcher.
   *
   * @var \Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher
   */
  private ContainerAwareEventDispatcher $eventDispatcher;

  /**
   * HTTP authenticator.
   *
   * @var \Drupal\splio\Services\SplioAuthenticatorInterface
   */
  private SplioAuthenticatorInterface $authenticator;

  /**
   * Splio URI generator.
   *
   * @var \Drupal\splio\Services\SplioUriGeneratorInterface
   */
  private SplioUriGeneratorInterface $uriGenerator;

  /**
   * Splio entity interface.
   *
   * @var \Drupal\splio\Services\SplioEntityHandlerInterface
   */
  private SplioEntityHandlerInterface $splioEntityHandler;

  /**
   * Splio entity structure generator.
   *
   * @var \Drupal\splio\Services\SplioEntityStructureGeneratorInterface
   */
  private SplioEntityStructureGeneratorInterface $structureGenerator;

  /**
   * Exception message parser.
   *
   * @var \Drupal\splio\Services\ExceptionMessageParser
   */
  private ExceptionMessageParserInterface $exceptionMessageParser;

  private SplioConnectorInterface $splioConnector;

  protected function setUp(): void {
    parent::setUp();

    $this->setClientMock();

    $this->config = $this->createMock(ConfigFactory::class);
    $this->keyManager = $this->createMock(KeyRepository::class);
    $this->entityTypeManager = $this->createMock(EntityTypeManagerInterface::class);
    $this->queueFactory = $this->createMock(QueueFactory::class);
    $this->logger = $this->createMock(LoggerInterface::class);
    $this->eventDispatcher = $this->createMock(ContainerAwareEventDispatcher::class);
    $this->authenticator = $this->createMock(SplioAuthenticatorInterface::class);
    $this->uriGenerator = $this->createMock(SplioUriGeneratorInterface::class);
    $this->splioEntityHandler = $this->createMock(SplioEntityHandlerInterface::class);
    $this->structureGenerator = $this->createMock(SplioEntityStructureGeneratorInterface::class);
    $this->exceptionMessageParser = $this->createMock(ExceptionMessageParserInterface::class);

    $this->splioConnector = new SplioConnector(
      $this->keyManager,
      $this->config,
      $this->entityTypeManager,
      $this->queueFactory,
      $this->eventDispatcher,
      $this->logger,
      $this->client,
      $this->authenticator,
      $this->uriGenerator,
      $this->splioEntityHandler,
      $this->structureGenerator,
      $this->exceptionMessageParser
    );
  }

  /**
   * @dataProvider removeNotNeededFieldsProvider
   */
  public function testRemoveNotNeededFields(array $data) {
    $this->prepareStructureGeneratorIsFieldNotNeeded();

    $response = $this->invokeMethod(
      $this->splioConnector,
      'removeNotNeededFields',
      [$data['fields']]
    );

    self::assertSame($data['expected'], $response);
  }

  public function removeNotNeededFieldsProvider(): array {
    return [
      [
        'remove_not_needed_fields' => [
          'expected' => [
            'needed' => [
              'field',
            ],
          ],
          'fields' => [
            'needed' => [
              'field',
            ],
            'not_needed' => [
              'field',
            ],
            'splioEntityType' => 'entity_type',
            'action' => 'action',
          ],
        ],
      ],
    ];
  }

  /**
   * @dataProvider \Drupal\Tests\splio\Unit\Provider\SplioRequestOptionsDataProvider::execute()
   */
  public function testGenerateRequestOptions(array $data) {
    $this->prepareAuthenticatorGetToken();
    $this->prepareStructureGeneratorIsFieldNotNeeded();

    $response = $this->invokeMethod(
      $this->splioConnector,
      'generateRequestOptions',
      [$data]
    );

    self::assertSame($data, $response);
  }

  public function prepareStructureGeneratorIsFieldNotNeeded() {
    $this
      ->structureGenerator
      ->method('isFieldNotNeeded')
      ->willReturnCallback(
        [
          $this,
          'removeNotNeededFieldsCallback',
        ]
      );
  }

  public function prepareAuthenticatorGetToken() {
    $this
      ->authenticator
      ->method('authenticate')
      ->willReturn(self::TOKEN_VALID);
  }

  public function removeNotNeededFieldsCallback(
    string $field,
    string $entityType,
    string $action
  ) {
    if ('not_needed' === $field) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * @dataProvider pingToSplioOkProvider
   */
  public function testPingToSplioOk(array $data) {
    $this->poolClientMockHandlerResponses(
      [$data['client_response']]
    );

    $result = $this->splioConnector->ping();

    self::assertSame($data['expected'], $result);
  }

  public function pingToSplioOkProvider(): array {
    return [
      [
        'response_200' => [
          'expected' => TRUE,
          'client_response' => [
            'code' => Response::HTTP_OK,
            'body' => '',
          ],
        ],
      ],
    ];
  }

  /**
   * @dataProvider pingToSplioFailProvider
   */
  public function testPingToSplioFail(array $data) {
    $this->poolClientMockHandlerResponses(
      [$data['client_response']]
    );

    $result = $this->splioConnector->ping();

    self::assertSame($data['expected'], $result);
  }

  public function pingToSplioFailProvider(): array {
    return [
      [
        'response_404' => [
          'expected' => FALSE,
          'client_response' => [
            'code' => Response::HTTP_NOT_FOUND,
            'body' => '',
          ],
        ],
        'response_500' => [
          'expected' => FALSE,
          'client_response' => [
            [
              'code' => Response::HTTP_INTERNAL_SERVER_ERROR,
              'body' => '',
            ],
          ],
        ],
      ],
    ];
  }

  /**
   * @dataProvider \Drupal\Tests\splio\Unit\Provider\SplioContactsListDataProvider::execute()
   */
  public function testGetContactsListsOk(array $data) {
    $this->poolClientMockHandlerResponses(
      [$data['client_response']]
    );

    $result = $this->splioConnector->getContactsLists();

    self::assertSame($data['expected'], $result);
  }

  /**
   * @dataProvider getContactsListsFailProvider
   */
  public function testGetContactsListsFail(array $data) {
    $this->poolClientMockHandlerResponses(
      [$data['client_response']]
    );

    $result = $this->splioConnector->getContactsLists();

    self::assertSame($data['expected'], $result);
  }

  /**
   * Provider for testGetContactsListsFail.
   *
   * @return array
   *   Data provided.
   */
  public function getContactsListsFailProvider(): array {
    return [
      [
        'response_500' => [
          'expected' => [],
          'client_response' => [
            'code' => Response::HTTP_OK,
            'body' => '',
          ],
        ],
      ],
    ];
  }

  /**
   * @dataProvider \Drupal\Tests\splio\Unit\Provider\SplioContactsFieldDataProvider::execute()
   */
  public function testGetEntityFieldsFromSplioOk(array $data) {
    $this->poolClientMockHandlerResponses(
      [$data['client_response']]
    );

    $result = $this
      ->splioConnector
      ->getEntityTypeFieldsFromSplio(
        $data['entity']
      );

    self::assertIsArray($result);
    self::assertArrayHasKey('elements', $result);
    self::assertSame('ID', $result['elements'][0]['label']);
  }

  /**
   * Provider for testGetEntityFieldsFromSplioOk.
   *
   * @return array
   *   Data provided.
   */
  public function getEntityFieldsFromSplioOkProvider(): array {
    return [
      [
        'response_200' => [
          'entity' => SplioEntity::TYPE_CONTACTS,
          'client_response' => [
            'code' => Response::HTTP_OK,
            'body' => 'SplioContactsFields.json',
          ],
        ],
      ],
    ];
  }

  /**
   * @dataProvider getEntityFieldsFromSplioFailProvider
   */
  public function testGetEntityFieldsFromSplioFail(array $data) {
    $this->poolClientMockHandlerResponses(
      [$data['client_response']]
    );

    $result = $this->splioConnector->getContactsLists();

    self::assertSame($data['expected'], $result);
  }

  /**
   * Provider for testGetEntityFieldsFromSplioFail.
   *
   * @return array
   *   Data provided.
   */
  public function getEntityFieldsFromSplioFailProvider(): array {
    return [
      [
        'response_500' => [
          'expected' => [],
          'client_response' => [
            'code' => Response::HTTP_OK,
            'body' => '',
          ],
        ],
      ],
    ];
  }

  /**
   * @dataProvider createEntitiesOkProvider
   */
  public function testCreateEntitiesOk(array $data) {
    $this->prepareGetSplioEntityTypeFromLocalEntityResponse($data['entityType']);
    $this->prepareGetProperEntityTypeResponse($data['entityType']);
    $this->prepareGetProperEntityResponse($data['entities'][0]);
    $this->prepareGenerateEntityStructureResponse($data['structure']);
    $this->prepareIsFieldNotNeededResponse($data['fieldNotNeeded']);

    $this->poolClientMockHandlerResponses(
      $data['client_responses']
    );

    $result = $this->splioConnector->createEntities($data['entities']);

    self::assertSame($data['expected'], $result[0]->getStatusCode());
  }

  /**
   * Provider for testCreateEntitiesOk.
   *
   * @return array
   *   Data provided.
   */
  public function createEntitiesOkProvider(): array {
    $entity = $this->createEntityMock();

    return [
      [
        'response_200' => [
          'expected' => Response::HTTP_OK,
          'entityType' => SplioEntity::TYPE_CONTACTS,
          'entities' => [
            $entity,
          ],
          'structure' => [
            'action' => 'create',
          ],
          'fieldNotNeeded' => TRUE,
          'client_responses' => [
            [
              'code' => Response::HTTP_OK,
              'body' => '',
            ],
          ],
        ],
      ],
    ];
  }

  /**
   * @dataProvider UpdateEntitiesOkProvider
   */
  public function testUpdateEntitiesOk(array $data) {
    $this->prepareGetSplioEntityTypeFromLocalEntityResponse($data['entityType']);
    $this->prepareGetProperEntityTypeResponse($data['entityType']);
    $this->prepareGetProperEntityResponse($data['entities'][0]);
    $this->prepareGenerateEntityStructureResponse($data['structure']);
    $this->prepareIsFieldNotNeededResponse($data['fieldNotNeeded']);

    $this->poolClientMockHandlerResponses(
      $data['client_responses']
    );

    $result = $this->splioConnector->updateEntities($data['entities']);

    self::assertSame($data['expected'], $result[0]->getStatusCode());
  }

  /**
   * Provider for testUpdateEntitiesOk.
   *
   * @return array
   *   Data provided.
   */
  public function UpdateEntitiesOkProvider(): array {
    $entity = $this->createEntityMock();

    return [
      [
        'response_200' => [
          'expected' => Response::HTTP_OK,
          'entityType' => SplioEntity::TYPE_CONTACTS,
          'entities' => [
            $entity,
          ],
          'structure' => [
            'action' => 'update',
          ],
          'fieldNotNeeded' => TRUE,
          'client_responses' => [
            [
              'code' => Response::HTTP_OK,
              'body' => '',
            ],
          ],
        ],
      ],
    ];
  }

  /**
   * @dataProvider deleteEntitiesOkProvider
   */
  public function testDeleteEntitiesOk(array $data) {
    $this->prepareGetSplioEntityTypeFromLocalEntityResponse($data['entityType']);
    $this->prepareGetProperEntityTypeResponse($data['entityType']);
    $this->prepareGetProperEntityResponse($data['entities'][0]);
    $this->prepareGenerateEntityStructureResponse($data['structure']);
    $this->prepareIsFieldNotNeededResponse($data['fieldNotNeeded']);

    $this->poolClientMockHandlerResponses(
      $data['client_responses']
    );

    $result = $this->splioConnector->deleteEntities($data['entities']);

    self::assertSame($data['expected'], $result[0]->getStatusCode());
  }

  /**
   * Provider for testDeleteEntitiesOk.
   *
   * @return array
   *   Data provided.
   */
  public function deleteEntitiesOkProvider(): array {
    $entity = $this->createEntityMock();

    return [
      [
        'response_200' => [
          'expected' => Response::HTTP_OK,
          'entityType' => SplioEntity::TYPE_CONTACTS,
          'entities' => [
            $entity,
          ],
          'structure' => [
            'action' => 'delete',
          ],
          'fieldNotNeeded' => TRUE,
          'client_responses' => [
            [
              'code' => Response::HTTP_OK,
              'body' => '',
            ],
          ],
        ],
      ],
    ];
  }

  private function prepareGetSplioEntityTypeFromLocalEntityResponse($response) {
    $this->splioEntityHandler
      ->method('getSplioEntityTypeFromLocalEntity')
      ->willReturn($response);
  }

  private function prepareGetProperEntityTypeResponse($response) {
    $this->splioEntityHandler
      ->method('getProperEntityType')
      ->willReturn($response);
  }

  /**
   * @param mixed $response
   */
  private function prepareGetProperEntityResponse($response) {
    $this->splioEntityHandler
      ->method('getProperEntity')
      ->willReturn($response);
  }

  private function prepareGenerateEntityStructureResponse($response) {
    $this->structureGenerator
      ->method('generateEntityStructure')
      ->willReturn($response);
  }

  private function prepareIsFieldNotNeededResponse($response) {
    $this->structureGenerator
      ->method('isFieldNotNeeded')
      ->willReturn($response);
  }

  /**
   *
   */
  private function createEntityMock() {

    $entity = $this
      ->createMock(Node::class);

    $entity
      ->method('__get')
      ->with('original')
      ->willReturn($entity);

    return $entity;
  }

}
