<?php

namespace Drupal\Tests\splio\Unit;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\splio\Entity\SplioField;
use Drupal\splio\Services\EntityValueHandler;
use Drupal\splio\Services\EntityValueHandlerInterface;
use Drupal\Tests\splio\Unit\Common\SplioUnitTestCase;
use Psr\Log\LoggerInterface;

class EntityValueHandlerTest extends SplioUnitTestCase {

  const DATE_FORMAT = 'Y-m-d H:i:s';

  const VALUES_VALID = [
    'timestamp' => '1663922776',
    'string' => 'Domus Aurea',
    'entity_date' => '{"uuid":"5ed7ad19-b796-476e-ae66-2cfa73416983","langcode":"es","status":true,"dependencies":[],"id":"products_date_added","list_index":null,"splio_field":"date_added","splio_entity":"products","drupal_field":"first_published_date","type_field":"date","is_key_field":false,"is_default_field":true}',
    'entity_string' => '{"uuid":"ab9774fe-3d61-4722-aee1-1e7a60221784","langcode":"es","status":true,"dependencies":[],"id":"products_brand","list_index":null,"splio_field":"brand","splio_entity":"products","drupal_field":"brand","type_field":"string","is_key_field":false,"is_default_field":true}',
  ];

  const ENTITY_TYPE_VALID = 'splio_field';

  private EntityTypeManagerInterface $entityTypeManager;

  private LoggerInterface $logger;

  private EntityValueHandlerInterface $entityValueHandler;

  public function setUp(): void {
    parent::setUp();

    $this->entityTypeManager = $this
      ->getMockBuilder(EntityTypeManagerInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $this->logger = $this
      ->getMockBuilder(LoggerInterface::class)
      ->disableOriginalConstructor()
      ->getMock();

    $this->entityValueHandler = new EntityValueHandler(
      $this->entityTypeManager,
      $this->logger
    );
  }

  /**
   * @dataProvider formatFieldOkProvider
   */
  public function testFormatFieldOk(array $data) {
    $splioField = new SplioField($data['values'], $data['entityType']);

    $result = $this->entityValueHandler->formatField(
      $splioField,
      $data['fieldValue']
    );

    self::assertSame($data['expected'], $result);
  }

  public function formatFieldOkProvider(): array {
    return [
      [
        'is_date_numeric' => [
          'expected' => date(self::DATE_FORMAT, self::VALUES_VALID['timestamp']),
          'values' => json_decode(self::VALUES_VALID['entity_date'], TRUE),
          'entityType' => self::ENTITY_TYPE_VALID,
          'fieldValue' => self::VALUES_VALID['timestamp'],
        ],
      ],
      [
        'is_not_date' => [
          'expected' => self::VALUES_VALID['string'],
          'values' => json_decode(self::VALUES_VALID['entity_string'], TRUE),
          'entityType' => self::ENTITY_TYPE_VALID,
          'fieldValue' => self::VALUES_VALID['string'],
        ],
      ],
      [
        'is_date_not_numeric_value' => [
          'expected' => '',
          'values' => json_decode(self::VALUES_VALID['entity_date'], TRUE),
          'entityType' => self::ENTITY_TYPE_VALID,
          'fieldValue' => self::VALUES_VALID['string'],
        ],
      ],
      [
        'is_date_zero_value' => [
          'expected' => '',
          'values' => json_decode(self::VALUES_VALID['entity_date'], TRUE),
          'entityType' => 'date',
          'fieldValue' => 0,
        ],
      ],
    ];
  }

}
