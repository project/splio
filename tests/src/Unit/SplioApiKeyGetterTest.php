<?php

namespace Drupal\Tests\splio\Unit;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\key\Entity\Key;
use Drupal\key\KeyRepositoryInterface;
use Drupal\splio\Services\SplioApiKeyGetter;
use Drupal\splio\Services\SplioApiKeyGetterInterface;
use Drupal\Tests\UnitTestCase;

/**
 * Tests for SplioApiKeyGetter class.
 */
class SplioApiKeyGetterTest extends UnitTestCase {

  const API_KEY = [
    'apiKey' => 'abcdefg',
    'universe' => 'bodeboca_sandbox',
  ];

  const API_KEY_NAME = 'splio_sandbox';

  const CONFIG_NAME = 'splio.settings';

  /**
   * Immutable config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  private ImmutableConfig $immutableConfig;

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private ConfigFactoryInterface $config;

  /**
   * Key manager.
   *
   * @var \Drupal\key\KeyRepositoryInterface
   */
  private KeyRepositoryInterface $keyManager;

  /**
   * Entity key.
   *
   * @var \Drupal\key\Entity\Key
   */
  private Key $key;

  /**
   * API key getter.
   *
   * @var \Drupal\splio\Services\SplioApiKeyGetterInterface
   */
  private SplioApiKeyGetterInterface $apiKeyGetter;

  /**
   * Test for getApiKey.
   *
   * @dataProvider getApiKeyProvider
   */
  public function testGetApiKey(array $data) {
    $this->immutableConfig = $this->createMock(ImmutableConfig::class);
    $this->immutableConfig
      ->method('get')
      ->willReturn($data['api_key_name']);

    $this->config = $this->createMock(ConfigFactory::class);
    $this->config
      ->method('get')
      ->willReturn($this->immutableConfig);

    $this->key = $this->createMock(Key::class);
    $this->key
      ->method('getKeyValues')
      ->willReturn($data['savedKey']);

    $this->keyManager = $this->createMock(KeyRepositoryInterface::class);
    $this->keyManager
      ->method('getKey')
      ->willReturn($this->key);

    $this->apiKeyGetter = new SplioApiKeyGetter(
      $this->config,
      $this->keyManager
    );
    $response = $this->apiKeyGetter->getApiKey($data['config']);

    self::assertSame($data['expected'], $response);
  }

  /**
   * Provider for testGetApiKey.
   *
   * @return array
   *   Data provided.
   */
  public function getApiKeyProvider(): array {
    return [
      [
        'api_key_from_config_ok' => [
          'expected' => self::API_KEY['apiKey'],
          'api_key_name' => self::API_KEY_NAME,
          'savedKey' => self::API_KEY,
          'config' => self::CONFIG_NAME,
        ],
      ],
      [
        'api_key_null' => [
          'expected' => NULL,
          'api_key_name' => NULL,
          'savedKey' => NULL,
          'config' => self::CONFIG_NAME,
        ],
      ],
    ];
  }

}
