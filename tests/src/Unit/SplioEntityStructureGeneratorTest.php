<?php

namespace Drupal\Tests\splio\Unit;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldItemList;
use Drupal\node\Entity\Node;
use Drupal\splio\Entity\SplioEntity;
use Drupal\splio\Entity\SplioField;
use Drupal\splio\Services\EntityValueHandlerInterface;
use Drupal\splio\Services\SplioEntityHandlerInterface;
use Drupal\splio\Services\SplioEntityStructureGenerator;
use Drupal\splio\Services\SplioEntityStructureGeneratorInterface;
use Drupal\Tests\splio\Unit\Common\SplioUnitTestCase;

/**
 * Class test for SplioEntityStructureGenerator.
 */
class SplioEntityStructureGeneratorTest extends SplioUnitTestCase {

  const EMAIL_VALID = 'some@email.com';

  /**
   * Splio entity handler.
   *
   * @var \Drupal\splio\Services\SplioEntityHandlerInterface
   */
  private SplioEntityHandlerInterface $splioEntityHandler;

  /**
   * Entity value handler.
   *
   * @var \Drupal\splio\Services\EntityValueHandlerInterface
   */
  private EntityValueHandlerInterface $entityValueHandler;

  /**
   * Splio structure generator.
   *
   * @var \Drupal\splio\Services\SplioEntityStructureGeneratorInterface
   */
  private SplioEntityStructureGeneratorInterface $structureGenerator;

  /**
   * Initial set up.
   */
  protected function setUp(): void {
    parent::setUp();

    $this->entityValueHandler = $this->createMock(EntityValueHandlerInterface::class);
    $this->splioEntityHandler = $this->createMock(SplioEntityHandlerInterface::class);
    $entityTypeManager = $this->createMock(EntityTypeManagerInterface::class);
    $this->entityValueHandler = $this->createMock(EntityValueHandlerInterface::class);
    $this->structureGenerator = new SplioEntityStructureGenerator(
      $this->splioEntityHandler,
      $entityTypeManager,
      $this->entityValueHandler
    );
  }

  /**
   * Tests that Splio default and custom fields are generated correctly.
   *
   * @dataProvider generateStandardFieldsProvider
   */
  public function testGenerateStandardFields(array $data) {
    $entity = $this->createMock(EntityInterface::class);

    $splioField = $this->createMock(SplioField::class);
    $splioField
      ->method('getDrupalField')
      ->willReturn($data['drupal_field']);
    $splioField
      ->method('getSplioField')
      ->willReturn($data['splio_field']);
    $splioField
      ->method('isDefaultField')
      ->willReturn($data['is_default_field']);
    $splioField
      ->method('isKeyField')
      ->willReturn($data['is_key_field']);

    $this->entityValueHandler
      ->method('formatField')
      ->willReturn($data['field_value']);

    $this->invokeMethod(
      $this->structureGenerator,
      'generateStandardFields',
      [
        [$splioField],
        $entity,
      ]
    );

    self::assertSame(
      $data['expected'],
      $this->structureGenerator->getStructure()
    );
  }

  /**
   * Provider for testGenerateStandardFields.
   *
   * @return array
   *   Data provided.
   */
  public function generateStandardFieldsProvider(): array {
    return [
      [
        'not_contact_and_defaut_key_field' => [
          'expected' => [
            'external_id' => '4186987',
          ],
          'entity_type' => SplioEntity::TYPE_RECEIPTS,
          'drupal_field' => 'order_id',
          'splio_field' => 'extid',
          'is_default_field' => TRUE,
          'is_key_field' => TRUE,
          'field_value' => '4186987',
        ],
      ],
      [
        'not_contact_and_default_not_key_field' => [
          'expected' => [
            'date_added' => '1664292289',
          ],
          'entity_type' => SplioEntity::TYPE_RECEIPTS,
          'drupal_field' => 'order_created',
          'splio_field' => 'date_added',
          'is_default_field' => TRUE,
          'is_key_field' => FALSE,
          'field_value' => '1664292289',
        ],
      ],
      [
        'contact_and_default_key_field' => [
          'expected' => [
            'external_id' => self::EMAIL_VALID,
          ],
          'entity_type' => SplioEntity::TYPE_CONTACTS,
          'drupal_field' => 'email',
          'splio_field' => 'email',
          'is_default_field' => TRUE,
          'is_key_field' => TRUE,
          'field_value' => self::EMAIL_VALID,
        ],
      ],
      [
        'contact_and_default_not_key_field' => [
          'expected' => [
            'cellphone' => '699887766',
          ],
          'entity_type' => SplioEntity::TYPE_CONTACTS,
          'drupal_field' => 'telephone',
          'splio_field' => 'cellphone',
          'is_default_field' => TRUE,
          'is_key_field' => FALSE,
          'field_value' => '699887766',
        ],
      ],
      [
        'not_default_field' => [
          'expected' => [
            'custom_fields' => [
              'deliveryAgency' => [
                'name' => 'deliveryAgency',
                'value' => 'CELERITAS',
              ],
            ],
          ],
          'entity_type' => SplioEntity::TYPE_RECEIPTS,
          'drupal_field' => 'transportista',
          'splio_field' => 'deliveryAgency',
          'is_default_field' => FALSE,
          'is_key_field' => FALSE,
          'field_value' => 'CELERITAS',
        ],
      ],
    ];
  }

  /**
   * Tests that structure for order lines is generated properly.
   *
   * @dataProvider generateOrderLinesFieldsProvider
   */
  public function testGenerateOrderLinesFields(array $data) {
    $fieldItemList = $this->createMock(FieldItemList::class);

    $this->entityValueHandler
      ->method('getFieldValue')
      ->willReturn($data['field_value']);

    $orderLineItems = $this->createMock(Node::class);

    $splioField = $this->createMock(SplioField::class);
    $splioField
      ->method('isKeyField')
      ->willReturn($data['is_key_field']);
    $splioField
      ->method('getDrupalField')
      ->willReturn($data['drupal_field']);
    $splioField
      ->method('getSplioField')
      ->willReturn($data['splio_field']);
    $splioField
      ->method('isDefaultField')
      ->willReturn($data['is_default_field']);

    $this->splioEntityHandler
      ->method('getLocalSplioEntitiesFields')
      ->willReturn([$splioField]);

    $storage = $this->createMock(EntityStorageInterface::class);
    $storage
      ->method('loadByProperties')
      ->willReturn([$splioField]);

    $this->invokeMethod(
      $this->structureGenerator,
      'generateOrderLinesFields',
      [
        [$orderLineItems],
      ]
    );

    self::assertSame(
      $data['expected'],
      $this->structureGenerator->getStructure()
    );
  }

  /**
   * Provider for testGenerateOrderLinesFields.
   *
   * @return array
   *   Data provided.
   */
  public function generateOrderLinesFieldsProvider():array {
    return [
      [
        'not_empty_default' => [
          'expected' => [
            'products' => [
              [
                'id_order' => '4186987',
              ],
            ],
          ],
          'drupal_field' => '',
          'splio_field' => 'id_order',
          'field_value' => '4186987',
          'is_default_field' => TRUE,
          'is_key_field' => TRUE,
          'email' => self::EMAIL_VALID,
        ],
      ],
      [
        'empty_default' => [
          'expected' => [
            'products' => [
              [
                'id_order' => NULL,
              ],
            ],
          ],
          'drupal_field' => '',
          'splio_field' => 'id_order',
          'field_value' => NULL,
          'is_default_field' => TRUE,
          'is_key_field' => TRUE,
          'email' => self::EMAIL_VALID,
        ],
      ],
      [
        'empty_not_default' => [
          'expected' => [
            'products' => [
              [
                'custom_fields' => [
                  [
                    'name' => 'field_name',
                    'value' => NULL,
                  ],
                ],
              ],
            ],
          ],
          'drupal_field' => '',
          'splio_field' => 'field_name',
          'field_value' => NULL,
          'is_default_field' => FALSE,
          'is_key_field' => FALSE,
          'email' => self::EMAIL_VALID,
        ],
      ],
      [
        'not_empty_not_default' => [
          'expected' => [
            'products' => [
              [
                'custom_fields' => [
                  [
                    'name' => 'field_name',
                    'value' => '4186987',
                  ],
                ],
              ],
            ],
          ],
          'drupal_field' => '',
          'splio_field' => 'field_name',
          'field_value' => '4186987',
          'is_default_field' => FALSE,
          'is_key_field' => FALSE,
          'email' => self::EMAIL_VALID,
        ],
      ],
    ];
  }

  /**
   * Tests that certain fields are not sent to Splio.
   *
   * @dataProvider isFieldNotNeededProvider
   */
  public function testIsFieldNotNeeded(array $data) {
    $result = $this->structureGenerator->isFieldNotNeeded(
      $data['field'],
      $data['entity_type'],
      $data['action']
    );

    self::assertSame($data['expected'], $result);
  }

  /**
   * Provider for testIsFieldNotNeeded.
   *
   * @return array
   *   Data provided.
   */
  public function isFieldNotNeededProvider(): array {
    return [
      [
        'field_not_needed' => [
          'expected' => TRUE,
          'field' => 'external_id',
          'entity_type' => SplioEntity::TYPE_PRODUCTS,
          'action' => 'update',
        ],
      ],
      [
        'field_needed' => [
          'expected' => FALSE,
          'field' => 'first_name',
          'entity_type' => SplioEntity::TYPE_CONTACTS,
          'action' => 'update',
        ],
      ],
    ];
  }

  /**
   * Tests that order lines have the proper values.
   *
   * Splio requests specific field "product_id", instead of "id_product".
   * Lines without product are removed.
   *
   * @dataProvider \Drupal\Tests\splio\Unit\Provider\SplioReceiptStructureProvider::execute()
   */
  public function testCheckOrderLines($data) {
    $structure = $data['sent_data'];
    $structureExpected = $data['expected'];

    $this->structureGenerator->setStructure($structure);

    $this->invokeMethod(
      $this->structureGenerator,
      'checkOrderLines'
    );

    self::assertSame(
      $structureExpected,
      $this->structureGenerator->getStructure()
    );
  }

  /**
   * Provider for testCheckOrderLines.
   *
   * @return array
   *   Provided data.
   */
  public function checkOrderLinesProvider() {
    return [
      [
        'refactor_order_lines' => [
          'expected_structure_filename' => 'SplioReceiptStructureExpected.json',
          'structure_filename' => 'SplioReceiptStructure.json',
        ],
      ],
    ];
  }

  /**
   * Tests that contact list has the proper structure.
   *
   * E.g.: ['id_subscribed_list']
   *
   * @dataProvider generateContactListsFieldsProvider
   */
  public function testGenerateContactListsFields(array $data) {
    $entity = $this->createMock(EntityInterface::class);

    $splioField = $this->createMock(SplioField::class);
    $splioField
      ->method('getDrupalField')
      ->willReturn($data['drupal_field']);
    $splioField
      ->method('getSplioField')
      ->willReturn($data['splio_field']);
    $splioField
      ->method('getListIndex')
      ->willReturn($data['list_index']);

    $this->entityValueHandler
      ->method('getFieldValue')
      ->willReturn($data['field_value']);

    $structure = $this->invokeMethod(
      $this->structureGenerator,
      'generateContactListsFields',
      [
        $entity,
        ['list_name' => $splioField],
      ]
    );

    self::assertSame($data['expected'], $structure);
  }

  /**
   * Provider for testGenerateContactListsFields.
   *
   * @return array
   *   Data provided.
   */
  public function generateContactListsFieldsProvider(): array {
    return [
      [
        'no_list_subscribed' => [
          'expected' => [],
          'drupal_field' => 'some_field',
          'splio_field' => 'some_field',
          'list_index' => '0',
          'field_value' => NULL,
        ],
      ],
      [
        'contact_field_equal_to_true' => [
          'expected' => [
            [
              'id' => '0',
            ],
          ],
          'drupal_field' => 'some_field',
          'splio_field' => 'some_field',
          'list_index' => '0',
          'field_value' => TRUE,
        ],
      ],
      [
        'contact_field_equal_to_1' => [
          'expected' => [
            [
              'id' => '0',
            ],
          ],
          'drupal_field' => 'some_field',
          'splio_field' => 'some_field',
          'list_index' => '0',
          'field_value' => 1,
        ],
      ],
      [
        'list' => [
          'expected' => [
            [
              'id' => '1',
            ],
          ],
          'drupal_field' => 'some_field',
          'splio_field' => 'bodeboca.com',
          'list_index' => '1',
          'field_value' => 'bodeboca.com',
        ],
      ],
      [
        'array_of_lists' => [
          'expected' => [
            [
              'id' => '1',
            ],
          ],
          'drupal_field' => 'some_field',
          'splio_field' => 'bodeboca.com',
          'list_index' => '1',
          'field_value' => ['bodeboca.com'],
        ],
      ],
    ];
  }

}
