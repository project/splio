<?php

/**
 * @file
 * Provides an extensible module to sync Drupal entities with Splio.
 */

use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\splio\Exception\EntitiesNotConfiguredYetException;
use Drupal\splio\Exception\EntityIsEmptyException;
use Drupal\splio\Exception\EntityTypeNotFoundInSplioException;

/**
 * Implements hook_help().
 */
function splio_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the splio module.
    case 'help.page.splio':
      $text = file_get_contents(__DIR__ . '/README.md');
      if (!\Drupal::moduleHandler()->moduleExists('markdown')) {
        return '<pre>' . Html::escape($text) . '</pre>';
      }
      else {
        // Use the Markdown filter to render the README.
        $filter_manager = \Drupal::service('plugin.manager.filter');
        $settings = \Drupal::configFactory()->get('markdown.settings')->getRawData();
        $config = ['settings' => $settings];
        $filter = $filter_manager->createInstance('markdown', $config);
        return $filter->process($text, 'en');
      }
  }
  return NULL;
}

/**
 * Implements hook_entity_insert().
 *
 * Receives an entity that has just been inserted into the DB. If it belongs to
 * an entity type that has been configured as a Splio entity it will be added
 * to the Splio entities queue with the 'create' action.
 */
function splio_entity_insert(EntityInterface $entity) {
  /** @var \Drupal\splio\Services\SplioQueueHandler $service */
  $service = \Drupal::service('splio.queue_handler');

  try {
    $service->addEntityToQueue($entity, 'create');
  }
  catch (
    EntitiesNotConfiguredYetException
    | EntityIsEmptyException
    $exception
  ) {
    \Drupal::logger('splio')->error($exception->getMessage());
  }
  catch (EntityTypeNotFoundInSplioException $exception) {
    // Do nothing.
  }
}

/**
 * Implements hook_entity_update().
 *
 * Receives an entity that has just been updated from the DB. If it belongs to
 * an entity type that has been configured as a Splio entity it will be added
 * to the Splio entities queue with the 'update' action.
 */
function splio_entity_update(EntityInterface $entity) {
  /** @var \Drupal\splio\Services\SplioQueueHandler $service */
  $service = \Drupal::service('splio.queue_handler');

  try {
    $service->addEntityToQueue($entity, 'update');
  }
  catch (
    EntitiesNotConfiguredYetException
    | EntityIsEmptyException
    $exception
  ) {
    \Drupal::logger('splio')->warning($exception->getMessage());
  }
  catch (EntityTypeNotFoundInSplioException $exception) {
    // Do nothing.
  }
}

/**
 * Implements hook_entity_delete().
 *
 * Receives an entity that has just been deleted from the DB. If it belongs to
 * an entity type that has been configured as a Splio entity it will be added
 * to the Splio entities queue with the 'delete' action.
 */
function splio_entity_delete(EntityInterface $entity) {
  /** @var \Drupal\splio\Services\SplioQueueHandler $service */
  $service = \Drupal::service('splio.queue_handler');

  try {
    $service->addEntityToQueue($entity, 'delete');
  }
  catch (
    EntitiesNotConfiguredYetException
    | EntityIsEmptyException
    $exception
  ) {
    \Drupal::logger('splio')->error($exception->getMessage());
  }
  catch (EntityTypeNotFoundInSplioException $exception) {
    // Do nothing.
  }
}
